open! Core
open! Utils

let count n l = List.count l ~f:(Int.equal n)

let get_image input (width, height) =
  let image = exactly_one_exn input in
  String.to_list image
  |> List.map ~f:Char.get_digit_exn
  |> List.chunks_of ~length:(width * height)
;;

module Part1 = struct
  let f input dimensions =
    let image = get_image input dimensions in
    let fewest_zeros =
      List.min_elt image ~compare:(Comparable.lift Int.compare ~f:(count 0))
      |> Option.value_exn
    in
    let count_ones = count 1 fewest_zeros in
    let count_twos = count 2 fewest_zeros in
    Core.print_s [%message "Done" (count_ones * count_twos : int)]
  ;;

  let command =
    let args_param =
      let open Command.Let_syntax in
      [%map_open
        let width = flag "-width" (required int) ~doc:"INT width"
        and height = flag "-height" (required int) ~doc:"INT height" in
        width, height]
    in
    Commands.with_args ~part:1 ~f ~args_param
  ;;
end

module Part2 = struct
  let f input (width, height) =
    let image = get_image input (width, height) |> List.map ~f:Array.of_list in
    List.init (width * height) ~f:(fun i ->
        List.fold ~init:2 image ~f:(fun curr layer ->
            if curr = 2 then layer.(i) else curr))
    |> List.chunks_of ~length:width
    |> List.map ~f:(fun row ->
           List.map row ~f:(function
               | 1 -> '@'
               | _ -> ' ')
           |> String.of_char_list)
    |> List.iter ~f:print_endline
  ;;

  let command =
    let args_param =
      let open Command.Let_syntax in
      [%map_open
        let width = flag "-width" (required int) ~doc:"INT width"
        and height = flag "-height" (required int) ~doc:"INT height" in
        width, height]
    in
    Commands.with_args ~part:2 ~f ~args_param
  ;;
end

let command = Commands.group ~day:8 ~part1:Part1.command ~part2:Part2.command
