open! Core
open! Std_internal

let init input =
  match input with
  | [] | _ :: _ :: _ -> raise_s [%message "Invalid input length"]
  | [ input ]        -> Intcode.create input
;;

let run input arg1 arg2 =
  let intcode = init input in
  Intcode.set intcode ~addr:1 ~value:arg1;
  Intcode.set intcode ~addr:2 ~value:arg2;
  Intcode.run intcode;
  Intcode.get intcode ~addr:0
;;

module Part1 = struct
  let f input = Core.print_s [%message "Done" ~return:(run input 12 2 : int)]
end

module Part2 = struct
  let find_inputs input target =
    let possible_inputs = List.range 0 99 in
    List.find_exn
      (List.cartesian_product possible_inputs possible_inputs)
      ~f:(fun (noun, verb) -> run input noun verb = target)
  ;;

  let command =
    Commands.with_args
      ~part:2
      ~args_param:Command.Param.(flag "target" (required int) ~doc:"INT target output")
      ~f:(fun input target ->
        let noun, verb = find_inputs input target in
        Core.print_s
          [%message "Done" (noun : int) (verb : int) ~answer:((100 * noun) + verb : int)])
  ;;
end

let command =
  Commands.group ~day:2 ~part1:(Commands.one ~part:1 ~f:Part1.f) ~part2:Part2.command
;;
