open Async

let command =
  Command.group
    ~summary:"Advent of Code 2019"
    [ Day01.command
    ; Day02.command
    ; Day03.command
    ; Day04.command
    ; Day05.command
    ; Day06.command
    ; Day07.command
    ; Day08.command
    ; Day09.command
    ; Day10.command
    ]
;;
