open! Core
open! Utils

let rec check_pairs i ~f =
  if i < 10
  then false
  else (
    let ones = i % 10 in
    let tens = i / 10 % 10 in
    if f ones tens then true else check_pairs (i / 10) ~f)
;;

let has_double = check_pairs ~f:( = )
let has_decrease = check_pairs ~f:( < )

let%expect_test _ =
  let test ~f i = Core.printf "%b\n" (f i) in
  test ~f:has_double 113322;
  [%expect {| true |}];
  test ~f:has_double 123456;
  [%expect {| false |}];
  test ~f:has_decrease 113322;
  [%expect {| true |}];
  test ~f:has_decrease 123456;
  [%expect {| false |}]
;;

let command ~part ~count_valid =
  let open Command.Let_syntax in
  ( Int.to_string part
  , Command.basic
      ~summary:(sprintf "Part %d" part)
      [%map_open
        let lower = flag "lower" (required int) ~doc:"INT lower bound"
        and upper = flag "upper" (required int) ~doc:"INT upper bound" in
        fun () -> Core.print_s [%message "Done" ~valid:(count_valid lower upper : int)]] )
;;

module Part1 = struct
  let count_valid lower upper =
    List.range lower upper
    |> List.count ~f:(fun i -> has_double i && not (has_decrease i))
  ;;

  let command = command ~part:1 ~count_valid
end

module Part2 = struct
  let last_three_same i = i % 1000 % 111 = 0
  let last_two_same i = i % 100 % 11 = 0

  let rec has_clean_double i ~prev =
    if i < 10
    then false
    else if last_two_same i && (not (last_three_same i)) && prev <> i % 10
    then true
    else has_clean_double (i / 10) ~prev:(i % 10)
  ;;

  let count_valid lower upper =
    List.range lower upper
    |> List.count ~f:(fun i -> has_clean_double i ~prev:(-1) && not (has_decrease i))
  ;;

  let command = command ~part:2 ~count_valid
end

let command = Commands.group ~day:4 ~part1:Part1.command ~part2:Part2.command
