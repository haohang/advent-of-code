open! Core
open! Utils

let run_one_loop intcodes signal =
  List.fold ~init:(true, signal) intcodes ~f:(fun (_, signal) intcode ->
      Intcode.input intcode signal;
      let status = Intcode.run' intcode in
      let output = Intcode.one_output_exn intcode in
      let continue =
        match status with
        | Halted           -> false
        | Blocked_on_input -> true
      in
      continue, output)
;;

let init memory inputs =
  List.map inputs ~f:(fun setting ->
      let intcode = Intcode.create memory in
      Intcode.input intcode setting;
      intcode)
;;

let find_best input perms ~run =
  let memory = exactly_one_exn input in
  List.fold perms ~init:Int.min_value ~f:(fun accum setting ->
      Int.max accum (run memory setting))
;;

module Part1 = struct
  let run memory inputs =
    let intcodes = init memory inputs in
    snd (run_one_loop intcodes 0)
  ;;

  let f input =
    let perms = permutations (List.range 0 5) in
    let best = find_best input perms ~run in
    Core.print_s [%message "Done" (best : int)]
  ;;
end

module Part2 = struct
  let run memory inputs =
    let intcodes = init memory inputs in
    let rec aux signal =
      let continue, signal = run_one_loop intcodes signal in
      if continue then aux signal else signal
    in
    aux 0
  ;;

  let f input =
    let perms = permutations (List.range 5 10) in
    let best = find_best input perms ~run in
    Core.print_s [%message "Done" (best : int)]
  ;;
end

let command = Commands.both ~day:7 ~part1:Part1.f ~part2:Part2.f
