open! Core

type t

val create : string -> t

module Status : sig
  type t =
    | Halted
    | Blocked_on_input
end

val run' : t -> Status.t
val run : t -> unit
val set : t -> addr:int -> value:int -> unit
val get : t -> addr:int -> int
val input : t -> int -> unit

(* Destructively read output. *)
val output : t -> int list
val one_output_exn : t -> int
