open! Core
open! Std_internal

let init input =
  match input with
  | [] | _ :: _ :: _ -> raise_s [%message "Invalid input length"]
  | [ input ]        -> Intcode.create input
;;

let run input code =
  let intcode = init input in
  Intcode.input intcode code;
  Intcode.run intcode;
  Intcode.output intcode
;;

module Part1 = struct
  let f input = Core.print_s [%message "Done" ~return:(run input 1 : int list)]
end

module Part2 = struct
  let f input = Core.print_s [%message "Done" ~return:(run input 5 : int list)]
end

let command = Commands.both ~day:5 ~part1:Part1.f ~part2:Part2.f
