open! Core
open! Std_internal

let compute_fuel input ~get_fuel =
  let input = List.map input ~f:Int.of_string in
  let total_fuel = List.fold input ~init:0 ~f:(fun accum mass -> accum + get_fuel mass) in
  print_s [%message "Done" (total_fuel : int)]
;;

module Part1 = struct
  let get_fuel mass = (mass / 3) - 2
  let f input = compute_fuel input ~get_fuel
end

module Part2 = struct
  let rec get_fuel mass =
    let fuel = Part1.get_fuel mass in
    if fuel <= 0 then 0 else fuel + get_fuel fuel
  ;;

  let f input = compute_fuel input ~get_fuel
end

let command = Commands.both ~day:1 ~part1:Part1.f ~part2:Part2.f
