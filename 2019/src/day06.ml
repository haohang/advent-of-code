open! Core
open! Std_internal
open! Core
open! Std_internal

module Orbit = struct
  type t =
    { center : string
    ; orbiter : string
    }
  [@@deriving fields, sexp_of]

  let creator, () =
    Fields.make_creator
      ~center:(fun field () -> Simple_parser.Result.get_string ~field, ())
      ~orbiter:(fun field () -> Simple_parser.Result.get_string ~field, ())
      ()
  ;;

  let spec = [ "(center alphanum)"; "\\)"; "(orbiter alphanum)" ]

  let of_string s =
    let result = Simple_parser.parse ~s spec in
    creator result
  ;;

  let%expect_test "of_string" =
    Core.printf !"%{sexp: t}\n%!" (of_string "ABC)DEF");
    [%expect {| ((center ABC) (orbiter DEF)) |}]
  ;;
end

let orbit_map input =
  List.map input ~f:Orbit.of_string
  |> List.map ~f:(fun o -> o.Orbit.orbiter, o.center)
  |> String.Map.of_alist_exn
;;

let rec get_path elt orbit_map =
  let elt = String.Map.find_exn orbit_map elt in
  if String.equal elt "COM" then [] else elt :: get_path elt orbit_map
;;

module Part1 = struct
  let f input =
    let orbit_map = orbit_map input in
    let orbits =
      List.fold ~init:0 (String.Map.keys orbit_map) ~f:(fun accum elt ->
          accum + List.length (get_path elt orbit_map))
    in
    Core.print_s [%message "Done" (orbits : int)]
  ;;
end

module Part2 = struct
  let f input =
    let orbit_map = orbit_map input in
    let santa_path =
      get_path "SAN" orbit_map
      |> List.mapi ~f:(fun idx elt -> elt, idx)
      |> String.Map.of_alist_exn
    in
    let you_path =
      get_path "YOU" orbit_map
      |> List.mapi ~f:(fun idx elt -> elt, idx)
      |> String.Map.of_alist_exn
    in
    let shortest =
      String.Map.merge santa_path you_path ~f:(fun ~key:_ -> function
        | `Left _ | `Right _ -> None
        | `Both (s, y)       -> Some (s + y))
      |> String.Map.fold ~init:Int.max_value ~f:(fun ~key:_ ~data:len -> Int.min len)
    in
    Core.print_s [%message "Done" (shortest : int)]
  ;;
end

let command = Commands.both ~day:6 ~part1:Part1.f ~part2:Part2.f
