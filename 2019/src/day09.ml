open! Core
open! Utils

module Part1 = struct
  let f input =
    let intcode = Intcode.create (exactly_one_exn input) in
    Intcode.input intcode 1;
    Intcode.run intcode;
    Core.print_s [%message "Done" ~output:(Intcode.output intcode : int list)]
  ;;
end

module Part2 = struct
  let f input =
    let intcode = Intcode.create (exactly_one_exn input) in
    Intcode.input intcode 2;
    Intcode.run intcode;
    Core.print_s [%message "Done" ~output:(Intcode.output intcode : int list)]
  ;;
end

let command = Commands.both ~day:9 ~part1:Part1.f ~part2:Part2.f
