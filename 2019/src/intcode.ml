open! Core
module Position = Int

module Memory : sig
  module Addr : sig
    type t [@@deriving sexp_of]

    val create : int -> t
    val add : t -> int -> t
  end

  module Mode : sig
    type t

    val of_int : int -> t
  end

  type t [@@deriving sexp_of]

  val create : string -> t
  val get : t -> Addr.t -> int
  val get_as_addr : t -> Addr.t -> mode:Mode.t -> Addr.t
  val set : t -> Addr.t -> int -> unit
  val adjust_base : t -> int -> unit

  (* Dereferences the given address and interprets based on the provided mode. *)
  val deref : t -> Addr.t -> mode:Mode.t -> int
end = struct
  module Addr = struct
    type t = int [@@deriving sexp_of]

    let create = Fn.id
    let add = Int.( + )
  end

  module Mode = struct
    type t =
      | Position
      | Immediate
      | Relative

    let of_int = function
      | 0    -> Position
      | 1    -> Immediate
      | 2    -> Relative
      | mode -> raise_s [%message "Invalid mode" (mode : int)]
    ;;
  end

  type t =
    { memory : int Array.t
    ; mutable relative_base_offset : int
    }
  [@@deriving sexp_of]

  let get t i = t.memory.(i)

  let get_as_addr t i ~mode =
    match mode with
    | Mode.Position -> get t i
    | Immediate     ->
      raise_s [%message "BUG - tried to get_as_addr in immediate mode" ~addr:(i : int)]
    | Relative      -> get t i + t.relative_base_offset
  ;;

  let set t i val_ = t.memory.(i) <- val_
  let adjust_base t by = t.relative_base_offset <- t.relative_base_offset + by

  let deref t i ~mode =
    match mode with
    | Mode.Position | Relative ->
      (* In position and relative mode, t.(i) represents a position, so we need to get the value at that position rather than i. *)
      get t (get_as_addr t i ~mode)
    | Immediate                ->
      (* In immediate mode, t.(i) represents a value, so we return the value there. *)
      get t i
  ;;

  let create s =
    let initial_memory =
      String.split s ~on:',' |> List.map ~f:Int.of_string |> Array.of_list
    in
    let memory =
      Array.append initial_memory (Array.create ~len:(Array.length initial_memory * 10) 0)
    in
    { memory; relative_base_offset = 0 }
  ;;
end

module Opcode = struct
  module Arg = struct
    let get_mode i = Memory.Mode.of_int (i % 10)

    module One = struct
      type t = Memory.Mode.t

      let of_int i = get_mode (i / 100)
    end

    module Two = struct
      type t =
        { arg1 : Memory.Mode.t
        ; arg2 : Memory.Mode.t
        }

      let of_int i =
        let arg1 = get_mode (i / 100) in
        let arg2 = get_mode (i / 1000) in
        { arg1; arg2 }
      ;;
    end

    module Three = struct
      type t =
        { arg1 : Memory.Mode.t
        ; arg2 : Memory.Mode.t
        ; arg3 : Memory.Mode.t
        }

      let of_int i =
        let { Two.arg1; arg2 } = Two.of_int i in
        let arg3 = get_mode (i / 10000) in
        { arg1; arg2; arg3 }
      ;;
    end
  end

  type t =
    | Add           of Arg.Three.t
    | Multiply      of Arg.Three.t
    | Input         of Arg.One.t
    | Output        of Arg.One.t
    | Jump_if_true  of Arg.Two.t
    | Jump_if_false of Arg.Two.t
    | Less_than     of Arg.Three.t
    | Equals        of Arg.Three.t
    | Adjust_base   of Arg.One.t
    | Halt

  let of_int i =
    let base_op = i % 100 in
    match base_op with
    | 1      -> Add (Arg.Three.of_int i)
    | 2      -> Multiply (Arg.Three.of_int i)
    | 3      -> Input (Arg.One.of_int i)
    | 4      -> Output (Arg.One.of_int i)
    | 5      -> Jump_if_true (Arg.Two.of_int i)
    | 6      -> Jump_if_false (Arg.Two.of_int i)
    | 7      -> Less_than (Arg.Three.of_int i)
    | 8      -> Equals (Arg.Three.of_int i)
    | 9      -> Adjust_base (Arg.One.of_int i)
    | 99     -> Halt
    | opcode -> raise_s [%message "Unknown opcode" (opcode : int)]
  ;;
end

type t =
  { memory : Memory.t
  ; mutable pc : Memory.Addr.t
  ; input : int Queue.t
  ; output : int Queue.t
  }
[@@deriving sexp_of]

module Status = struct
  type t =
    | Halted
    | Blocked_on_input
end

let rec ternary_op ({ pc; memory; _ } as t) { Opcode.Arg.Three.arg1; arg2; arg3 } ~f =
  let a = Memory.deref ~mode:arg1 memory (Memory.Addr.add pc 1) in
  let b = Memory.deref ~mode:arg2 memory (Memory.Addr.add pc 2) in
  let target = Memory.get_as_addr ~mode:arg3 memory (Memory.Addr.add pc 3) in
  Memory.set memory target (f a b);
  incr_pc_and_continue t 4

and add t modes = ternary_op t modes ~f:( + )
and multiply t modes = ternary_op t modes ~f:( * )

and input ({ pc; memory; input; _ } as t) mode =
  match Queue.dequeue input with
  | None   -> Status.Blocked_on_input
  | Some i ->
    let target = Memory.get_as_addr memory (Memory.Addr.add pc 1) ~mode in
    Memory.set memory target i;
    incr_pc_and_continue t 2

and output ({ pc; memory; output; _ } as t) mode =
  Queue.enqueue output (Memory.deref ~mode memory (Memory.Addr.add pc 1));
  incr_pc_and_continue t 2

and jump ({ pc; memory; _ } as t) { Opcode.Arg.Two.arg1; arg2 } ~if_ =
  let val_ = Memory.deref ~mode:arg1 memory (Memory.Addr.add pc 1) in
  if Bool.equal if_ (val_ <> 0)
  then (
    t.pc <- Memory.Addr.create (Memory.deref ~mode:arg2 memory (Memory.Addr.add pc 2));
    run' t)
  else incr_pc_and_continue t 3

and less_than t modes = ternary_op t modes ~f:(fun a b -> if a < b then 1 else 0)
and equals t modes = ternary_op t modes ~f:(fun a b -> if a = b then 1 else 0)

and adjust_base ({ memory; pc; _ } as t) mode =
  Memory.adjust_base memory (Memory.deref memory (Memory.Addr.add pc 1) ~mode);
  incr_pc_and_continue t 2

and run' t =
  match Opcode.of_int (Memory.get t.memory t.pc) with
  | Add modes           -> add t modes
  | Multiply modes      -> multiply t modes
  | Input mode          -> input t mode
  | Output mode         -> output t mode
  | Jump_if_true modes  -> jump t modes ~if_:true
  | Jump_if_false modes -> jump t modes ~if_:false
  | Less_than modes     -> less_than t modes
  | Equals modes        -> equals t modes
  | Adjust_base mode    -> adjust_base t mode
  | Halt                -> Halted

and incr_pc_and_continue t by =
  t.pc <- Memory.Addr.add t.pc by;
  run' t
;;

let run t = ignore (run' t : Status.t)

let create s =
  let memory = Memory.create s in
  { memory; pc = Memory.Addr.create 0; input = Queue.create (); output = Queue.create () }
;;

let set t ~addr ~value =
  let addr = Memory.Addr.create addr in
  Memory.set t.memory addr value
;;

let get t ~addr =
  let addr = Memory.Addr.create addr in
  Memory.get t.memory addr
;;

let input t = Queue.enqueue t.input

let output t =
  let output = Queue.to_list t.output in
  Queue.clear t.output;
  output
;;

let one_output_exn t = Queue.dequeue_exn t.output

let%test_module _ =
  (module struct
    let%expect_test "relative mode" =
      let t = create "109,19,99" in
      run t;
      Core.printf !"%{sexp: t}\n%!" t;
      [%expect
        {|
        ((memory
          ((memory
            (109 19 99 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0))
           (relative_base_offset 19)))
         (pc 2) (input ()) (output ())) |}];
      let t = create "104,1125899906842624,99" in
      run t;
      Core.printf !"%{sexp: t}\n%!" t;
      [%expect {|
        ((memory
          ((memory
            (104 1125899906842624 99 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
             0 0 0 0 0 0))
           (relative_base_offset 0)))
         (pc 2) (input ()) (output (1125899906842624))) |}]
    ;;
  end)
;;
