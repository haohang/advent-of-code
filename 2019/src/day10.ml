open! Core
open! Utils
open Graph

module Part1 = struct
  let normalize ({ Coord.row; col } as c) =
    (* Horizontal and vertical slopes get normalized. *)
    if row = 0 && col = 0
    then c
    else if row = 0
    then { Coord.row = 0; col = (if col < 0 then -1 else 1) }
    else if col = 0
    then { Coord.row = (if row < 0 then -1 else 1); col = 0 }
    else (
      let gcd = gcd (abs row) (abs col) in
      { Coord.row = row / gcd; col = col / gcd })
  ;;

  let f input =
    let grid = Grid.of_strings input in
    let asteroids =
      Array.foldi grid.Grid.grid ~init:[] ~f:(fun row coords r ->
          Array.foldi r ~init:coords ~f:(fun col coords char ->
              if Char.equal '#' char then { Coord.row; col } :: coords else coords))
    in
    let count_visible from =
      List.filter_map asteroids ~f:(fun a ->
          if Coord.equal a from
          then None
          else (
            let vec = Coord.sub a from in
            Some (normalize (Coord.sub a from), vec)))
      |> Coord.Map.of_alist_multi
      |> Coord.Map.length
    in
    let max =
      List.fold asteroids ~init:0 ~f:(fun accum a -> Int.max accum (count_visible a))
    in
    (* We subtract one because we include the asteroid itself as a visible
       asteroid. *)
    Core.print_s [%message "Done" ~best:(max - 1 : int)]
  ;;
end

module Part2 = struct
  let f input = ignore input

  (* let f input =
   *   let intcode = Intcode.create (exactly_one_exn input) in
   *   Intcode.input intcode 2;
   *   Intcode.run intcode;
   *   Core.print_s [%message "Done" ~output:(Intcode.output intcode : int list)]
   * ;; *)
end

let command = Commands.both ~day:10 ~part1:Part1.f ~part2:Part2.f
