open! Core
open! Utils
open Graph

let visited_coords moves =
  let moves = String.split moves ~on:',' |> List.map ~f:Move.of_string in
  let _, visited =
    List.fold
      moves
      ~init:((Coord.origin, 0), Coord.Map.empty)
      ~f:(fun ((curr_loc, curr_steps), visited) move ->
        let (new_loc, new_steps), newly_visited =
          Move.apply_with_step_count move curr_loc
        in
        let visited =
          List.fold newly_visited ~init:visited ~f:(fun accum (loc, steps) ->
              if not (Coord.Map.mem accum loc)
              then Coord.Map.set accum ~key:loc ~data:(steps + curr_steps)
              else accum)
        in
        (new_loc, new_steps + curr_steps), visited)
  in
  visited
;;

let get_overlaps input =
  match input with
  | [] | [ _ ] | _ :: _ :: _ :: _ -> raise_s [%message "Invalid input"]
  | [ first; second ]             ->
    let coords1 = visited_coords first in
    let coords2 = visited_coords second in
    Coord.Map.merge coords1 coords2 ~f:(fun ~key:_ -> function
      | `Left _ | `Right _     -> None
      | `Both (steps1, steps2) -> Some (steps1 + steps2))
;;

module Part1 = struct
  let f input =
    let closest =
      get_overlaps input
      |> Coord.Map.fold ~init:Int.max_value ~f:(fun ~key:next ~data:_ curr_best ->
             Int.min curr_best (Coord.manhattan_dist next Coord.origin))
    in
    Core.print_s [%message "Done" (closest : int)]
  ;;
end

module Part2 = struct
  let f input =
    let closest =
      get_overlaps input
      |> Coord.Map.fold ~init:Int.max_value ~f:(fun ~key:_ ~data:next curr_best ->
             Int.min curr_best next)
    in
    Core.print_s [%message "Done" (closest : int)]
  ;;
end

let command = Commands.both ~day:3 ~part1:Part1.f ~part2:Part2.f
