open! Core
open! Std_internal

module Config = struct
  let day = "06"
  let short_input = {|3,4,3,1,2|}
  let test_inputs = [||]
end

module Parsed = struct
  type t = int list [@@deriving sexp]

  let parse_one s = String.split s ~on:',' |> List.map ~f:Int.of_string
  let parse input = List.map input ~f:parse_one |> exactly_one_exn
end

module Inputs = struct
  let real_input () =
    Stdio.In_channel.read_lines [%string "../input/%{Config.day}.txt"] |> Parsed.parse
  ;;

  let short_input = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" short_input;
    [%expect {| (3 4 3 1 2) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

let sim l n =
  let incr_by accum key value =
    Int.Map.update accum key ~f:(function
      | None -> value
      | Some i -> i + value)
  in
  let init = List.fold ~init:Int.Map.empty l ~f:(fun accum i -> incr_by accum i 1) in
  let sim_one fish =
    Int.Map.fold fish ~init:Int.Map.empty ~f:(fun ~key:i ~data:k accum ->
      if i > 0
      then incr_by accum (i - 1) k
      else (
        let accum = incr_by accum 6 k in
        incr_by accum 8 k))
  in
  Fn.apply_n_times ~n sim_one init
  |> Int.Map.fold ~init:0 ~f:(fun ~key:_ ~data:k accum -> accum + k)
;;

module Part1 = struct
  let f input =
    let result = sim input 80 in
    printf !"%{sexp: int}\n%!" result
  ;;

  let%expect_test "part 1" =
    f Inputs.short_input;
    [%expect {| 5934 |}];
    f (Inputs.real_input ());
    [%expect {| 396210 |}]
  ;;
end

module Part2 = struct
  let f input =
    let result = sim input 256 in
    printf !"%{sexp: int}\n%!" result
  ;;

  let%expect_test "part 2" =
    f Inputs.short_input;
    [%expect {| 26984457539 |}];
    f (Inputs.real_input ());
    [%expect {| 1770823541496 |}]
  ;;
end
