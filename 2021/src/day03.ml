open! Core
open! Std_internal

module Config = struct
  let day = "03"

  let short_input =
    {|00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010|}
  ;;

  let test_inputs = [||]
end

module Parsed = struct
  type t = int array list [@@deriving sexp]

  let parse_one s =
    String.to_list s
    |> List.map ~f:Char.to_string
    |> List.map ~f:Int.of_string
    |> Array.of_list
  ;;

  let parse input = List.map input ~f:parse_one
end

module Inputs = struct
  let real_input () =
    Stdio.In_channel.read_lines [%string "../input/%{Config.day}.txt"] |> Parsed.parse
  ;;

  let short_input = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" short_input;
    [%expect
      {|
      ((0 0 1 0 0) (1 1 1 1 0) (1 0 1 1 0) (1 0 1 1 1) (1 0 1 0 1) (0 1 1 1 1)
       (0 0 1 1 1) (1 1 1 0 0) (1 0 0 0 0) (1 1 0 0 1) (0 0 0 1 0) (0 1 0 1 0)) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

let ones l i = List.fold ~init:0 l ~f:(fun acc l -> acc + l.(i))

let most_common_digit l i =
  let total = List.length l in
  let ones = ones l i in
  let zeros = total - ones in
  if ones > zeros then `one else if zeros > ones then `zero else `tie
;;

module Part1 = struct
  let find_gamma_and_epsilon input =
    let gamma =
      List.init
        (Array.length (List.hd_exn input))
        ~f:(fun i ->
          match most_common_digit input i with
          | `one -> 1
          | `zero -> 0
          | `tie -> 1)
      |> binary_digits_to_decimal
    in
    let epsilon =
      List.init
        (Array.length (List.hd_exn input))
        ~f:(fun i ->
          match most_common_digit input i with
          | `one -> 0
          | `zero -> 1
          | `tie -> 0)
      |> binary_digits_to_decimal
    in
    gamma * epsilon
  ;;

  let f input =
    let result = find_gamma_and_epsilon input in
    printf !"%{sexp: int}\n%!" result
  ;;

  let%expect_test "part 1" =
    f Inputs.short_input;
    [%expect {| 198 |}];
    f (Inputs.real_input ());
    [%expect {| 3969000 |}]
  ;;
end

module Part2 = struct
  let find l ~which =
    let rec helper l i =
      match l with
      | [] -> raise_s [%message "BUG"]
      | [ x ] -> binary_digits_to_decimal (Array.to_list x)
      | _ :: _ ->
        let most_common_digit = most_common_digit l i in
        let which = which ~most_common_digit in
        let l = List.filter l ~f:(fun l -> l.(i) = which) in
        helper l (i + 1)
    in
    helper l 0
  ;;

  let find_o2 =
    find ~which:(fun ~most_common_digit ->
      match most_common_digit with
      | `one | `tie -> 1
      | `zero -> 0)
  ;;

  let find_co2 =
    find ~which:(fun ~most_common_digit ->
      match most_common_digit with
      | `one | `tie -> 0
      | `zero -> 1)
  ;;

  let find_o2_and_co2 l =
    let o2 = find_o2 l in
    let co2 = find_co2 l in
    o2 * co2
  ;;

  let f input =
    let result = find_o2_and_co2 input in
    printf !"%{sexp: int}\n%!" result
  ;;

  let%expect_test "part 2" =
    f Inputs.short_input;
    [%expect {| 230 |}];
    f (Inputs.real_input ());
    [%expect {| 4267809 |}]
  ;;
end
