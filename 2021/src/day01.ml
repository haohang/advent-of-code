open! Core
open! Std_internal

module Config = struct
  let day = 1

  let short_input = {|199
200
208
210
200
207
240
269
260
263|}

  let test_inputs = [||]
end

module Parsed = struct
  type t = int list [@@deriving sexp]

  let parse_one s = Int.of_string s
  let parse input = List.map input ~f:parse_one
end

module Inputs = struct
  let real_input () =
    Stdio.In_channel.read_lines (sprintf "../input/%02d.txt" Config.day) |> Parsed.parse
  ;;

  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect {| (199 200 208 210 200 207 240 269 260 263) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

module Part1 = struct
  let run input =
    List.zip_exn (List.drop_last_exn input) (List.tl_exn input)
    |> List.count ~f:(fun (a, b) -> b > a)
  ;;

  let f input =
    let result = run input in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 1" =
    f (Inputs.short_input ());
    [%expect {| result: 7 |}];
    f (Inputs.real_input ());
    [%expect {| result: 1393 |}]
  ;;
end

module Part2 = struct
  let run input =
    let sums =
      List.zip_exn (slice_list input 0 (-2)) (slice_list input 1 (-1))
      |> List.zip_exn (slice_list input 2 Int.max_value)
      |> List.map ~f:(fun (a, (b, c)) -> a + b + c)
    in
    Part1.run sums
  ;;

  let f input =
    let result = run input in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 2" =
    f (Inputs.short_input ());
    [%expect {| result: 5 |}];
    f (Inputs.real_input ());
    [%expect {| result: 1359 |}]
  ;;
end
