open! Core
open! Std_internal

module Config = struct
  let day = "18"

  let short_input =
    {|[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]|}
  ;;

  let test_inputs = [||]
end

module Snailfish = struct
  type single =
    { depth : int
    ; value : [ `l of int | `r of int ]
    }
  [@@deriving sexp, compare, equal]

  type t = single list [@@deriving sexp, compare, equal]

  let add t1 t2 = List.map (t1 @ t2) ~f:(fun elt -> { elt with depth = elt.depth + 1 })

  let split t =
    let a, b =
      List.fold t ~init:([], false) ~f:(fun (accum, split) elt ->
        if split
        then elt :: accum, split
        else (
          match elt.value with
          | `l i when i > 9 ->
            let a = i / 2 in
            let b = i - a in
            ( { depth = elt.depth + 1; value = `r b }
              :: { depth = elt.depth + 1; value = `l a }
              :: accum
            , true )
          | `r i when i > 9 ->
            let a = i / 2 in
            let b = i - a in
            ( { depth = elt.depth + 1; value = `r b }
              :: { depth = elt.depth + 1; value = `l a }
              :: accum
            , true )
          | _ -> elt :: accum, false))
    in
    List.rev a, b
  ;;

  let explode t =
    let a, b =
      List.fold t ~init:([], `no) ~f:(fun (accum, exploded) elt ->
        match exploded with
        | `fully -> elt :: accum, exploded
        | `no when elt.depth < 5 -> elt :: accum, exploded
        | `no ->
          (match elt.value with
           | `r _ -> raise_s [%message "BUG! Invariant broken"]
           | `l i ->
             (match accum with
              | [] -> [ { depth = elt.depth - 1; value = `l 0 } ], `left
              | x :: xs ->
                let prev = List.find accum ~f:(fun i -> i.depth = elt.depth - 1) in
                let value =
                  match prev with
                  | None -> `l 0
                  | Some v ->
                    (match v.value with
                     | `l _ -> `r 0
                     | `r _ -> `l 0)
                in
                let x_value =
                  match x.value with
                  | `l j -> `l (j + i)
                  | `r j -> `r (j + i)
                in
                ( { depth = elt.depth - 1; value } :: { x with value = x_value } :: xs
                , `left )))
        | `left ->
          (match elt.value with
           | `l _ -> raise_s [%message "BUG! Invariant broken"]
           | `r i -> accum, `right i)
        | `right i ->
          let value =
            match elt.value with
            | `l j -> `l (i + j)
            | `r j -> `r (i + j)
          in
          { elt with value } :: accum, `fully)
    in
    ( List.rev a
    , match b with
      | `fully | `right _ -> true
      | `no -> false
      | _ ->
        Core.print_s [%message (t : t) (b : [ `fully | `no | `left | `right of int ])];
        rip () )
  ;;

  module For_parsing = struct
    type t =
      | Regular of int
      | Pair of t * t
    [@@deriving sexp]

    let rec of_string s =
      match Or_error.try_with (fun () -> Int.of_string s) with
      | Ok i -> Regular i
      | Error _ ->
        let a, b =
          let depth = ref 0 in
          String.chop_prefix_exn s ~prefix:"["
          |> String.chop_suffix_exn ~suffix:"]"
          |> String.to_list
          |> List.split_while ~f:(function
               | '[' ->
                 incr depth;
                 true
               | ']' ->
                 decr depth;
                 true
               | ',' -> !depth <> 0
               | _ -> true)
        in
        Pair
          ( of_string (String.of_char_list a)
          , of_string (String.of_char_list (slice_list b 1 Int.max_value)) )
    ;;

    let reconstruct t =
      let stack = Deque.create () in
      List.iter t ~f:(fun elt ->
        let v =
          Regular
            (match elt.value with
             | `l i | `r i -> i)
        in
        let rec collapse d v =
          match Deque.dequeue_front stack with
          | None -> Deque.enqueue_front stack (d, v)
          | Some (d', v') ->
            if d' = d
            then collapse (d - 1) (Pair (v', v))
            else (
              Deque.enqueue_front stack (d', v');
              Deque.enqueue_front stack (d, v))
        in
        collapse elt.depth v);
      Deque.to_list stack |> e1
    ;;

    let flatten t =
      let rec helper t depth =
        match t with
        | Regular _ -> rip ()
        | Pair (t1, t2) ->
          let t1_component =
            match t1 with
            | Regular i -> [ { depth = depth + 1; value = `l i } ]
            | Pair _ -> helper t1 (depth + 1)
          in
          let t2_component =
            match t2 with
            | Regular i -> [ { depth = depth + 1; value = `r i } ]
            | Pair _ -> helper t2 (depth + 1)
          in
          t1_component @ t2_component
      in
      helper t 0
    ;;

    let rec magnitude t =
      match t with
      | Regular i -> i
      | Pair (t1, t2) -> (3 * magnitude t1) + (2 * magnitude t2)
    ;;
  end

  let%expect_test _ =
    let s = {|[[6,[5,[4,[3,2]]]],1]|} in
    let n = s |> For_parsing.of_string |> For_parsing.flatten in
    Core.print_s [%message (n : t)];
    let o = explode n in
    Core.print_s
      [%message (o : t * bool) (For_parsing.reconstruct (fst o) : int * For_parsing.t)];
    [%expect
      {|
      (n
       (((depth 2) (value (l 6))) ((depth 3) (value (l 5)))
        ((depth 4) (value (l 4))) ((depth 5) (value (l 3)))
        ((depth 5) (value (r 2))) ((depth 1) (value (r 1)))))
      ((o
        ((((depth 2) (value (l 6))) ((depth 3) (value (l 5)))
          ((depth 4) (value (l 7))) ((depth 4) (value (r 0)))
          ((depth 1) (value (r 3))))
         true))
       ("For_parsing.reconstruct (fst o)"
        (0
         (Pair (Pair (Regular 6) (Pair (Regular 5) (Pair (Regular 7) (Regular 0))))
          (Regular 3))))) |}]
  ;;

  let rec reduce t =
    let t, exploded = explode t in
    if exploded
    then reduce t
    else (
      let t, split = split t in
      if split then reduce t else t)
  ;;

  let perform_addition t1 t2 = add t1 t2 |> reduce
end

module Parsed = struct
  type t = Snailfish.t list [@@deriving sexp]

  let parse_one s = Snailfish.For_parsing.of_string s |> Snailfish.For_parsing.flatten
  let parse input = List.map input ~f:parse_one
end

module Inputs = struct
  let real_input () =
    Stdio.In_channel.read_lines [%string "../input/%{Config.day}.txt"] |> Parsed.parse
  ;;

  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect
      {|
      ((((depth 3) (value (l 0))) ((depth 4) (value (l 5)))
        ((depth 4) (value (r 8))) ((depth 4) (value (l 1)))
        ((depth 4) (value (r 7))) ((depth 4) (value (l 9)))
        ((depth 4) (value (r 6))) ((depth 3) (value (l 4)))
        ((depth 4) (value (l 1))) ((depth 4) (value (r 2)))
        ((depth 4) (value (l 1))) ((depth 4) (value (r 4)))
        ((depth 3) (value (r 2))))
       (((depth 3) (value (l 5))) ((depth 4) (value (l 2)))
        ((depth 4) (value (r 8))) ((depth 2) (value (r 4)))
        ((depth 2) (value (l 5))) ((depth 4) (value (l 9)))
        ((depth 4) (value (r 9))) ((depth 3) (value (r 0))))
       (((depth 1) (value (l 6))) ((depth 4) (value (l 6)))
        ((depth 4) (value (r 2))) ((depth 4) (value (l 5)))
        ((depth 4) (value (r 6))) ((depth 4) (value (l 7)))
        ((depth 4) (value (r 6))) ((depth 4) (value (l 4)))
        ((depth 4) (value (r 7))))
       (((depth 3) (value (l 6))) ((depth 4) (value (l 0)))
        ((depth 4) (value (r 7))) ((depth 3) (value (l 0)))
        ((depth 3) (value (r 9))) ((depth 2) (value (l 4)))
        ((depth 3) (value (l 9))) ((depth 4) (value (l 9)))
        ((depth 4) (value (r 0))))
       (((depth 3) (value (l 7))) ((depth 4) (value (l 6)))
        ((depth 4) (value (r 4))) ((depth 3) (value (l 3)))
        ((depth 4) (value (l 1))) ((depth 4) (value (r 3)))
        ((depth 4) (value (l 5))) ((depth 4) (value (r 5)))
        ((depth 3) (value (r 1))) ((depth 2) (value (r 9))))
       (((depth 2) (value (l 6))) ((depth 4) (value (l 7)))
        ((depth 4) (value (r 3))) ((depth 4) (value (l 3)))
        ((depth 4) (value (r 2))) ((depth 4) (value (l 3)))
        ((depth 4) (value (r 8))) ((depth 4) (value (l 5)))
        ((depth 4) (value (r 7))) ((depth 2) (value (r 4))))
       (((depth 4) (value (l 5))) ((depth 4) (value (r 4)))
        ((depth 4) (value (l 7))) ((depth 4) (value (r 7)))
        ((depth 2) (value (r 8))) ((depth 3) (value (l 8)))
        ((depth 3) (value (r 3))) ((depth 2) (value (r 8))))
       (((depth 2) (value (l 9))) ((depth 2) (value (r 3)))
        ((depth 3) (value (l 9))) ((depth 3) (value (r 9)))
        ((depth 3) (value (l 6))) ((depth 4) (value (l 4)))
        ((depth 4) (value (r 9))))
       (((depth 2) (value (l 2))) ((depth 4) (value (l 7)))
        ((depth 4) (value (r 7))) ((depth 3) (value (r 7)))
        ((depth 3) (value (l 5))) ((depth 3) (value (r 8)))
        ((depth 4) (value (l 9))) ((depth 4) (value (r 3)))
        ((depth 4) (value (l 0))) ((depth 4) (value (r 2))))
       (((depth 4) (value (l 5))) ((depth 4) (value (r 2)))
        ((depth 3) (value (r 5))) ((depth 3) (value (l 8)))
        ((depth 4) (value (l 3))) ((depth 4) (value (r 7)))
        ((depth 3) (value (l 5))) ((depth 4) (value (l 7)))
        ((depth 4) (value (r 5))) ((depth 3) (value (l 4)))
        ((depth 3) (value (r 4))))) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

module Part1 = struct
  let f input =
    let result =
      List.reduce_exn input ~f:(fun a b -> Snailfish.perform_addition a b)
      |> Snailfish.For_parsing.reconstruct
      |> snd
      |> Snailfish.For_parsing.magnitude
    in
    print_endline [%string "result: %{result#Int}"]
  ;;

  (* let%expect_test "part 1" = *)
  (*   f (Inputs.short_input ()); *)
  (*   [%expect {| result: 4140 |}]; *)
  (*   f (Inputs.real_input ()); *)
  (*   [%expect {| result: 4202 |}] *)
  (* ;; *)
end

module Part2 = struct
  let f input =
    let pairs =
      List.cartesian_product input input
      |> List.filter ~f:(fun (t1, t2) -> not (Snailfish.equal t1 t2))
    in
    let total = ref 0 in
    let result =
      List.map pairs ~f:(fun (a, b) ->
        incr total;
        Snailfish.perform_addition a b
        |> Snailfish.For_parsing.reconstruct
        |> snd
        |> Snailfish.For_parsing.magnitude)
      |> List.reduce_exn ~f:Int.max
    in
    print_endline [%string "result: %{result#Int}"]
  ;;

  (* let%expect_test "part 2" = *)
  (*   f (Inputs.short_input ()); *)
  (*   [%expect {| result: 3993 |}]; *)
  (*   f (Inputs.real_input ()); *)
  (*   [%expect {| result: 4779 |}] *)
  (* ;; *)
end

let with_parsed f input =
  let parsed = Parsed.parse input in
  f parsed
;;

let command =
  Commands.both ~day:01 ~part1:(with_parsed Part1.f) ~part2:(with_parsed Part2.f)
;;
