open! Core
open! Std_internal

module Config = struct
  let day = "10"

  let short_input =
    {|[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]|}
  ;;

  let test_inputs = [||]
end

module Parsed = struct
  type t = char list list [@@deriving sexp]

  let parse_one s = String.to_list s
  let parse input = List.map input ~f:parse_one
end

module Inputs = struct
  let real_input () =
    Stdio.In_channel.read_lines [%string "../input/%{Config.day}.txt"] |> Parsed.parse
  ;;

  let short_input = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" short_input;
    [%expect
      {|
      (([ "(" { "(" < "(" "(" ")" ")" [ ] > [ [ { [ ] { < "(" ")" < > >)
       ([ "(" "(" ")" [ < > ] ")" ] "(" { [ < { < < [ ] > > "(")
       ({ "(" [ "(" < { } [ < > [ ] } > { [ ] { [ "(" < "(" ")" >)
       ("(" "(" "(" "(" { < > } < { < { < > } { [ ] { [ ] { })
       ([ [ < [ "(" [ ] ")" ")" < "(" [ [ { } [ [ "(" ")" ] ] ])
       ([ { [ { "(" { } ] { } } "(" [ { [ { { { } } "(" [ ])
       ({ < [ [ ] ] > } < { [ { [ { [ ] { "(" ")" [ [ [ ])
       ([ < "(" < "(" < "(" < { } ")" ")" > < "(" [ ] "(" [ ] "(" ")")
       (< { "(" [ "(" [ [ "(" < > "(" ")" ")" { } ] > "(" < < { {)
       (< { "(" [ { { } } [ < [ [ [ < > { } ] ] ] > [ ] ])) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

let is_open = function
  | '[' | '(' | '{' | '<' -> true
  | _ -> false
;;

let matches c c' =
  match c, c' with
  | '[', ']' | '(', ')' | '{', '}' | '<', '>' -> true
  | _ -> false
;;

let get_match = function
  | '[' -> ']'
  | '(' -> ')'
  | '{' -> '}'
  | '<' -> '>'
  | _ -> rip ()
;;

module Part1 = struct
  let points = function
    | ')' -> 3
    | ']' -> 57
    | '}' -> 1197
    | '>' -> 25137
    | _ -> 0
  ;;

  let check s =
    let stack = Deque.create () in
    let score = ref 0 in
    List.iter s ~f:(fun c ->
      if is_open c
      then Deque.enqueue_front stack c
      else (
        match Deque.dequeue_front stack with
        | None -> if !score = 0 then score := points c
        | Some c' -> if (not (matches c' c)) && !score = 0 then score := points c));
    !score
  ;;

  let f input =
    let result = List.map input ~f:check |> sumi in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 1" =
    f Inputs.short_input;
    [%expect {| result: 26397 |}];
    f (Inputs.real_input ());
    [%expect {| result: 265527 |}]
  ;;
end

module Part2 = struct
  let points = function
    | ')' -> 1
    | ']' -> 2
    | '}' -> 3
    | '>' -> 4
    | _ -> 0
  ;;

  let get_score = List.fold ~init:0 ~f:(fun accum next -> (accum * 5) + points next)

  let check s =
    let stack = Deque.create () in
    let score = ref 0 in
    List.iter s ~f:(fun c ->
      if is_open c
      then Deque.enqueue_front stack c
      else (
        match Deque.dequeue_front stack with
        | None -> if !score = 0 then score := points c
        | Some c' -> if (not (matches c' c)) && !score = 0 then score := points c));
    if !score > 0
    then None
    else Deque.to_list stack |> List.map ~f:get_match |> get_score |> Option.return
  ;;

  let%expect_test _ =
    let result = "}}]])})]" |> String.to_list |> get_score in
    Core.print_s [%message (result : int)];
    [%expect {| (result 288957) |}]
  ;;

  let f input =
    let result = List.filter_map input ~f:check |> List.sort ~compare:Int.compare in
    let result = List.nth_exn result (List.length result / 2) in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 2" =
    f Inputs.short_input;
    [%expect {| result: 288957 |}];
    f (Inputs.real_input ());
    [%expect {| result: 3969823589 |}]
  ;;
end
