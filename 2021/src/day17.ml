open! Core
open! Std_internal
open Graph

module Config = struct
  let day = "17"
  let short_input = {||}
  let test_inputs = [||]
end

module Parsed = struct
  type t = Coord.Set.t [@@deriving sexp]

  (* target area: x=207..263, y=-115..-63 *)
  let parse _ =
    Coord.range Coord.{ row = -115; col = 207 } Coord.{ row = -63; col = 263 }
    |> Coord.Set.of_list
  ;;
end

module Inputs = struct
  let real_input () =
    Stdio.In_channel.read_lines [%string "../input/%{Config.day}.txt"] |> Parsed.parse
  ;;

  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;
end

let run_one_step c v =
  let c' = Coord.add c v in
  let v' = { Coord.row = v.row - 1; col = Int.max 0 (v.col - 1) } in
  c', v'
;;

let m = { Coord.row = -115; col = 263 }

let simulate v t =
  let rec run c v max_y =
    let c', v' = run_one_step c v in
    if Coord.Set.mem t c'
    then Some (c', max_y)
    else if c'.col > m.col || c'.row < m.row
    then None
    else run c' v' (Int.max max_y c'.row)
  in
  run Coord.origin v 0
;;

let find_best t =
  List.init m.col ~f:Fn.id
  |> List.concat_map ~f:(fun col -> List.init 500 ~f:(fun row -> { Coord.row; col }))
  |> List.filter_map ~f:(fun v -> simulate v t)
  |> List.max_elt ~compare:(Comparable.lift Int.compare ~f:snd)
  |> Option.value_exn
;;

let find_tot t =
  List.init m.col ~f:(( + ) 1)
  |> List.concat_map ~f:(fun col ->
       List.init 500 ~f:(fun row -> { Coord.row = m.row + row; col }))
  |> List.filter_map ~f:(fun v -> simulate v t)
  |> List.length
;;

module Part1 = struct
  let f input =
    let result = find_best input |> snd in
    print_endline [%string "result: %{result#Int}"]
  ;;

  (* let%expect_test "part 1" = *)
  (*   f (Inputs.real_input ()); *)
  (*   [%expect {| result: 6555 |}] *)
  (* ;; *)
end

module Part2 = struct
  let f input =
    let result = find_tot input in
    print_endline [%string "result: %{result#Int}"]
  ;;

  (* let%expect_test "part 2" = *)
  (*   f (Inputs.real_input ()); *)
  (*   [%expect {| result: 4973 |}] *)
  (* ;; *)
end

let with_parsed f input =
  let parsed = Parsed.parse input in
  f parsed
;;

let command =
  Commands.both ~day:01 ~part1:(with_parsed Part1.f) ~part2:(with_parsed Part2.f)
;;
