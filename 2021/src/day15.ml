open! Core
open! Std_internal
open Graph

module Config = struct
  let day = "15"

  let short_input =
    {|1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581|}
  ;;

  let test_inputs = [||]
end

module Parsed = struct
  type t = int Grid.t [@@deriving sexp]

  let parse input = Grid.of_char_strings input ~f:ctoi
end

(* TODO: refactor into library *)
let neighbors grid c =
  List.map Direction.all ~f:Direction.deltas
  |> List.map ~f:(Coord.add c)
  |> List.filter ~f:(Grid.in_bounds grid)
;;

module Node = struct
  module T = struct
    type t = int * int * Coord.t * Coord.Set.t [@@deriving sexp]

    let compare (r1, c1, _, _) (r2, c2, _, _) = Int.compare (r1 + c1) (r2 + c2)
  end

  include T
  include Comparable.Make (T)
end

let dijkstra grid =
  let results =
    Grid.create
      ~width:grid.Grid.width
      ~height:grid.Grid.height
      (Int.max_value, Coord.origin)
  in
  let lower_right = { Coord.row = grid.Grid.height - 1; col = grid.width - 1 } in
  Grid.set_exn results Coord.origin (0, lower_right);
  let coords = Grid.coords grid in
  let unvisited = Coord.Hash_set.of_list coords in
  let to_consider = Coord.Hash_set.create () in
  Hash_set.add to_consider Coord.origin;
  let rec run () =
    let c, risk =
      Hash_set.to_list to_consider
      |> List.map ~f:(fun c -> c, Grid.get_exn results c |> fst)
      |> List.min_elt ~compare:(Comparable.lift Int.compare ~f:snd)
      |> Option.value_exn
    in
    if Coord.equal c lower_right
    then risk
    else (
      let neighbors = neighbors grid c |> List.filter ~f:(Hash_set.mem unvisited) in
      List.iter neighbors ~f:(fun n ->
        let r = Grid.get_exn grid n in
        if Grid.get_exn results n |> fst > risk + r
        then (
          Grid.set_exn results n (risk + r, c);
          Hash_set.add to_consider n));
      Hash_set.remove unvisited c;
      Hash_set.remove to_consider c;
      run ())
  in
  run ()
;;

module Inputs = struct
  let real_input () =
    Stdio.In_channel.read_lines [%string "../input/%{Config.day}.txt"] |> Parsed.parse
  ;;

  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect
      {|
      ((height 10) (width 10)
       (grid
        ((1 1 6 3 7 5 1 7 4 2) (1 3 8 1 3 7 3 6 7 2) (2 1 3 6 5 1 1 3 2 8)
         (3 6 9 4 9 3 1 5 6 9) (7 4 6 3 4 1 7 1 1 1) (1 3 1 9 1 2 8 1 3 7)
         (1 3 5 9 9 1 2 4 2 1) (3 1 2 5 4 2 1 6 3 9) (1 2 9 3 1 3 8 5 2 1)
         (2 3 1 1 9 4 4 5 8 1)))) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

module Part1 = struct
  let run input =
    List.zip_exn (List.drop_last_exn input) (List.tl_exn input)
    |> List.count ~f:(fun (a, b) -> b > a)
  ;;

  let f input =
    let result = dijkstra input in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 1" =
    f (Inputs.short_input ());
    [%expect {| result: 40 |}];
    f (Inputs.real_input ());
    [%expect {| result: 429 |}]
  ;;
end

module Part2 = struct
  let tile_grid grid =
    let new_grid = Grid.create ~width:(grid.Grid.width * 5) ~height:(grid.height * 5) 0 in
    Grid.iteri grid ~f:(fun { row; col } i ->
      for rd = 0 to 4 do
        for cd = 0 to 4 do
          let row = row + (rd * grid.height) in
          let col = col + (cd * grid.width) in
          let n =
            let res = i + rd + cd in
            let rec fix res =
              if res = 0 then 1 else if res <= 9 then res else fix (res - 9)
            in
            fix res
          in
          Grid.set_exn new_grid { Coord.row; col } n
        done
      done);
    new_grid
  ;;

  let%expect_test "tile_grid" =
    let grid = Parsed.parse [ "8" ] in
    let new_grid = tile_grid grid in
    Core.print_endline (Grid.to_string new_grid ~to_char:itoc);
    [%expect {|
      89123
      91234
      12345
      23456
      34567 |}];
    let grid = Parsed.parse [ "89"; "78" ] in
    let new_grid = tile_grid grid in
    Core.print_endline (Grid.to_string new_grid ~to_char:itoc);
    [%expect
      {|
      8991122334
      7889911223
      9112233445
      8991122334
      1223344556
      9112233445
      2334455667
      1223344556
      3445566778
      2334455667 |}]
  ;;

  let f input =
    let new_grid = tile_grid input in
    let result = dijkstra new_grid in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 2" =
    f (Inputs.short_input ());
    [%expect {| result: 315 |}];
    f (Inputs.real_input ());
    [%expect {| result: 2844 |}]
  ;;
end
