open! Core
open! Std_internal
open Graph

module Config = struct
  let day = "09"

  let short_input = {|2199943210
3987894921
9856789892
8767896789
9899965678|}

  let test_inputs = [||]
end

module Parsed = struct
  type t = int Grid.t [@@deriving sexp]

  let parse input =
    Grid.of_char_strings input ~f:(fun c -> Int.of_string (sprintf "%c" c))
  ;;
end

module Inputs = struct
  let real_input () =
    Stdio.In_channel.read_lines [%string "../input/%{Config.day}.txt"] |> Parsed.parse
  ;;

  let short_input = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" short_input;
    [%expect {|
      ((height 5) (width 10)
       (grid
        ((2 1 9 9 9 4 3 2 1 0) (3 9 8 7 8 9 4 9 2 1) (9 8 5 6 7 8 9 8 9 2)
         (8 7 6 7 8 9 6 7 8 9) (9 8 9 9 9 6 5 6 7 8)))) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

(* TODO: Move to library *)
let neighbors grid c =
  List.map Direction.all ~f:Direction.deltas
  |> List.map ~f:(Coord.add c)
  |> List.filter ~f:(Grid.in_bounds grid)
;;

let is_low_point grid c v =
  List.for_all (neighbors grid c) ~f:(fun c' ->
    match Grid.get grid c' with
    | None -> true
    | Some v' -> v' > v)
;;

let low_points grid =
  Grid.foldi grid ~init:[] ~f:(fun c accum v ->
    if is_low_point grid c v then c :: accum else accum)
;;

module Part1 = struct
  let f input =
    let result =
      low_points input |> List.map ~f:(Grid.get_exn input) |> List.reduce_exn ~f:( + )
    in
    print_endline [%string "%{result#Int}"]
  ;;

  let%expect_test "part 1" =
    f Inputs.short_input;
    [%expect {| 11 |}];
    f (Inputs.real_input ());
    [%expect {| 281 |}]
  ;;
end

module Part2 = struct
  let f grid =
    let low_points = low_points grid in
    let new_grid = Grid.create ~width:grid.width ~height:grid.height (-1) in
    let queue = Deque.create () in
    let basins =
      Int.Table.of_alist_exn
        (List.mapi low_points ~f:(fun i c -> i, Coord.Set.singleton c))
    in
    List.iteri low_points ~f:(fun i c ->
      Grid.set_exn new_grid c i;
      List.iter (neighbors grid c) ~f:(fun c ->
        if Grid.get_exn grid c <> 9 then Deque.enqueue_back queue c));
    let rec flood () =
      match Deque.dequeue_front queue with
      | None -> ()
      | Some c ->
        let v = Grid.get_exn grid c in
        let b = Grid.get_exn new_grid c in
        if b <> -1 || v = 9
        then flood ()
        else (
          let neighbors = neighbors grid c in
          let b =
            List.find_map neighbors ~f:(fun c' ->
              let v' = Grid.get_exn grid c' in
              let b' = Grid.get_exn new_grid c' in
              if b' <> -1 && v' < v then Some b' else None)
          in
          match b with
          | None -> flood ()
          | Some b ->
            Int.Table.update basins b ~f:(function
              | None -> Coord.Set.singleton c
              | Some s -> Coord.Set.add s c);
            Grid.set_exn new_grid c b;
            List.iter neighbors ~f:(fun c ->
              if Grid.get_exn new_grid c = -1 && Grid.get_exn grid c <> 9
              then Deque.enqueue_back queue c);
            flood ())
    in
    flood ();
    let basins =
      Int.Table.to_alist basins
      |> List.map ~f:(fun (_, b) -> Coord.Set.length b)
      |> List.sort ~compare:Int.compare
      |> List.rev
    in
    let a, b, c = slice_list basins 0 3 |> e3 in
    let result = a * b * c in
    print_endline [%string "%{result#Int}"]
  ;;

  let%expect_test "part 2" =
    f Inputs.short_input;
    [%expect {| 1134 |}];
    f (Inputs.real_input ());
    [%expect {| 1071000 |}]
  ;;
end
