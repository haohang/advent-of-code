open! Core
open! Std_internal

module Config = struct
  let day = "12"

  let short_input = {|start-A
start-b
A-c
A-b
b-d
A-end
b-end|}

  let test_inputs = [||]
end

module Parsed = struct
  type t = string list String.Map.t [@@deriving sexp]

  let parse_one s =
    let a, b = String.split s ~on:'-' |> e2 in
    [ a, b; b, a ]
  ;;

  let parse input = List.concat_map input ~f:parse_one |> String.Map.of_alist_multi
end

module Inputs = struct
  let real_input () =
    Stdio.In_channel.read_lines [%string "../input/%{Config.day}.txt"] |> Parsed.parse
  ;;

  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect {|
      ((A (start c b end)) (b (start A d end)) (c (A)) (d (b)) (end (A b))
       (start (A b))) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

let is_big s = String.equal (String.uppercase s) s

let paths edges =
  let queue = Deque.create () in
  Deque.enqueue_front queue ("start", [ "start" ]);
  let num = ref 0 in
  let paths = ref [] in
  let rec run () =
    match Deque.dequeue_front queue with
    | None -> !num, !paths
    | Some (node, seen) ->
      if String.equal node "end"
      then (
        incr num;
        paths := List.rev seen :: !paths)
      else (
        let next = String.Map.find edges node |> Option.value ~default:[] in
        List.iter next ~f:(fun next ->
          if (not (is_big next)) && List.mem seen next ~equal:String.equal
          then ()
          else Deque.enqueue_back queue (next, next :: seen)));
      run ()
  in
  run ()
;;

module Part1 = struct
  let f input =
    let result = paths input |> fst in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 1" =
    f (Inputs.short_input ());
    [%expect {| result: 10 |}];
    f (Inputs.real_input ());
    [%expect {| result: 3679 |}]
  ;;
end

module Part2 = struct
  let paths_dumb edges =
    let queue = Deque.create () in
    Deque.enqueue_front queue ("start", [ "start" ], false);
    let num = ref 0 in
    let paths = ref [] in
    let rec run () =
      match Deque.dequeue_front queue with
      | None -> !num, !paths
      | Some (node, seen, duplicated) ->
        if String.equal node "end"
        then (
          incr num;
          paths := List.rev seen :: !paths)
        else (
          let next = String.Map.find edges node |> Option.value ~default:[] in
          List.iter next ~f:(fun next ->
            if String.equal next "start"
            then ()
            else (
              let count = List.count seen ~f:(String.equal next) in
              let is_big = is_big next in
              if is_big || count = 0 || (count = 1 && not duplicated)
              then
                Deque.enqueue_back
                  queue
                  (next, next :: seen, duplicated || (count = 1 && not is_big))
              else ())));
        run ()
    in
    run ()
  ;;

  let f input =
    let result, _paths = paths_dumb input in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 2" =
    f (Inputs.short_input ());
    [%expect {| result: 36 |}];
    f (Inputs.real_input ());
    [%expect {| result: 107395 |}]
  ;;
end
