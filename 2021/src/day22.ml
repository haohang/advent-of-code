open! Core
open! Std_internal
open Graph

module Config = struct
  let day = 22

  let short_input =
    {|on x=10..12,y=10..12,z=10..12
on x=11..13,y=11..13,z=11..13
off x=9..11,y=9..11,z=9..11
on x=10..10,y=10..10,z=10..10|}
  ;;

  let test_inputs = [||]
end

module Parsed = struct
  type one =
    [ `on of NCoord.t * NCoord.t
    | `off of NCoord.t * NCoord.t
    ]
  [@@deriving sexp]

  type t = one list [@@deriving sexp]

  let parse_one s =
    let i, r = sps s |> e2 in
    let x, y, z = spc r |> e3 in
    let f e =
      let _, r = String.split e ~on:'=' |> e2 in
      let a, _, b = String.split r ~on:'.' |> e3 in
      let a = Int.of_string a in
      let b = Int.of_string b in
      a, b
    in
    let x = f x in
    let y = f y in
    let z = f z in
    let a, b = List.unzip [ x; y; z ] in
    let a = NCoord.create_exn ~dim:3 a in
    let b = NCoord.create_exn ~dim:3 b in
    match i with
    | "on" -> `on (a, b)
    | "off" -> `off (a, b)
    | _ -> rip ()
  ;;

  let parse input = List.map input ~f:parse_one
end

module Inputs = struct
  let real_input () =
    Stdio.In_channel.read_lines (sprintf "../input/%02d.txt" Config.day) |> Parsed.parse
  ;;

  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect {|
      ((on (((dim 3) (coords (10 10 10))) ((dim 3) (coords (12 12 12)))))
       (on (((dim 3) (coords (11 11 11))) ((dim 3) (coords (13 13 13)))))
       (off (((dim 3) (coords (9 9 9))) ((dim 3) (coords (11 11 11)))))
       (on (((dim 3) (coords (10 10 10))) ((dim 3) (coords (10 10 10)))))) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

module Cuboid = struct
  module T = struct
    type t = NCoord.t * NCoord.t [@@deriving sexp, compare, hash]
  end

  include T
  include Hashable.Make (T)
end

let min c c' =
  List.zip_exn (NCoord.coords c) (NCoord.coords c')
  |> List.map ~f:(fun (a, b) -> Int.min a b)
  |> NCoord.create_exn ~dim:3
;;

let max c c' =
  List.zip_exn (NCoord.coords c) (NCoord.coords c')
  |> List.map ~f:(fun (a, b) -> Int.max a b)
  |> NCoord.create_exn ~dim:3
;;

let contains (ca, cb) p =
  List.zip_exn (NCoord.coords ca) (NCoord.coords cb)
  |> List.zip_exn (NCoord.coords p)
  |> List.for_all ~f:(fun (p, (a, b)) -> a <= p && p <= b)
;;

let is_valid_cuboid c c' =
  let m = min c c' in
  NCoord.equal m c
;;

let intersect ((ca, cb) as _c) ((ca', cb') as _c') =
  let o1 = is_valid_cuboid ca cb' in
  let o2 = is_valid_cuboid ca' cb in
  match o1, o2 with
  | false, _ | _, false -> None
  | true, true -> Some (max ca ca', min cb cb')
;;

let size (ca, cb) =
  List.zip_exn (NCoord.coords ca) (NCoord.coords cb)
  |> List.map ~f:(fun (c, c') -> c' - c + 1)
  |> List.reduce_exn ~f:( * )
;;

let%expect_test _ =
  let c (x, y, z) = NCoord.create_exn ~dim:3 [ x; y; z ] in
  let i (ca, cb) (ca', cb') =
    Core.printf
      !"%{sexp: (NCoord.t * NCoord.t) option}\n%!"
      (intersect (c ca, c cb) (c ca', c cb'))
  in
  i ((0, 0, 0), (10, 10, 10)) ((5, 5, 5), (15, 15, 15));
  [%expect {| ((((dim 3) (coords (5 5 5))) ((dim 3) (coords (10 10 10))))) |}];
  i ((5, 5, 5), (15, 15, 15)) ((0, 0, 0), (10, 10, 10));
  [%expect {| ((((dim 3) (coords (5 5 5))) ((dim 3) (coords (10 10 10))))) |}];
  i ((5, 5, 5), (10, 10, 10)) ((0, 0, 0), (10, 10, 10));
  [%expect {| ((((dim 3) (coords (5 5 5))) ((dim 3) (coords (10 10 10))))) |}];
  i ((5, 5, 5), (10, 10, 10)) ((11, 11, 11), (12, 12, 12));
  [%expect {| () |}];
  let s (ca, cb) = Core.printf "%d\n%!" (size (c ca, c cb)) in
  s ((0, 0, 0), (5, 5, 5));
  [%expect {| 216 |}]
;;

let combine input =
  let cuboids = Cuboid.Table.create () in
  let add key value =
    Cuboid.Table.update cuboids key ~f:(function
      | None -> value
      | Some i -> i + value)
  in
  List.iter input ~f:(fun s ->
    let intersects s =
      Cuboid.Table.fold cuboids ~init:[] ~f:(fun ~key:c ~data:sign accum ->
        match intersect s c with
        | None -> accum
        | Some i -> (i, -sign) :: accum)
      |> List.iter ~f:(fun (key, sign) -> add key sign)
    in
    match s with
    | `on s ->
      intersects s;
      add s 1
    | `off s -> intersects s);
  Cuboid.Table.to_alist cuboids
  |> List.map ~f:(fun (c, s) -> s * size c)
  |> List.reduce_exn ~f:( + )
;;

module Part1 = struct
  let f input =
    let range =
      NCoord.create_exn ~dim:3 [ -50; -50; -50 ], NCoord.create_exn ~dim:3 [ 50; 50; 50 ]
    in
    let input =
      List.filter_map input ~f:(fun next ->
        let s =
          match next with
          | `on s | `off s -> s
        in
        Option.map (intersect range s) ~f:(fun s ->
          match next with
          | `on _ -> `on s
          | `off _ -> `off s))
    in
    let result = combine input in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 1" =
    f (Inputs.short_input ());
    [%expect {| result: 39 |}];
    f (Inputs.real_input ());
    [%expect {| result: 615869 |}]
  ;;
end

module Part2 = struct
  let f input =
    let result = combine input in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 2" =
    f (Inputs.short_input ());
    [%expect {| result: 39 |}];
    f (Inputs.real_input ());
    [%expect {| result: 1323862415207825 |}]
  ;;
end

let with_parsed f input =
  let parsed = Parsed.parse input in
  f parsed
;;

let command =
  Commands.both ~day:Config.day ~part1:(with_parsed Part1.f) ~part2:(with_parsed Part2.f)
;;
