open! Core
open! Std_internal

module Config = struct
  let day = "02"

  let short_input = {|forward 5
down 5
forward 8
up 3
down 8
forward 2|}

  let test_inputs = [||]
end

module Parsed = struct
  type one =
    | Forward of int
    | Down of int
    | Up of int
  [@@deriving sexp]

  type t = one list [@@deriving sexp]

  let parse_one s =
    match String.split s ~on:' ' with
    | [ "forward"; n ] -> Forward (Int.of_string n)
    | [ "down"; n ] -> Down (Int.of_string n)
    | [ "up"; n ] -> Up (Int.of_string n)
    | _ -> raise_s [%message "unparseable"]
  ;;

  let parse input = List.map input ~f:parse_one
end

module Inputs = struct
  let real_input () =
    Stdio.In_channel.read_lines [%string "../input/%{Config.day}.txt"] |> Parsed.parse
  ;;

  let short_input = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" short_input;
    [%expect {| ((Forward 5) (Down 5) (Forward 8) (Up 3) (Down 8) (Forward 2)) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

module Part1 = struct
  let run instructions =
    List.fold instructions ~init:(0, 0) ~f:(fun (horiz, vert) i ->
      match i with
      | Parsed.Forward n -> horiz + n, vert
      | Down n -> horiz, vert + n
      | Up n -> horiz, vert - n)
  ;;

  let f input =
    let h, v = run input in
    let result = h * v in
    printf !"%{sexp: int}\n%!" result
  ;;

  let%expect_test "part 1" =
    f Inputs.short_input;
    [%expect {| 150 |}];
    f (Inputs.real_input ());
    [%expect {| 2070300 |}]
  ;;
end

module Part2 = struct
  let run instructions =
    List.fold instructions ~init:(0, 0, 0) ~f:(fun (horiz, vert, aim) i ->
      match i with
      | Parsed.Forward n -> horiz + n, vert + (aim * n), aim
      | Down n -> horiz, vert, aim + n
      | Up n -> horiz, vert, aim - n)
  ;;

  let f input =
    let h, v, _a = run input in
    let result = h * v in
    printf !"%{sexp: int}\n%!" result
  ;;

  let%expect_test "part 2" =
    f Inputs.short_input;
    [%expect {| 900 |}];
    f (Inputs.real_input ());
    [%expect {| 2078985210 |}]
  ;;
end
