open! Core
open! Std_internal
open Graph

module Config = struct
  let day = "11"

  let short_input =
    {|5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526|}
  ;;

  let test_inputs = [||]
end

module Parsed = struct
  type t = int Grid.t [@@deriving sexp]

  let parse input = Grid.of_char_strings input ~f:ctoi
end

module Inputs = struct
  let real_input () =
    Stdio.In_channel.read_lines [%string "../input/%{Config.day}.txt"] |> Parsed.parse
  ;;

  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect
      {|
      ((height 10) (width 10)
       (grid
        ((5 4 8 3 1 4 3 2 2 3) (2 7 4 5 8 5 4 7 1 1) (5 2 6 4 5 5 6 1 7 3)
         (6 1 4 1 3 3 6 1 4 6) (6 3 5 7 3 8 5 4 7 8) (4 1 6 7 5 2 4 6 4 5)
         (2 1 7 6 8 4 1 7 2 1) (6 8 8 2 8 8 1 1 3 4) (4 8 4 6 8 4 8 5 5 4)
         (5 2 8 3 7 5 1 5 2 6)))) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

(* TODO: Move to library *)
let neighbors grid c =
  List.cartesian_product [ -1; 0; 1 ] [ -1; 0; 1 ]
  |> List.map ~f:(fun (row, col) -> { Coord.row; col })
  |> List.map ~f:(fun c' -> Coord.add c c')
  |> List.filter ~f:(fun c' -> Grid.in_bounds grid c')
;;

let flash grid c =
  let neighbors = neighbors grid c in
  List.iter neighbors ~f:(fun c' ->
    let old = Grid.get_exn grid c' in
    Grid.set_exn grid c' (old + 1))
;;

let run_one_step grid =
  Grid.iteri grid ~f:(fun c a -> Grid.set_exn grid c (a + 1));
  let flashed = Coord.Hash_set.create () in
  let rec run () =
    let to_flash =
      Grid.foldi ~init:[] grid ~f:(fun c accum a ->
        if a >= 10 && not (Hash_set.mem flashed c) then c :: accum else accum)
    in
    List.iter to_flash ~f:(fun c ->
      flash grid c;
      Hash_set.add flashed c);
    if List.is_empty to_flash then () else run ()
  in
  run ();
  Grid.iteri grid ~f:(fun c a -> if a >= 10 then Grid.set_exn grid c 0);
  Hash_set.length flashed
;;

let%expect_test _ =
  let grid = Inputs.short_input () in
  let result = run_one_step grid in
  let print result =
    Core.printf "%d\n%s\n%!" result (Grid.to_string grid ~to_char:itoc)
  in
  print result;
  [%expect
    {|
    0
    6594254334
    3856965822
    6375667284
    7252447257
    7468496589
    5278635756
    3287952832
    7993992245
    5957959665
    6394862637 |}];
  let result = run_one_step grid in
  print result;
  [%expect
    {|
    35
    8807476555
    5089087054
    8597889608
    8485769600
    8700908800
    6600088989
    6800005943
    0000007456
    9000000876
    8700006848 |}]
;;

let run grid ~n = Fn.apply_n_times ~n (fun acc -> acc + run_one_step grid) 0

module Part1 = struct
  let f input =
    let result = run input ~n:100 in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 1" =
    f (Inputs.short_input ());
    [%expect {| result: 1656 |}];
    f (Inputs.real_input ());
    [%expect {| result: 1719 |}]
  ;;
end

module Part2 = struct
  let f input =
    let size = input.Grid.height * input.width in
    let rec find i =
      let res = run_one_step input in
      if res = size then i + 1 else find (i + 1)
    in
    let result = find 0 in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 2" =
    f (Inputs.short_input ());
    [%expect {| result: 195 |}];
    f (Inputs.real_input ());
    [%expect {| result: 232 |}]
  ;;
end
