open! Core
open! Std_internal
open Graph

module Config = struct
  let day = 20

  let short_input =
    {|..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

#..#.
#....
##..#
..#..
..###|}
  ;;

  let test_inputs = [||]
end

module Parsed = struct
  type t = char Array.t * Coord.Set.t * int Grid.t [@@deriving sexp]

  let parse input =
    let rules, grid = coagulate input |> e2 in
    let rules = e1 rules |> String.to_list |> Array.of_list in
    let grid =
      Grid.of_char_strings grid ~f:(function
        | '#' -> 1
        | '.' -> 0
        | _ -> rip ())
    in
    let ones =
      Grid.foldi grid ~init:Coord.Set.empty ~f:(fun c accum v ->
        if v = 1 then Coord.Set.add accum c else accum)
    in
    rules, ones, grid
  ;;
end

module Inputs = struct
  let real_input () =
    Stdio.In_channel.read_lines (sprintf "../input/%02d.txt" Config.day) |> Parsed.parse
  ;;

  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect
      {|
      ((. . # . # . . # # # # # . # . # . # . # # # . # # . . . . . # # # . # # . #
        . . # # # . # # # # . . # # # # # . . # . . . . # . . # . . # # . . # # # .
        . # # # # # # . # # # . . . # # # # . . # . . # # # # # . . # # . . # . # #
        # # # . . . # # . # . # . . # . # # . . # . # . . . . . . # . # # # . # # #
        # # # . # # # . # # # # . . . # . # # . # # . . # . . # . . # # # # # . . .
        . . # . # . . . . # # # . . # . # # . . . . . . # . . . . . # . . # . . # .
        . # # . . # . . . # # . # # # # # # . # # # # . # # # # . # . # . . . # . .
        . . . . . # . . # . # . # . . . # # # # . # # . # . . . . . . # . . # . . .
        # # . # . # # . . # . . . # # . # . # # . . # # # . # . . . . . . # . # . .
        . . . . . # . # . # . # # # # . # # # . # # . . . # . . . . . # # # # . # .
        . # . . # . # # . # . . . . # # . . # . # # # # . . . . # # . . . # # . . #
        . . . # . . . . . . # . # . . . . . . . # . . . . . . . # # . . # # # # . .
        # . . . # . # . # . . . # # . . # . # . . # # # . . # # # # # . . . . . . .
        . # . . # # # # . . . . . . # . . #)
       (((row 0) (col 0)) ((row 0) (col 3)) ((row 1) (col 0)) ((row 2) (col 0))
        ((row 2) (col 1)) ((row 2) (col 4)) ((row 3) (col 2)) ((row 4) (col 2))
        ((row 4) (col 3)) ((row 4) (col 4)))
       ((height 5) (width 5)
        (grid ((1 0 0 1 0) (1 0 0 0 0) (1 1 0 0 1) (0 0 1 0 0) (0 0 1 1 1))))) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

(* TODO: refactor to library *)
let neighbors c =
  [ -1, -1; -1, 0; -1, 1; 0, -1; 0, 0; 0, 1; 1, -1; 1, 0; 1, 1 ]
  |> List.map ~f:(fun (row, col) -> { Coord.row = c.Coord.row + row; col = c.col + col })
;;

let enhance rules ones grid ~n =
  let rows = grid.Grid.height in
  let cols = grid.width in
  let bufs = 2 * n in
  let to_consider =
    List.init
      (rows + (bufs * 2))
      ~f:(fun row ->
        List.init
          (cols + (bufs * 2))
          ~f:(fun col -> { Coord.row = row - bufs; col = col - bufs }))
    |> List.concat
    |> Coord.Set.of_list
  in
  let enhance ones ~default =
    Coord.Set.fold to_consider ~init:Coord.Set.empty ~f:(fun accum c ->
      let idx =
        neighbors c
        |> List.map ~f:(fun c ->
             if Coord.Set.mem ones c
             then 1
             else if Coord.Set.mem to_consider c
             then 0
             else default)
      in
      (* Core.print_s [%message (c : Coord.t) (idx : int list)]; *)
      let idx = binary_digits_to_decimal idx in
      if Char.equal rules.(idx) '#' then Coord.Set.add accum c else accum)
  in
  let _print ones =
    for row = 0 to rows + (bufs * 2) do
      for col = 0 to cols + (bufs * 2) do
        let c = { Coord.row = row - bufs; col = col - bufs } in
        if Coord.Set.mem ones c then Core.printf "#" else Core.printf "."
      done;
      Core.printf "\n"
    done;
    Core.printf "\n"
  in
  let ones, _default =
    Fn.apply_n_times
      ~n
      (fun (ones, default) ->
        let res = enhance ones ~default in
        (* _print res; *)
        res, (default + 1) % 2)
      (ones, 0)
  in
  Coord.Set.length ones
;;

module Part1 = struct
  let f input =
    let rules, ones, grid = input in
    let result = enhance rules ones grid ~n:2 in
    print_endline [%string "result: %{result#Int}"]
  ;;

  (* TODO: short input is producing wrong results *)
  let%expect_test "part 1" =
    f (Inputs.short_input ());
    [%expect {| result: 59 |}];
    f (Inputs.real_input ());
    [%expect {| result: 5354 |}]
  ;;
end

module Part2 = struct
  let f input =
    let rules, ones, grid = input in
    let result = enhance rules ones grid ~n:50 in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 2" =
    f (Inputs.short_input ());
    [%expect {| result: 11302 |}];
    f (Inputs.real_input ());
    [%expect {| result: 18269 |}]
  ;;
end

let with_parsed f input =
  let parsed = Parsed.parse input in
  f parsed
;;

let command =
  Commands.both ~day:Config.day ~part1:(with_parsed Part1.f) ~part2:(with_parsed Part2.f)
;;
