open! Core
open! Std_internal

module Config = struct
  let day = "04"

  let short_input =
    {|7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7|}
  ;;

  let test_inputs = [||]
end

module Parsed = struct
  type board = int list list [@@deriving sexp]
  type t = int list * board list [@@deriving sexp]

  let parse input =
    match coagulate input with
    | numbers :: boards ->
      let numbers = exactly_one_exn numbers in
      let numbers = String.split numbers ~on:',' |> List.map ~f:Int.of_string in
      let boards =
        List.map
          boards
          ~f:
            (List.map ~f:(fun s ->
               String.split s ~on:' '
               |> List.filter_map ~f:(fun s ->
                    if String.is_empty s then None else Some (Int.of_string s))))
      in
      numbers, boards
    | _ -> raise_s [%message "Unparseable"]
  ;;
end

module Inputs = struct
  let real_input () =
    Stdio.In_channel.read_lines [%string "../input/%{Config.day}.txt"] |> Parsed.parse
  ;;

  let short_input = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" short_input;
    [%expect
      {|
      ((7 4 9 5 11 17 23 2 0 14 21 24 10 16 13 6 15 25 12 22 18 20 8 19 3 26 1)
       (((22 13 17 11 0) (8 2 23 4 24) (21 9 14 16 7) (6 10 3 18 5)
         (1 12 20 15 19))
        ((3 15 0 2 22) (9 18 13 17 5) (19 8 7 25 23) (20 11 10 24 4)
         (14 21 16 12 6))
        ((14 21 17 24 4) (10 16 15 9 19) (18 8 23 26 20) (22 11 13 6 5)
         (2 0 12 3 7)))) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

let play_one ~numbers ~board =
  let find n =
    List.find_mapi board ~f:(fun r row ->
      List.find_mapi row ~f:(fun c elt -> if elt = n then Some (r, c) else None))
  in
  let coords = List.map numbers ~f:find in
  let result =
    List.fold_until
      ~init:(Int.Map.empty, Int.Map.empty, [], 0)
      coords
      ~f:(fun (by_row, by_col, seen, total) next ->
        match next with
        | None -> Continue (by_row, by_col, seen, total + 1)
        | Some ((r, c) as next) ->
          let by_row =
            Int.Map.update by_row r ~f:(function
              | None -> 1
              | Some i -> i + 1)
          in
          let by_col =
            Int.Map.update by_col c ~f:(function
              | None -> 1
              | Some i -> i + 1)
          in
          let num_row = Int.Map.find_exn by_row r in
          let num_col = Int.Map.find_exn by_col c in
          let seen = next :: seen in
          if num_row = List.length board || num_col = List.length board
          then Stop (`Found (total + 1, seen))
          else Continue (by_row, by_col, seen, total + 1))
      ~finish:(fun _ -> `Not_found)
  in
  match result with
  | `Not_found -> None
  | `Found (total, seen) ->
    let sum =
      List.concat_mapi board ~f:(fun r row -> List.mapi row ~f:(fun c elt -> (r, c), elt))
      |> List.fold ~init:0 ~f:(fun acc (coord, elt) ->
           if List.mem seen ~equal:[%compare.equal: int * int] coord
           then acc
           else acc + elt)
    in
    let r, c = List.hd_exn seen in
    let last = List.nth_exn (List.nth_exn board r) c in
    Some (total, last * sum)
;;

module Part1 = struct
  let f (numbers, boards) =
    let results = List.filter_map boards ~f:(fun board -> play_one ~numbers ~board) in
    let result =
      List.min_elt results ~compare:(Comparable.lift Int.compare ~f:fst)
      |> Option.value_exn
      |> snd
    in
    printf !"%{sexp: int}\n%!" result
  ;;

  let%expect_test "part 1" =
    f Inputs.short_input;
    [%expect {| 4512 |}];
    f (Inputs.real_input ());
    [%expect {| 72770 |}]
  ;;
end

module Part2 = struct
  let f (numbers, boards) =
    let results = List.filter_map boards ~f:(fun board -> play_one ~numbers ~board) in
    let result =
      List.max_elt results ~compare:(Comparable.lift Int.compare ~f:fst)
      |> Option.value_exn
      |> snd
    in
    printf !"%{sexp: int}\n%!" result
  ;;

  let%expect_test "part 2" =
    f Inputs.short_input;
    [%expect {| 1924 |}];
    f (Inputs.real_input ());
    [%expect {| 13912 |}]
  ;;
end
