open! Core
open! Std_internal
open Graph

let parse input = Grid.of_strings input

let run_one grid =
  (* Move east *)
  let to_move = ref [] in
  Grid.iteri grid ~f:(fun coord elt ->
    match elt with
    | '.' | 'v' -> ()
    | '>' ->
      let next =
        let cand = { coord with col = coord.Coord.col + 1 } in
        if Grid.in_bounds grid cand then cand else { coord with col = 0 }
      in
      if Char.equal (Grid.get_exn grid next) '.' then to_move := (coord, next) :: !to_move
    | _ -> rip ());
  List.iter !to_move ~f:(fun (prev, next) ->
    Grid.set_exn grid prev '.';
    Grid.set_exn grid next '>');
  (* Move south *)
  let to_move1 = ref [] in
  Grid.iteri grid ~f:(fun coord elt ->
    match elt with
    | '.' | '>' -> ()
    | 'v' ->
      let next =
        let cand = { coord with row = coord.Coord.row + 1 } in
        if Grid.in_bounds grid cand then cand else { coord with row = 0 }
      in
      if Char.equal (Grid.get_exn grid next) '.'
      then to_move1 := (coord, next) :: !to_move1
    | _ -> rip ());
  List.iter !to_move1 ~f:(fun (prev, next) ->
    Grid.set_exn grid prev '.';
    Grid.set_exn grid next 'v');
  List.length !to_move + List.length !to_move1
;;

module Part1 = struct
  let f input =
    let input = parse input in
    let rec run i =
      let res = run_one input in
      (* Core.printf "%s\n\n%!" (Grid.to_string input ~to_char:Fn.id); *)
      if res = 0 then i else run (i + 1)
    in
    let result = run 1 in
    printf !"%{sexp: int}\n%!" result
  ;;
end

module Part2 = struct
  let f input =
    let input = parse input in
    let result = input.Grid.height in
    printf !"%{sexp: int}\n%!" result
  ;;
end

let command = Commands.both ~day:25 ~part1:Part1.f ~part2:Part2.f
