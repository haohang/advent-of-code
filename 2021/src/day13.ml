open! Core
open! Std_internal
open Graph

module Config = struct
  let day = "13"

  let short_input =
    {|6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5|}
  ;;

  let test_inputs = [||]
end

module Parsed = struct
  type t = Coord.Set.t * (string * int) list [@@deriving sexp]

  let parse input =
    let a, b = coagulate input |> e2 in
    let coords =
      List.map a ~f:(fun a ->
        let col, row = spci a |> e2 in
        Coord.{ row; col })
    in
    let instructions =
      List.map b ~f:(fun b ->
        let axis, num = String.split b ~on:'=' |> e2 in
        axis, stoi num)
    in
    Coord.Set.of_list coords, instructions
  ;;
end

module Inputs = struct
  let real_input () =
    Stdio.In_channel.read_lines [%string "../input/%{Config.day}.txt"] |> Parsed.parse
  ;;

  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect
      {|
      ((((row 0) (col 3)) ((row 0) (col 6)) ((row 0) (col 9)) ((row 1) (col 4))
        ((row 3) (col 0)) ((row 4) (col 3)) ((row 4) (col 8)) ((row 4) (col 10))
        ((row 10) (col 1)) ((row 10) (col 6)) ((row 10) (col 8)) ((row 10) (col 9))
        ((row 11) (col 4)) ((row 12) (col 6)) ((row 12) (col 10))
        ((row 13) (col 0)) ((row 14) (col 0)) ((row 14) (col 2)))
       (("fold along y" 7) ("fold along x" 5))) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

let apply_fold coords (axis, num) =
  let apply_fold_one_coord { Coord.row; col } =
    let row, col =
      match axis with
      | "fold along x" -> row, if col > num then num - (col - num) else col
      | "fold along y" -> (if row > num then num - (row - num) else row), col
      | _ -> rip ()
    in
    { Coord.row; col }
  in
  Coord.Set.fold ~init:Coord.Set.empty coords ~f:(fun accum c ->
    Coord.Set.add accum (apply_fold_one_coord c))
;;

module Part1 = struct
  let f input =
    let coords, folds = input in
    let result = apply_fold coords (List.hd_exn folds) |> Coord.Set.length in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 1" =
    f (Inputs.short_input ());
    [%expect {| result: 17 |}];
    f (Inputs.real_input ());
    [%expect {| result: 731 |}]
  ;;
end

module Part2 = struct
  let f input =
    let coords, folds = input in
    let coords = List.fold ~init:coords folds ~f:(fun accum f -> apply_fold accum f) in
    let height, width =
      Coord.Set.fold ~init:(0, 0) coords ~f:(fun (r, col) c ->
        let row = Int.max c.row r in
        let col = Int.max c.col col in
        row, col)
    in
    let grid = Grid.create ~width:(width + 1) ~height:(height + 1) '_' in
    Coord.Set.iter coords ~f:(fun c -> Grid.set_exn grid c '#');
    Core.print_endline (Grid.to_string grid ~to_char:Fn.id)
  ;;

  let%expect_test "part 2" =
    f (Inputs.short_input ());
    [%expect
      {|
      #####
      #___#
      #___#
      #___#
      ##### |}];
    f (Inputs.real_input ());
    [%expect
      {|
      ####_#__#__##__#__#__##__####_#__#__##_
      ___#_#_#__#__#_#__#_#__#_#____#__#_#__#
      __#__##___#__#_#__#_#____###__#__#_#___
      _#___#_#__####_#__#_#____#____#__#_#___
      #____#_#__#__#_#__#_#__#_#____#__#_#__#
      ####_#__#_#__#__##___##__#_____##___##_ |}]
  ;;
end
