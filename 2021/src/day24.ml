open! Core
open! Std_internal

(*
inp a - Read an input value and write it to variable a.
add a b - Add the value of a to the value of b, then store the result in variable a.
mul a b - Multiply the value of a by the value of b, then store the result in variable a.
div a b - Divide the value of a by the value of b, truncate the result to an integer, then store the result in variable a. (Here, "truncate" means to round the value toward zero.)
mod a b - Divide the value of a by the value of b, then store the remainder in variable a. (This is also called the modulo operation.)
eql a b - If the value of a and b are equal, then store the value 1 in variable a. Otherwise, store the value 0 in variable a.
 *)
module Instruction = struct
  module Var = struct
    type t = char

    let of_string s = String.to_list s |> e1
  end

  module Int_or_var = struct
    type t =
      | Int of int
      | Var of Var.t

    let of_string s =
      match Or_error.try_with (fun () -> Int.of_string s) with
      | Ok i -> Int i
      | Error _ -> Var (Var.of_string s)
    ;;
  end

  type t =
    | Inp of Var.t
    | Add of Var.t * Int_or_var.t
    | Mul of Var.t * Int_or_var.t
    | Div of Var.t * Int_or_var.t
    | Mod of Var.t * Int_or_var.t
    | Eql of Var.t * Int_or_var.t

  let of_string s =
    match sps s with
    | [ "inp"; v ] -> Inp (Var.of_string v)
    | [ "add"; a; b ] -> Add (Var.of_string a, Int_or_var.of_string b)
    | [ "mul"; a; b ] -> Mul (Var.of_string a, Int_or_var.of_string b)
    | [ "div"; a; b ] -> Div (Var.of_string a, Int_or_var.of_string b)
    | [ "mod"; a; b ] -> Mod (Var.of_string a, Int_or_var.of_string b)
    | [ "eql"; a; b ] -> Eql (Var.of_string a, Int_or_var.of_string b)
    | _ -> rip ()
  ;;
end

module Machine = struct
  type t = int Char.Table.t

  let create = Char.Table.create
  let get_val t v = Char.Table.find_or_add t v ~default:(const 0)

  let get_int_or_val t iov =
    match iov with
    | Instruction.Int_or_var.Int i -> i
    | Var v -> get_val t v
  ;;

  let run t instr input =
    List.iter instr ~f:(function
      | Instruction.Inp var ->
        let v = Queue.dequeue_exn input in
        Char.Table.set t ~key:var ~data:v
      | Add (a, b) ->
        let va = get_val t a in
        let vb = get_int_or_val t b in
        let v = va + vb in
        Char.Table.set t ~key:a ~data:v
      | Mul (a, b) ->
        let va = get_val t a in
        let vb = get_int_or_val t b in
        let v = va * vb in
        Char.Table.set t ~key:a ~data:v
      | Div (a, b) ->
        let va = get_val t a in
        let vb = get_int_or_val t b in
        let v = va / vb in
        Char.Table.set t ~key:a ~data:v
      | Mod (a, b) ->
        let va = get_val t a in
        let vb = get_int_or_val t b in
        let v = va % vb in
        Char.Table.set t ~key:a ~data:v
      | Eql (a, b) ->
        let va = get_val t a in
        let vb = get_int_or_val t b in
        let v = if va = vb then 1 else 0 in
        Char.Table.set t ~key:a ~data:v)
  ;;
  (* Core.print_s [%message (t : int Char.Table.t)] *)
end

module Symbolic_machine = struct
  module Digit = struct
    type t = char [@@deriving sexp, compare, hash]
  end

  module Maybe_symbol = struct
    module T = struct
      type t =
        | Add of t * t
        | Mul of t * t
        | Div of t * t
        | Mod of t * t
        | Eql of t * t
        | Int of int
        | Digit of Digit.t
      [@@deriving sexp, compare, hash]
    end

    include T
    include Hashable.Make (T)

    let rec to_string t =
      match t with
      | Digit d -> Char.to_string d
      | Int i -> Int.to_string i
      | Add (a, b) -> sprintf "(%s + %s)" (to_string a) (to_string b)
      | Mul (a, b) -> sprintf "(%s * %s)" (to_string a) (to_string b)
      | Div (a, b) -> sprintf "(%s / %s)" (to_string a) (to_string b)
      | Mod (a, b) -> sprintf "(%s mod %s)" (to_string a) (to_string b)
      | Eql (a, b) -> sprintf "(%s = %s)" (to_string a) (to_string b)
    ;;

    let eval t values =
      let rec eval =
        let memo = Table.create () in
        fun t ->
          Table.find_or_add memo t ~default:(fun () ->
            match t with
            | Digit d -> Char.Map.find_exn values d
            | Int i -> i
            | Add (a, b) -> eval a + eval b
            | Mul (a, b) -> eval a * eval b
            | Div (a, b) -> eval a / eval b
            | Mod (a, b) -> eval a % eval b
            | Eql (a, b) -> if eval a = eval b then 1 else 0)
      in
      eval t
    ;;
  end

  type t = Maybe_symbol.t Char.Table.t [@@deriving sexp_of]

  let create () = Char.Table.create ()
  let get_val t v = Char.Table.find_or_add t v ~default:(const (Maybe_symbol.Int 0))

  let get_int_or_val t iov =
    match iov with
    | Instruction.Int_or_var.Int i -> Maybe_symbol.Int i
    | Var v -> get_val t v
  ;;

  let apply_mod t i : Maybe_symbol.t =
    match t with
    | Maybe_symbol.Int v -> Int (v % i)
    | Mul (a, Int v) -> if v = i then a else t
    | _ -> t
  ;;

  let apply_div t i : Maybe_symbol.t =
    match t with
    | Maybe_symbol.Int v -> Int (v / i)
    | Mod (_, Int v) -> if v <= i then Int 0 else t
    | _ -> t
  ;;

  let apply_eql t i : Maybe_symbol.t =
    match t with
    | Maybe_symbol.Int v -> Int (if i = v then 1 else 0)
    | Digit _ -> if i >= 10 then Int 0 else t
    | _ -> t
  ;;

  let rec apply_mult t i : Maybe_symbol.t =
    match t with
    | Maybe_symbol.Int j -> Int (i * j)
    | Add (a, b) -> Add (apply_mult a i, apply_mult b i)
    | Mul (Int j, b) | Mul (b, Int j) -> Mul (b, Int (i * j))
    | Mul (a, b) -> Mul (apply_mult a i, b)
    | Div (a, b) -> Div (apply_mult a i, b)
    | Eql _ -> Mul (Int i, t)
    | Mod (a, b) -> Mod (apply_mult a i, b)
    | Digit _ -> Mul (t, Int i)
  ;;

  let apply_add t i : Maybe_symbol.t =
    match t with
    | Maybe_symbol.Int j -> Int (i + j)
    | Add (Int j, b) | Add (b, Int j) -> Add (b, Int (i + j))
    | _ -> t
  ;;

  let run t instr input =
    let get_and_combine (a : Instruction.Var.t) (b : Instruction.Int_or_var.t) ~f
      : Instruction.Var.t * Maybe_symbol.t
      =
      let va = get_val t a in
      let vb = get_int_or_val t b in
      let v = f (va, vb) in
      a, v
    in
    List.iteri instr ~f:(fun i ins ->
      Core.printf "%i\n%!" i;
      (* Core.print_s [%message (i : int) (t : Maybe_symbol.t Char.Table.t)]; *)
      let var, v =
        match ins with
        | Instruction.Inp var ->
          let v = Queue.dequeue_exn input in
          var, Maybe_symbol.Digit v
        | Add (a, b) ->
          get_and_combine a b ~f:(function
            | Int a, Int b -> Maybe_symbol.Int (a + b)
            | Int 0, v | v, Int 0 -> v
            | Int i, v | v, Int i -> apply_add v i
            | a, b -> Add (a, b))
        | Mul (a, b) ->
          get_and_combine a b ~f:(function
            | Int a, Int b -> Maybe_symbol.Int (a * b)
            | Int 1, v | v, Int 1 -> v
            | Int 0, _v | _v, Int 0 -> Int 0
            | v, Int i | Int i, v -> apply_mult v i
            | a, b -> Mul (a, b))
        | Div (a, b) ->
          get_and_combine a b ~f:(function
            | Int a, Int b -> Int (a / b)
            | v, Int 1 -> v
            | v, Int i -> apply_div v i
            | a, b -> Div (a, b))
        | Mod (a, b) ->
          get_and_combine a b ~f:(function
            | Int a, Int b -> Int (a % b)
            | v, Int i -> apply_mod v i
            | a, b -> Mod (a, b))
        | Eql (a, b) ->
          get_and_combine a b ~f:(function
            | v, Int i | Int i, v -> apply_eql v i
            | a, b -> Eql (a, b))
      in
      Char.Table.set t ~key:var ~data:v)
  ;;
end

let parse_one s = Instruction.of_string s
let parse input = List.map input ~f:parse_one

let%expect_test _ =
  let m = Machine.create () in
  let instr = {|inp z
inp x
mul z 3
eql z x|} |> String.split_lines |> parse in
  let input = [ 3; 8 ] |> Queue.of_list in
  Machine.run m instr input;
  [%expect {||}];
  let m = Machine.create () in
  let instr =
    {|inp w
  add z w
    mod z 2
    div w 2
    add y w
    mod y 2
    div w 2
    add x w
    mod x 2
    div w 2
    mod w 2|}
    |> String.split_lines
    |> List.map ~f:String.strip
    |> parse
  in
  let input = [ 8 ] |> Queue.of_list in
  Machine.run m instr input;
  [%expect {||}]
;;

module Part1 = struct
  let contains_zero i = List.exists i ~f:(Char.equal '0')

  let runi instr i =
    let m = Machine.create () in
    let chars = Int.to_string i |> String.to_list in
    if contains_zero chars
    then `skip
    else (
      let input = List.map ~f:ctoi chars |> Queue.of_list in
      match Or_error.try_with (fun () -> Machine.run m instr input) with
      | Error err ->
        Core.print_s [%message (i : int) (err : Error.t)];
        `ran false
      | Ok () ->
        let value = Machine.get_val m 'z' in
        `ran (value = 0))
  ;;

  let f input =
    let instr = parse input in
    let m = Symbolic_machine.create () in
    let input = "abcdefghijklmn" |> String.to_list |> Queue.of_list in
    Symbolic_machine.run m instr input;
    let z = Char.Table.find_exn m 'z' in
    Core.printf "%s\n%!" (Symbolic_machine.Maybe_symbol.to_string z)
  ;;

  let _f input =
    let instr = parse input in
    let i = 99999999999999 in
    let m = Symbolic_machine.create () in
    let input' = "abcdefghijklmn" |> String.to_list in
    let input = input' |> Queue.of_list in
    Symbolic_machine.run m instr input;
    let z = Char.Table.find_exn m 'z' in
    let rec run i =
      Core.print_s [%message (i : int)];
      let chars = Int.to_string i |> String.to_list in
      if contains_zero chars
      then run (i - 1)
      else (
        let values =
          List.map chars ~f:ctoi |> List.zip_exn input' |> Char.Map.of_alist_exn
        in
        match Symbolic_machine.Maybe_symbol.eval z values with
        | 0 -> i
        | _ -> run (i - 1))
    in
    let result = run i in
    printf !"%{sexp: int}\n%!" result
  ;;
end

module Part2 = struct
  let f input =
    let input = parse input in
    let result = List.length input in
    printf !"%{sexp: int}\n%!" result
  ;;
end

let command = Commands.both ~day:24 ~part1:Part1.f ~part2:Part2.f
