open! Core
open! Std_internal

module Config = struct
  let day = "14"

  let short_input =
    {|NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C|}
  ;;

  let test_inputs = [||]
end

module Parsed = struct
  type t = string * char String.Map.t [@@deriving sexp]

  let parse_one s =
    let a, _, c = String.split s ~on:' ' |> e3 in
    a, String.to_list c |> e1
  ;;

  let parse input =
    let template, rest = coagulate input |> e2 in
    let rest = List.map rest ~f:parse_one in
    template |> e1, rest |> String.Map.of_alist_exn
  ;;
end

module Inputs = struct
  let real_input () =
    Stdio.In_channel.read_lines [%string "../input/%{Config.day}.txt"] |> Parsed.parse
  ;;

  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect
      {|
      (NNCB
       ((BB N) (BC B) (BH H) (BN B) (CB H) (CC N) (CH B) (CN C) (HB C) (HC B)
        (HH N) (HN C) (NB B) (NC B) (NH C) (NN C))) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

let apply' rules ~n =
  Memo.general (fun template ->
    let chars = String.to_list template in
    let apply_one template =
      let res =
        List.fold (consecutive_pairs template) ~init:[] ~f:(fun accum (a, b) ->
          let s = sprintf "%c%c" a b in
          match String.Map.find rules s with
          | None -> a :: accum
          | Some s -> s :: a :: accum)
      in
      List.last_exn template :: res |> List.rev
    in
    Fn.apply_n_times apply_one chars ~n
    |> List.map ~f:(fun c -> c, 1)
    |> Char.Map.of_alist_multi
    |> Char.Map.to_alist
    |> List.map ~f:(fun (c, l) -> c, List.length l))
;;

let apply template rules ~n =
  let res = apply' rules ~n template in
  let min =
    List.min_elt res ~compare:(Comparable.lift Int.compare ~f:snd)
    |> Option.value_exn
    |> snd
  in
  let max =
    List.max_elt res ~compare:(Comparable.lift Int.compare ~f:snd)
    |> Option.value_exn
    |> snd
  in
  max - min
;;

module Part1 = struct
  let f input =
    let template, rules = input in
    let result = apply template rules ~n:10 in
    print_endline [%string "result: %{result#Int}"]
  ;;

  (* let%expect_test "part 1" = *)
  (*   f (Inputs.short_input ()); *)
  (*   [%expect {| result: 1588 |}]; *)
  (*   f (Inputs.real_input ()); *)
  (*   [%expect {| result: 2621 |}] *)
  (* ;; *)
end

module Part2 = struct
  let f input =
    let template, rules = input in
    let template = String.to_list template in
    let chars =
      template
      |> consecutive_pairs
      |> List.map ~f:(fun (a, b) -> sprintf "%c%c" a b, 1)
      |> String.Map.of_alist_multi
      |> String.Map.map ~f:List.length
    in
    let incr map key count =
      String.Map.update map key ~f:(function
        | None -> count
        | Some i -> i + count)
    in
    let apply_one duos =
      String.Map.to_alist duos
      |> List.fold ~init:String.Map.empty ~f:(fun accum (duo, count) ->
           match String.Map.find rules duo with
           | None -> incr accum duo count
           | Some c ->
             let a, b = String.to_list duo |> e2 in
             incr accum (sprintf "%c%c" a c) count
             |> fun accum -> incr accum (sprintf "%c%c" c b) count)
    in
    let res =
      Fn.apply_n_times apply_one chars ~n:40
      |> String.Map.to_alist
      |> List.fold ~init:Char.Map.empty ~f:(fun accum (s, count) ->
           List.fold (String.to_list s) ~init:accum ~f:(fun accum c ->
             Char.Map.update accum c ~f:(function
               | None -> count
               | Some i -> i + count)))
      |> fun a ->
      Char.Map.update a (List.hd_exn template) ~f:(function
        | None -> 1
        | Some i -> i + 1)
      |> fun a ->
      Char.Map.update a (List.last_exn template) ~f:(function
        | None -> 1
        | Some i -> i + 1)
      |> Char.Map.to_alist
    in
    let min =
      List.min_elt res ~compare:(Comparable.lift Int.compare ~f:snd)
      |> Option.value_exn
      |> snd
    in
    let max =
      List.max_elt res ~compare:(Comparable.lift Int.compare ~f:snd)
      |> Option.value_exn
      |> snd
    in
    let result = (max - min) / 2 in
    print_endline [%string "result: %{result#Int}"]
  ;;

  (* let%expect_test "part 2" = *)
  (*   f (Inputs.short_input ()); *)
  (*   [%expect {| result: 2188189693529 |}]; *)
  (*   f (Inputs.real_input ()); *)
  (*   [%expect {| result: 2843834241366 |}] *)
  (* ;; *)
end
