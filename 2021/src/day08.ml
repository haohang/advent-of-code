open! Core
open! Std_internal

module Config = struct
  let day = "08"

  let short_input =
    {|acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf|}
  ;;

  let test_inputs = [||]
end

module Parsed = struct
  type t = (string list * string list) list [@@deriving sexp]

  let parse_one s =
    match String.split s ~on:'|' with
    | [ input; output ] ->
      let input =
        String.split input ~on:' ' |> List.filter ~f:(fun i -> not (String.is_empty i))
      in
      let output =
        String.split output ~on:' ' |> List.filter ~f:(fun i -> not (String.is_empty i))
      in
      input, output
    | _ -> raise_s [%message "Unparseable" s]
  ;;

  let parse input = List.map input ~f:parse_one
end

module Inputs = struct
  let real_input () =
    Stdio.In_channel.read_lines [%string "../input/%{Config.day}.txt"] |> Parsed.parse
  ;;

  let short_input = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" short_input;
    [%expect
      {|
      (((acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab)
        (cdfeb fcadb cdfeb cdbaf))) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

module Part1 = struct
  let count input =
    List.map input ~f:snd
    |> List.map ~f:(fun l ->
         List.count l ~f:(fun s ->
           let len = String.length s in
           len = 2 || len = 4 || len = 3 || len = 7))
    |> List.reduce_exn ~f:( + )
  ;;

  let f input =
    let result = count input in
    printf !"%{sexp: int}\n%!" result
  ;;

  let%expect_test "part 1" =
    f Inputs.short_input;
    [%expect {| 0 |}];
    f (Inputs.real_input ());
    [%expect {| 383 |}]
  ;;
end

module Part2 = struct
  let digits =
    [ '0', "abcefg"
    ; '1', "cf"
    ; '2', "acdeg"
    ; '3', "acdfg"
    ; '4', "bcdf"
    ; '5', "abdfg"
    ; '6', "abdefg"
    ; '7', "acf"
    ; '8', "abcdefg"
    ; '9', "abcdfg"
    ]
  ;;

  let all_assignments =
    permutations_fast (String.to_list "abcdefg") ~equal:Char.equal
    |> List.map ~f:(fun p ->
         List.zip_exn (String.to_list "abcdefg") p |> Char.Map.of_alist_exn)
  ;;

  let replace assignment s =
    String.to_list s
    |> List.map ~f:(fun c -> Char.Map.find_exn assignment c)
    |> List.sort ~compare:Char.compare
    |> String.of_char_list
  ;;

  let is_possible assignment input =
    List.for_all input ~f:(fun s ->
      let r = replace assignment s in
      List.exists digits ~f:(fun (_, d) -> String.equal d r))
  ;;

  let solve (input, output) =
    let assignment =
      List.find_exn all_assignments ~f:(fun assignment -> is_possible assignment input)
    in
    List.map output ~f:(replace assignment)
    |> List.map ~f:(fun s ->
         List.find_exn digits ~f:(fun (_, d) -> String.equal d s) |> fst)
    |> String.of_char_list
    |> Int.of_string
  ;;

  let f input =
    let result = List.map input ~f:solve |> List.reduce_exn ~f:( + ) in
    printf !"%{sexp: int}\n%!" result
  ;;

  let%expect_test "part 2" =
    f Inputs.short_input;
    [%expect {| 5353 |}];
    f (Inputs.real_input ());
    [%expect {| 998900 |}]
  ;;
end
