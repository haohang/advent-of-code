open! Core
open! Std_internal
open Graph

module Config = struct
  let day = "19"

  let short_input =
    {|--- scanner 0 ---
404,-588,-901
528,-643,409
-838,591,734
390,-675,-793
-537,-823,-458
-485,-357,347
-345,-311,381
-661,-816,-575
-876,649,763
-618,-824,-621
553,345,-567
474,580,667
-447,-329,318
-584,868,-557
544,-627,-890
564,392,-477
455,729,728
-892,524,684
-689,845,-530
423,-701,434
7,-33,-71
630,319,-379
443,580,662
-789,900,-551
459,-707,401

--- scanner 1 ---
686,422,578
605,423,415
515,917,-361
-336,658,858
95,138,22
-476,619,847
-340,-569,-846
567,-361,727
-460,603,-452
669,-402,600
729,430,532
-500,-761,534
-322,571,750
-466,-666,-811
-429,-592,574
-355,545,-477
703,-491,-529
-328,-685,520
413,935,-424
-391,539,-444
586,-435,557
-364,-763,-893
807,-499,-711
755,-354,-619
553,889,-390

--- scanner 2 ---
649,640,665
682,-795,504
-784,533,-524
-644,584,-595
-588,-843,648
-30,6,44
-674,560,763
500,723,-460
609,671,-379
-555,-800,653
-675,-892,-343
697,-426,-610
578,704,681
493,664,-388
-671,-858,530
-667,343,800
571,-461,-707
-138,-166,112
-889,563,-600
646,-828,498
640,759,510
-630,509,768
-681,-892,-333
673,-379,-804
-742,-814,-386
577,-820,562

--- scanner 3 ---
-589,542,597
605,-692,669
-500,565,-823
-660,373,557
-458,-679,-417
-488,449,543
-626,468,-788
338,-750,-386
528,-832,-391
562,-778,733
-938,-730,414
543,643,-506
-524,371,-870
407,773,750
-104,29,83
378,-903,-323
-778,-728,485
426,699,580
-438,-605,-362
-469,-447,-387
509,732,623
647,635,-688
-868,-804,481
614,-800,639
595,780,-596

--- scanner 4 ---
727,592,562
-293,-554,779
441,611,-461
-714,465,-776
-743,427,-804
-660,-479,-426
832,-632,460
927,-485,-438
408,393,-506
466,436,-512
110,16,151
-258,-428,682
-393,719,612
-211,-452,876
808,-476,-593
-575,615,604
-485,667,467
-680,325,-822
-627,-443,-432
872,-547,-609
833,512,582
807,604,487
839,-516,451
891,-625,532
-652,-548,-490
30,-46,-14|}
  ;;

  let test_inputs = [||]
end

module Parsed = struct
  type t = NCoord.t list list [@@deriving sexp]

  let parse_one s =
    List.tl_exn s |> List.map ~f:(fun s -> NCoord.create_exn ~dim:3 (spci s))
  ;;

  let parse input = coagulate input |> List.map ~f:parse_one
end

module Inputs = struct
  let real_input () =
    Stdio.In_channel.read_lines [%string "../input/%{Config.day}.txt"] |> Parsed.parse
  ;;

  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect {|
      ((((dim 3) (coords (404 -588 -901))) ((dim 3) (coords (528 -643 409)))
        ((dim 3) (coords (-838 591 734))) ((dim 3) (coords (390 -675 -793)))
        ((dim 3) (coords (-537 -823 -458))) ((dim 3) (coords (-485 -357 347)))
        ((dim 3) (coords (-345 -311 381))) ((dim 3) (coords (-661 -816 -575)))
        ((dim 3) (coords (-876 649 763))) ((dim 3) (coords (-618 -824 -621)))
        ((dim 3) (coords (553 345 -567))) ((dim 3) (coords (474 580 667)))
        ((dim 3) (coords (-447 -329 318))) ((dim 3) (coords (-584 868 -557)))
        ((dim 3) (coords (544 -627 -890))) ((dim 3) (coords (564 392 -477)))
        ((dim 3) (coords (455 729 728))) ((dim 3) (coords (-892 524 684)))
        ((dim 3) (coords (-689 845 -530))) ((dim 3) (coords (423 -701 434)))
        ((dim 3) (coords (7 -33 -71))) ((dim 3) (coords (630 319 -379)))
        ((dim 3) (coords (443 580 662))) ((dim 3) (coords (-789 900 -551)))
        ((dim 3) (coords (459 -707 401))))
       (((dim 3) (coords (686 422 578))) ((dim 3) (coords (605 423 415)))
        ((dim 3) (coords (515 917 -361))) ((dim 3) (coords (-336 658 858)))
        ((dim 3) (coords (95 138 22))) ((dim 3) (coords (-476 619 847)))
        ((dim 3) (coords (-340 -569 -846))) ((dim 3) (coords (567 -361 727)))
        ((dim 3) (coords (-460 603 -452))) ((dim 3) (coords (669 -402 600)))
        ((dim 3) (coords (729 430 532))) ((dim 3) (coords (-500 -761 534)))
        ((dim 3) (coords (-322 571 750))) ((dim 3) (coords (-466 -666 -811)))
        ((dim 3) (coords (-429 -592 574))) ((dim 3) (coords (-355 545 -477)))
        ((dim 3) (coords (703 -491 -529))) ((dim 3) (coords (-328 -685 520)))
        ((dim 3) (coords (413 935 -424))) ((dim 3) (coords (-391 539 -444)))
        ((dim 3) (coords (586 -435 557))) ((dim 3) (coords (-364 -763 -893)))
        ((dim 3) (coords (807 -499 -711))) ((dim 3) (coords (755 -354 -619)))
        ((dim 3) (coords (553 889 -390))))
       (((dim 3) (coords (649 640 665))) ((dim 3) (coords (682 -795 504)))
        ((dim 3) (coords (-784 533 -524))) ((dim 3) (coords (-644 584 -595)))
        ((dim 3) (coords (-588 -843 648))) ((dim 3) (coords (-30 6 44)))
        ((dim 3) (coords (-674 560 763))) ((dim 3) (coords (500 723 -460)))
        ((dim 3) (coords (609 671 -379))) ((dim 3) (coords (-555 -800 653)))
        ((dim 3) (coords (-675 -892 -343))) ((dim 3) (coords (697 -426 -610)))
        ((dim 3) (coords (578 704 681))) ((dim 3) (coords (493 664 -388)))
        ((dim 3) (coords (-671 -858 530))) ((dim 3) (coords (-667 343 800)))
        ((dim 3) (coords (571 -461 -707))) ((dim 3) (coords (-138 -166 112)))
        ((dim 3) (coords (-889 563 -600))) ((dim 3) (coords (646 -828 498)))
        ((dim 3) (coords (640 759 510))) ((dim 3) (coords (-630 509 768)))
        ((dim 3) (coords (-681 -892 -333))) ((dim 3) (coords (673 -379 -804)))
        ((dim 3) (coords (-742 -814 -386))) ((dim 3) (coords (577 -820 562))))
       (((dim 3) (coords (-589 542 597))) ((dim 3) (coords (605 -692 669)))
        ((dim 3) (coords (-500 565 -823))) ((dim 3) (coords (-660 373 557)))
        ((dim 3) (coords (-458 -679 -417))) ((dim 3) (coords (-488 449 543)))
        ((dim 3) (coords (-626 468 -788))) ((dim 3) (coords (338 -750 -386)))
        ((dim 3) (coords (528 -832 -391))) ((dim 3) (coords (562 -778 733)))
        ((dim 3) (coords (-938 -730 414))) ((dim 3) (coords (543 643 -506)))
        ((dim 3) (coords (-524 371 -870))) ((dim 3) (coords (407 773 750)))
        ((dim 3) (coords (-104 29 83))) ((dim 3) (coords (378 -903 -323)))
        ((dim 3) (coords (-778 -728 485))) ((dim 3) (coords (426 699 580)))
        ((dim 3) (coords (-438 -605 -362))) ((dim 3) (coords (-469 -447 -387)))
        ((dim 3) (coords (509 732 623))) ((dim 3) (coords (647 635 -688)))
        ((dim 3) (coords (-868 -804 481))) ((dim 3) (coords (614 -800 639)))
        ((dim 3) (coords (595 780 -596))))
       (((dim 3) (coords (727 592 562))) ((dim 3) (coords (-293 -554 779)))
        ((dim 3) (coords (441 611 -461))) ((dim 3) (coords (-714 465 -776)))
        ((dim 3) (coords (-743 427 -804))) ((dim 3) (coords (-660 -479 -426)))
        ((dim 3) (coords (832 -632 460))) ((dim 3) (coords (927 -485 -438)))
        ((dim 3) (coords (408 393 -506))) ((dim 3) (coords (466 436 -512)))
        ((dim 3) (coords (110 16 151))) ((dim 3) (coords (-258 -428 682)))
        ((dim 3) (coords (-393 719 612))) ((dim 3) (coords (-211 -452 876)))
        ((dim 3) (coords (808 -476 -593))) ((dim 3) (coords (-575 615 604)))
        ((dim 3) (coords (-485 667 467))) ((dim 3) (coords (-680 325 -822)))
        ((dim 3) (coords (-627 -443 -432))) ((dim 3) (coords (872 -547 -609)))
        ((dim 3) (coords (833 512 582))) ((dim 3) (coords (807 604 487)))
        ((dim 3) (coords (839 -516 451))) ((dim 3) (coords (891 -625 532)))
        ((dim 3) (coords (-652 -548 -490))) ((dim 3) (coords (30 -46 -14))))) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

module Scanner_result = struct
  type t = NCoord.t list

  let print t =
    List.map t ~f:NCoord.coords
    |> List.map ~f:(fun c -> List.map c ~f:Int.to_string |> String.concat ~sep:",")
    |> String.concat ~sep:"\n"
    |> Core.print_endline
  ;;

  let signs =
    List.cartesian_product [ -1; 1 ] (List.cartesian_product [ -1; 1 ] [ -1; 1 ])
    |> List.map ~f:(fun (x, (y, z)) -> x, y, z)
  ;;

  let orientations =
    [ (fun (x, y, z) -> [ x; y; z ])
    ; (fun (x, y, z) -> [ x; z; y ])
    ; (fun (x, y, z) -> [ y; x; z ])
    ; (fun (x, y, z) -> [ y; z; x ])
    ; (fun (x, y, z) -> [ z; x; y ])
    ; (fun (x, y, z) -> [ z; y; x ])
    ]
  ;;

  let transforms =
    List.concat_map signs ~f:(fun (x_s, y_s, z_s) ->
      List.map orientations ~f:(fun o (x, y, z) -> o (x * x_s, y * y_s, z * z_s)))
  ;;

  let transforms' =
    List.map transforms ~f:(fun f c ->
      NCoord.coords c |> e3 |> f |> NCoord.create_exn ~dim:3)
  ;;

  let get_relative_to_each (t : t) =
    List.map t ~f:(fun c -> c, List.map t ~f:(fun d -> NCoord.sub_exn d c))
  ;;

  let possible_transforms_that_overlap t1 t2 =
    let t1s = get_relative_to_each t1 in
    let t2s = get_relative_to_each t2 in
    List.find_map t1s ~f:(fun (c1, t1) ->
      let t1_set = NCoord.Set.of_list t1 in
      List.find_map t2s ~f:(fun (c2, t2) ->
        List.find_map transforms' ~f:(fun f ->
          let t2 = List.map t2 ~f in
          let t2_set = NCoord.Set.of_list t2 in
          let inter = NCoord.Set.inter t1_set t2_set in
          if NCoord.Set.length inter >= 12
          then (
            let c2 = f c2 in
            (* print (NCoord.Set.to_list inter |> List.map ~f:(NCoord.add_exn c1)); *)
            (* Core.print_s [%message (NCoord.sub_exn c1 c2 : NCoord.t)]; *)
            Some
              ( NCoord.sub_exn c1 c2
              , f
              , NCoord.Set.union t1_set t2_set |> NCoord.Set.map ~f:(NCoord.add_exn c1) ))
          else None)))
  ;;
end

let find_pairs input =
  let queue = Deque.create () in
  Deque.enqueue_back queue (0, List.hd_exn input);
  let result = Int.Table.create () in
  let seen = Int.Hash_set.create () in
  let reached = Int.Hash_set.create () in
  let order = Deque.create () in
  Deque.enqueue_back order 0;
  let rec run () =
    match Deque.dequeue_front queue with
    | None -> Int.Table.to_alist result |> Int.Map.of_alist_exn, order
    | Some (i, t1) ->
      if Hash_set.mem seen i
      then run ()
      else (
        (* Core.print_s [%message "Finding pair" (i : int)]; *)
        Hash_set.add seen i;
        let data =
          List.filter_mapi input ~f:(fun j t2 ->
            if i = j
            then None
            else if Hash_set.mem seen j || Hash_set.mem reached j
            then None
            else (
              match Scanner_result.possible_transforms_that_overlap t1 t2 with
              | None -> None
              | Some res ->
                (* Core.print_s [%message "Enqueueing" (j : int)]; *)
                Deque.enqueue_back queue (j, t2);
                Deque.enqueue_back order j;
                Hash_set.add reached j;
                Some (j, res)))
        in
        (* Core.print_s [%message "Adding" (i : int)]; *)
        Int.Table.add_exn result ~key:i ~data;
        run ())
  in
  run ()
;;

let merge (pairs, order) =
  (* i -> (j, (pos_of_j_rel_to_i, f_applied_to_j, union)) list *)
  (* let pairs = Int.Map.to_alist pairs in *)
  Deque.fold
    order
    ~init:
      (NCoord.Set.empty, Int.Map.singleton 0 (NCoord.create_exn ~dim:3 [ 0; 0; 0 ], Fn.id))
    ~f:(fun (accum, infos) i ->
      let elts = Int.Map.find_exn pairs i in
      (* let to_process = List.map elts ~f:fst in
       * Core.print_s [%message (i : int) (to_process : int list)]; *)
      List.fold
        elts
        ~init:(accum, infos)
        ~f:(fun (accum, infos) (j, (pos_of_j_rel_to_i, f, coords)) ->
        let pos_of_i, prev_f = Int.Map.find_exn infos i in
        let adjusted_coords =
          NCoord.Set.map coords ~f:(fun c -> NCoord.add_exn (prev_f c) pos_of_i)
        in
        let pos_of_j_rel_to_i' = prev_f pos_of_j_rel_to_i in
        let pos_of_j = NCoord.add_exn pos_of_i pos_of_j_rel_to_i' in
        (* Core.print_s
         *   [%message
         *     (pos_of_i : NCoord.t)
         *       (pos_of_j : NCoord.t)
         *       (pos_of_j_rel_to_i : NCoord.t)
         *       (pos_of_j_rel_to_i' : NCoord.t)]; *)
        let infos = Int.Map.add_exn infos ~key:j ~data:(pos_of_j, Fn.compose prev_f f) in
        NCoord.Set.union accum adjusted_coords, infos))
;;

module Part1 = struct
  let f input =
    let pairs = find_pairs input in
    let accum, _ = merge pairs in
    (* Scanner_result.print (NCoord.Set.to_list accum |> List.sort ~compare:NCoord.compare); *)
    let result = NCoord.Set.length accum in
    print_endline [%string "result: %{result#Int}"]
  ;;

  (* let%expect_test "part 1" = *)
  (*   f (Inputs.short_input ()); *)
  (*   [%expect {| result: 1 |}]; *)
  (*   f (Inputs.real_input ()); *)
  (*   [%expect {| result: 0 |}] *)
  (* ;; *)
end

module Part2 = struct
  let f input =
    let pairs = find_pairs input in
    let _accum, infos = merge pairs in
    let coords = Int.Map.to_alist infos |> List.map ~f:snd |> List.map ~f:fst in
    let result =
      List.cartesian_product coords coords
      |> List.map ~f:(fun (a, b) -> NCoord.manhattan_exn a b)
      |> List.reduce_exn ~f:Int.max
    in
    print_endline [%string "result: %{result#Int}"]
  ;;

  (* let%expect_test "part 2" = *)
  (*   f (Inputs.short_input ()); *)
  (*   [%expect {| result: 1 |}]; *)
  (*   f (Inputs.real_input ()); *)
  (*   [%expect {| result: 0 |}] *)
  (* ;; *)
end

let with_parsed f input =
  let parsed = Parsed.parse input in
  f parsed
;;

let command =
  Commands.both ~day:19 ~part1:(with_parsed Part1.f) ~part2:(with_parsed Part2.f)
;;
