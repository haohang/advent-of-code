open! Core
open! Std_internal

module Config = struct
  let day = "05"

  let short_input =
    {|0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2|}
  ;;

  let test_inputs = [||]
end

module Parsed = struct
  type t = (Graph.Coord.t * Graph.Coord.t) list [@@deriving sexp]

  let parse_one s =
    match String.split s ~on:' ' with
    | [ first; "->"; second ] ->
      let one_coord s =
        match String.split s ~on:',' with
        | [ x; y ] -> Graph.Coord.{ row = Int.of_string x; col = Int.of_string y }
        | _ -> raise_s [%message "Unparseable"]
      in
      one_coord first, one_coord second
    | _ -> raise_s [%message "Unparseable"]
  ;;

  let parse input = List.map input ~f:parse_one
end

module Inputs = struct
  let real_input () =
    Stdio.In_channel.read_lines [%string "../input/%{Config.day}.txt"] |> Parsed.parse
  ;;

  let short_input = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" short_input;
    [%expect
      {|
      ((((row 0) (col 9)) ((row 5) (col 9))) (((row 8) (col 0)) ((row 0) (col 8)))
       (((row 9) (col 4)) ((row 3) (col 4))) (((row 2) (col 2)) ((row 2) (col 1)))
       (((row 7) (col 0)) ((row 7) (col 4))) (((row 6) (col 4)) ((row 2) (col 0)))
       (((row 0) (col 9)) ((row 2) (col 9))) (((row 3) (col 4)) ((row 1) (col 4)))
       (((row 0) (col 0)) ((row 8) (col 8))) (((row 5) (col 5)) ((row 8) (col 2)))) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

(* TODO: Move to library *)
let range (coord1 : Graph.Coord.t) (coord2 : Graph.Coord.t) =
  let min_row = Int.min coord1.row coord2.row in
  let min_col = Int.min coord1.col coord2.col in
  let max_row = Int.max coord1.row coord2.row in
  let d_row = if coord1.row = min_row then 1 else -1 in
  let d_col = if coord1.col = min_col then 1 else -1 in
  List.init
    (max_row - min_row + 1)
    ~f:(fun i ->
      { Graph.Coord.row = coord1.row + (i * d_row); col = coord1.col + (i * d_col) })
;;

let%expect_test _ =
  let coords = range Graph.Coord.{ row = 7; col = 9 } Graph.Coord.{ row = 9; col = 7 } in
  Core.print_s [%message (coords : Graph.Coord.t list)];
  [%expect {| (coords (((row 7) (col 9)) ((row 8) (col 8)) ((row 9) (col 7)))) |}]
;;

let covered a b =
  if a.Graph.Coord.row = b.Graph.Coord.row || a.col = b.col
  then Graph.Coord.range a b
  else range a b
;;

let num_covered_points list =
  let covered =
    List.fold list ~init:Graph.Coord.Map.empty ~f:(fun accum (a, b) ->
      let c = covered a b in
      List.fold c ~init:accum ~f:(fun a elt ->
        Graph.Coord.Map.update a elt ~f:(function
          | None -> 1
          | Some i -> i + 1)))
  in
  Graph.Coord.Map.to_alist covered |> List.count ~f:(fun (_, i) -> i >= 2)
;;

module Part1 = struct
  let f input =
    let vertical_or_horizontal =
      List.filter input ~f:(fun (start, end_) ->
        start.Graph.Coord.row = end_.Graph.Coord.row || start.col = end_.col)
    in
    printf !"%{sexp: int}\n%!" (num_covered_points vertical_or_horizontal)
  ;;

  let%expect_test "part 1" =
    f Inputs.short_input;
    [%expect {| 5 |}];
    f (Inputs.real_input ());
    [%expect {| 8622 |}]
  ;;
end

module Part2 = struct
  let f input = printf !"%{sexp: int}\n%!" (num_covered_points input)

  let%expect_test "part 2" =
    f Inputs.short_input;
    [%expect {| 12 |}];
    f (Inputs.real_input ());
    [%expect {| 22037 |}]
  ;;
end
