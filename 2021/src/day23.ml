open! Core
open! Std_internal
open Graph

let parse input = Grid.of_char_strings ~f:Fn.id input

let target_grid =
  {|#############
#...........#
###A#B#C#D###
###A#B#C#D###
#############|}
  |> String.split_lines
  |> parse
  |> Grid.to_string ~to_char:Fn.id
;;

let neighbors grid c =
  List.map Direction.all ~f:Direction.deltas
  |> List.map ~f:(Coord.add c)
  |> List.filter ~f:(Grid.in_bounds grid)
  |> List.filter ~f:(fun c -> Char.equal (Grid.get_exn grid c) '.')
;;

let energy = Char.Map.of_alist_exn [ 'A', 1; 'B', 10; 'C', 100; 'D', 1000 ]
let target = Char.Map.of_alist_exn [ 'A', 3; 'B', 5; 'C', 7; 'D', 9 ]

let check_entering_room_ok grid c c' elt =
  let is_entering_room = c'.Coord.row = 2 && c.Coord.row = 1 in
  let is_correct_room = Char.Map.find_exn target elt = c'.col in
  let other_in_room = Grid.get_exn grid { Coord.row = 3; col = c'.col } in
  if not is_entering_room
  then true
  else if not is_correct_room
  then false
  else Char.equal other_in_room elt || Char.equal other_in_room '.'
;;

let check_is_stopped_outside_hallway c =
  c.Coord.row = 1 && (c.col = 3 || c.col = 5 || c.col = 7 || c.col = 9)
;;

let find_moves grid =
  let find_moves' grid c elt =
    if c.Coord.col = Char.Map.find_exn target elt
       && (c.Coord.row = 3
          || (c.row = 2 && Char.equal (Grid.get_exn grid { c with row = 3 }) elt))
    then []
    else (
      let e = Char.Map.find_exn energy elt in
      let moves =
        neighbors grid c
        |> List.map ~f:(fun c' ->
             let next = Grid.copy grid in
             Grid.set_exn next c' elt;
             Grid.set_exn next c '.';
             c', next, e, elt)
      in
      List.concat_map moves ~f:(fun (c', next, e, elt) ->
        if check_is_stopped_outside_hallway c'
        then
          neighbors grid c'
          |> List.filter ~f:(fun c'' -> not (Coord.equal c'' c))
          |> List.map ~f:(fun c'' ->
               let next' = Grid.copy next in
               Grid.set_exn next' c'' elt;
               Grid.set_exn next' c' '.';
               c'', next', e * 2, elt)
        else [ c', next, e, elt ])
      |> List.filter ~f:(fun (c', _, _, elt) -> check_entering_room_ok grid c c' elt))
  in
  Grid.foldi grid ~init:[] ~f:(fun c accum elt ->
    match elt with
    | 'A' | 'B' | 'C' | 'D' ->
      let moves = find_moves' grid c elt in
      accum @ moves
    | _ -> accum)
;;

let%expect_test _ =
  let g s = String.split_lines s |> Grid.of_strings in
  let test s =
    let g = g s in
    List.iter (find_moves g) ~f:(fun (_, next, _, _) ->
      Core.printf !"%s\n\n%!" (Grid.to_string next ~to_char:Fn.id))
  in
  test {|#############
#.B.........#
###.#C#B#D###
###A#D#C#A###
#############|};
  [%expect
    {|
    #############
    #B..........#
    ###.#C#B#D###
    ###A#D#C#A###
    #############

    #############
    #...B.......#
    ###.#C#B#D###
    ###A#D#C#A###
    #############

    #############
    #.B.C.......#
    ###.#.#B#D###
    ###A#D#C#A###
    #############

    #############
    #.B...C.....#
    ###.#.#B#D###
    ###A#D#C#A###
    #############

    #############
    #.B...B.....#
    ###.#C#.#D###
    ###A#D#C#A###
    #############

    #############
    #.B.....B...#
    ###.#C#.#D###
    ###A#D#C#A###
    #############

    #############
    #.B.....D...#
    ###.#C#B#.###
    ###A#D#C#A###
    #############

    #############
    #.B.......D.#
    ###.#C#B#.###
    ###A#D#C#A###
    ############# |}];
  test {|#############
#.........D.#
###A#B#C#.###
###A#B#C#D###
#############|};
  [%expect
    {|
    #############
    #...........#
    ###A#B#C#D###
    ###A#B#C#D###
    #############

    #############
    #.......D...#
    ###A#B#C#.###
    ###A#B#C#D###
    #############

    #############
    #..........D#
    ###A#B#C#.###
    ###A#B#C#D###
    ############# |}];
  test {|#############
#...........#
###A#B#C#D###
###A#B#C#D###
#############|};
  [%expect {||}]
;;

let dijkstra grid =
  let costs = String.Table.create () in
  let seen = String.Hash_set.create () in
  let to_consider = String.Hash_set.create () in
  let s = Grid.to_string grid ~to_char:Fn.id in
  Hash_set.add to_consider s;
  String.Table.set costs ~key:s ~data:0;
  let rec run () =
    Core.print_s
      [%message (Hash_set.length to_consider : int) (Hash_set.length seen : int)];
    let q, cost =
      Hash_set.to_list to_consider
      |> List.map ~f:(fun s -> s, String.Table.find_exn costs s)
      |> List.min_elt ~compare:(Comparable.lift Int.compare ~f:snd)
      |> Option.value_exn
    in
    Core.printf "%s\n\n%!" q;
    if String.equal q target_grid
    then cost
    else (
      let moves = find_moves (Grid.of_strings (String.split_lines q)) in
      Hash_set.add seen q;
      List.iter moves ~f:(fun (_, grid, next_cost, _) ->
        let s = Grid.to_string grid ~to_char:Fn.id in
        if not (Hash_set.mem seen s)
        then (
          Core.printf "Adding candidate\n%s\n\n%!" s;
          Hash_set.add to_consider s;
          String.Table.update costs s ~f:(function
            | None -> next_cost + cost
            | Some c -> Int.min (next_cost + cost) c)));
      Hash_set.remove to_consider q;
      run ())
  in
  run ()
;;

module Part1 = struct
  let f input =
    let input = parse input in
    let result = dijkstra input in
    printf !"%{sexp: int}\n%!" result
  ;;
end

module Part2 = struct
  let f input =
    let _input = parse input in
    let result = 0 in
    printf !"%{sexp: int}\n%!" result
  ;;
end

let command = Commands.both ~day:23 ~part1:Part1.f ~part2:Part2.f
