open! Core
open! Std_internal

module Config = struct
  let day = 21
  let short_input = {||}
  let test_inputs = [||]
end

module Parsed = struct
  type t = int * int [@@deriving sexp]

  let parse _input = 10, 4
end

module Inputs = struct
  let real_input () =
    Stdio.In_channel.read_lines (sprintf "../input/%02d.txt" Config.day) |> Parsed.parse
  ;;

  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect {| (10 4) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

let rec reduce i n = if i <= n then i else reduce (i - n) n

module Part1 = struct
  module Die = struct
    type t =
      { mutable i : int
      ; mutable num : int
      }

    let create () = { i = 1; num = 0 }

    let roll t =
      let i = t.i in
      t.i <- reduce (t.i + 1) 100;
      t.num <- t.num + 1;
      i
    ;;

    let total_rolled t = t.num
  end

  module Pos = struct
    type t = { mutable i : int }

    let create i = { i }

    let step t n =
      let next = reduce (t.i + n) 10 in
      t.i <- next;
      next
    ;;
  end

  let f input =
    let p1, p2 = input in
    let p1_score = ref 0 in
    let p2_score = ref 0 in
    let n = ref 0 in
    let d = Die.create () in
    let p1 = Pos.create p1 in
    let p2 = Pos.create p2 in
    let rec run () =
      incr n;
      let pts = Die.roll d + Die.roll d + Die.roll d in
      p1_score := !p1_score + Pos.step p1 pts;
      if !p1_score >= 1000
      then ()
      else (
        let pts = Die.roll d + Die.roll d + Die.roll d in
        p2_score := !p2_score + Pos.step p2 pts;
        if !p2_score >= 1000 then () else run ())
    in
    run ();
    let result = Die.total_rolled d * Int.min !p1_score !p2_score in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 1" =
    f (Inputs.real_input ());
    [%expect {| result: 908091 |}]
  ;;
end

module Part2 = struct
  let possible_dice_rolls =
    List.cartesian_product [ 1; 2; 3 ] (List.cartesian_product [ 1; 2; 3 ] [ 1; 2; 3 ])
    |> List.map ~f:(fun (a, (b, c)) -> a + b + c)
  ;;

  let num_rolls = List.length possible_dice_rolls

  let resulting_pos =
    Memo.general (fun p ->
      List.map possible_dice_rolls ~f:(fun d -> reduce (p + d) 10)
      |> List.map ~f:(fun k -> k, 1)
      |> Int.Map.of_alist_multi
      |> Int.Map.to_alist
      |> List.map ~f:(fun (k, l) -> k, List.length l))
  ;;

  module Key1 = struct
    module T = struct
      type t =
        { pos1 : int
        ; pos2 : int
        ; pts1 : int
        ; pts2 : int
        }
      [@@deriving sexp, compare, hash]
    end

    include T
    include Hashable.Make (T)
  end

  let rec play =
    let memo = Key1.Table.create () in
    fun (pos1, pts1) (pos2, pts2) ->
      Key1.Table.find_or_add memo { Key1.pos1; pos2; pts1; pts2 } ~default:(fun () ->
        let pos1_next = resulting_pos pos1 in
        let pos2_next = resulting_pos pos2 in
        List.concat_map pos1_next ~f:(fun (pos1_next, n1) ->
          let pts1_next = pos1_next + pts1 in
          if pts1_next >= 21
          then [ n1, 0 ]
          else
            List.map pos2_next ~f:(fun (pos2_next, n2) ->
              let pts2_next = pos2_next + pts2 in
              if pts2_next >= 21
              then 0, n1 * n2
              else (
                let p1_wins, p2_wins =
                  play (pos1_next, pts1_next) (pos2_next, pts2_next)
                in
                p1_wins * n1 * n2, p2_wins * n1 * n2)))
        |> List.reduce_exn ~f:(fun (a1, a2) (b1, b2) -> a1 + b1, a2 + b2))
  ;;

  let num_wins p1 p2 = play (p1, 0) (p2, 0)

  let f input =
    let p1, p2 = input in
    let result = num_wins p1 p2 in
    printf !"%{sexp: int * int}\n%!" result
  ;;

  let%expect_test "part 2" =
    f (Inputs.real_input ());
    [%expect {| (190897246590017 90710140491134) |}]
  ;;
end

let with_parsed f input =
  let parsed = Parsed.parse input in
  f parsed
;;

let command =
  Commands.both ~day:Config.day ~part1:(with_parsed Part1.f) ~part2:(with_parsed Part2.f)
;;
