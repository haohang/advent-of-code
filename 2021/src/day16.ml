open! Core
open! Std_internal

module Config = struct
  let day = "16"
  let short_input = {|A0016C880162017C3686B18A3D4780|}

  let test_inputs =
    [| "D2FE28"
     ; "38006F45291200"
     ; "620080001611562C8802118E34"
     ; "C0015000016115A2E0802F182340"
    |]
  ;;
end

module Parsed = struct
  type t = int list [@@deriving sexp]

  let parse_one s =
    String.to_list s
    |> List.map ~f:(fun c ->
         if Char.is_digit c
         then ctoi c
         else (
           match c with
           | 'A' -> 10
           | 'B' -> 11
           | 'C' -> 12
           | 'D' -> 13
           | 'E' -> 14
           | 'F' -> 15
           | _ -> rip ()))
    |> List.concat_map ~f:(fun i ->
         decimal_to_binary_digits i |> pad_left ~len:4 ~with_:0)
  ;;

  let parse input = List.map input ~f:parse_one |> e1
end

module Inputs = struct
  let real_input () =
    Stdio.In_channel.read_lines [%string "../input/%{Config.day}.txt"] |> Parsed.parse
  ;;

  let short_input = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" short_input;
    [%expect
      {|
      (1 0 1 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 1 0 1 1 0 0 1 0 0 0 1 0 0 0 0 0 0 0 0 0
       0 1 0 1 1 0 0 0 1 0 0 0 0 0 0 0 0 1 0 1 1 1 1 1 0 0 0 0 1 1 0 1 1 0 1 0 0 0
       0 1 1 0 1 0 1 1 0 0 0 1 1 0 0 0 1 0 1 0 0 0 1 1 1 1 0 1 0 1 0 0 0 1 1 1 1 0
       0 0 0 0 0 0) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect
      {|
      ((1 1 0 1 0 0 1 0 1 1 1 1 1 1 1 0 0 0 1 0 1 0 0 0)
       (0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 1 1 1 1 0 1 0 0 0 1 0 1 0 0 1 0 1 0
        0 1 0 0 0 1 0 0 1 0 0 0 0 0 0 0 0 0)
       (0 1 1 0 0 0 1 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1
        1 0 0 0 0 1 0 0 0 1 0 1 0 1 0 1 1 0 0 0 1 0 1 1 0 0 1 0 0 0 1 0 0 0 0 0 0 0
        0 0 1 0 0 0 0 1 0 0 0 1 1 0 0 0 1 1 1 0 0 0 1 1 0 1 0 0)
       (1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
        0 1 0 1 1 0 0 0 0 1 0 0 0 1 0 1 0 1 1 0 1 0 0 0 1 0 1 1 1 0 0 0 0 0 1 0 0 0
        0 0 0 0 0 0 1 0 1 1 1 1 0 0 0 1 1 0 0 0 0 0 1 0 0 0 1 1 0 1 0 0 0 0 0 0)) |}]
  ;;
end

let get_n s ~n =
  let rec helper accum rest n =
    if n = 0
    then List.rev accum, rest
    else (
      match rest with
      | [] -> rip ()
      | x :: xs -> helper (x :: accum) xs (n - 1))
  in
  helper [] s n
;;

type parsed_packet =
  { version : int
  ; value : [ `literal of int | `op of int * parsed_packet list ]
  }
[@@deriving sexp]

let rec process_literal s =
  let rec next accum s =
    let n, s = get_n s ~n:5 in
    match n with
    | x :: xs ->
      if x = 1 then next (accum @ xs) s else accum @ xs |> binary_digits_to_decimal, s
    | _ -> rip ()
  in
  next [] s

and process_operator s =
  let length_type_id, s = get_n s ~n:1 in
  let len, s =
    if e1 length_type_id = 0
    then (
      let len, s = get_n s ~n:15 in
      `bits (binary_digits_to_decimal len), s)
    else (
      let num, s = get_n s ~n:11 in
      `packets (binary_digits_to_decimal num), s)
  in
  match len with
  | `bits len ->
    let sub, s = get_n s ~n:len in
    let rec run accum s =
      let v, s = process_packet s in
      if List.is_empty s then List.rev (v :: accum) else run (v :: accum) s
    in
    run [] sub, s
  | `packets num ->
    let rec run accum n s =
      if n = 0
      then List.rev accum, s
      else (
        let v, s = process_packet s in
        run (v :: accum) (n - 1) s)
    in
    run [] num s

and process_packet s =
  let version, s = get_n s ~n:3 in
  let id, s = get_n s ~n:3 in
  let version = binary_digits_to_decimal version in
  let id = binary_digits_to_decimal id in
  if id = 4
  then (
    let v, s = process_literal s in
    { version; value = `literal v }, s)
  else (
    let sub, s = process_operator s in
    { version; value = `op (id, sub) }, s)
;;

module Part1 = struct
  let f input =
    let parsed, _rest = process_packet input in
    let result =
      let rec sum p =
        match p.value with
        | `literal _ -> p.version
        | `op (_, l) -> p.version + (List.map l ~f:sum |> List.reduce_exn ~f:( + ))
      in
      sum parsed
    in
    print_endline [%string "result: %{result#Int}"]
  ;;

  (* let%expect_test "part 1" = *)
  (*   f Inputs.short_input; *)
  (*   [%expect {| result: 31 |}]; *)
  (*   f (Inputs.real_input ()); *)
  (*   [%expect {| result: 965 |}] *)
  (* ;; *)
end

module Part2 = struct
  let f input =
    let parsed, _rest = process_packet input in
    let result =
      let rec eval p =
        match p.value with
        | `literal v -> v
        | `op (i, l) ->
          let r = List.map l ~f:eval in
          (match i with
           | 0 -> List.reduce_exn ~f:( + ) r
           | 1 -> List.reduce_exn ~f:( * ) r
           | 2 -> List.reduce_exn ~f:Int.min r
           | 3 -> List.reduce_exn ~f:Int.max r
           | 5 ->
             let a, b = e2 r in
             if a > b then 1 else 0
           | 6 ->
             let a, b = e2 r in
             if a < b then 1 else 0
           | 7 ->
             let a, b = e2 r in
             if a = b then 1 else 0
           | _ -> rip ())
      in
      eval parsed
    in
    print_endline [%string "result: %{result#Int}"]
  ;;

  (* let%expect_test "part 2" = *)
  (*   f Inputs.short_input; *)
  (*   [%expect {| result: 54 |}]; *)
  (*   f (Inputs.real_input ()); *)
  (*   [%expect {| result: 116672213160 |}] *)
  (* ;; *)
end

let%expect_test "process_packet" =
  let parsed = Inputs.test_inputs.(0) in
  Core.print_s [%message (parsed : int list)];
  let p, _ = process_packet parsed in
  Core.print_s [%message (p : parsed_packet)];
  [%expect
    {|
    (parsed (1 1 0 1 0 0 1 0 1 1 1 1 1 1 1 0 0 0 1 0 1 0 0 0))
    (p ((version 6) (value (literal 2021)))) |}];
  let parsed = Inputs.test_inputs.(1) in
  Core.print_s [%message (parsed : int list)];
  let p, _ = process_packet parsed in
  Core.print_s [%message (p : parsed_packet)];
  [%expect
    {|
    (parsed
     (0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 1 1 1 1 0 1 0 0 0 1 0 1 0 0 1 0 1 0
      0 1 0 0 0 1 0 0 1 0 0 0 0 0 0 0 0 0))
    (p
     ((version 1)
      (value
       (op
        (6
         (((version 6) (value (literal 10))) ((version 2) (value (literal 20))))))))) |}];
  let parsed = Inputs.test_inputs.(2) in
  let p, _ = process_packet parsed in
  Core.print_s [%message (p : parsed_packet)];
  [%expect
    {|
    (p
     ((version 3)
      (value
       (op
        (0
         (((version 0)
           (value
            (op
             (0
              (((version 0) (value (literal 10)))
               ((version 5) (value (literal 11))))))))
          ((version 1)
           (value
            (op
             (0
              (((version 0) (value (literal 12)))
               ((version 3) (value (literal 13)))))))))))))) |}];
  let parsed = Inputs.test_inputs.(3) in
  let p, _ = process_packet parsed in
  Core.print_s [%message (p : parsed_packet)];
  [%expect
    {|
    (p
     ((version 6)
      (value
       (op
        (0
         (((version 0)
           (value
            (op
             (0
              (((version 0) (value (literal 10)))
               ((version 6) (value (literal 11))))))))
          ((version 4)
           (value
            (op
             (0
              (((version 7) (value (literal 12)))
               ((version 0) (value (literal 13)))))))))))))) |}]
;;
