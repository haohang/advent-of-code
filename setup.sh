#!/bin/bash

# ./run.sh YYYY

cp -r template $1
cd $1

cat bin/dune | sed "s/TEMPLATE/$1/g" | sponge bin/dune
cat bin/main.ml | sed "s/TEMPLATE/$1/g" | sponge bin/main.ml
cat lib/dune | sed "s/TEMPLATE/$1/g" | sponge lib/dune
mv lib/aocTEMPLATE.ml lib/aoc$1.ml
cat lib/aoc$1.ml | sed "s/TEMPLATE/$1/g" | sponge lib/aoc$1.ml
