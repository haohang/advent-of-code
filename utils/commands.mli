open! Core

val filename_param : string Command.Param.t

val group
  :  day:int
  -> part1:string * Command.t
  -> part2:string * Command.t
  -> string * Command.t

val with_args
  :  part:int
  -> f:(string list -> 'args -> unit)
  -> args_param:'args Command.Param.t
  -> string * Command.t

val one : part:int -> f:(string list -> unit) -> string * Command.t

val both
  :  day:int
  -> part1:(string list -> unit)
  -> part2:(string list -> unit)
  -> string * Command.t
