(** Miscellaneous uncategorized utility functions *)

(* Parses a list of empty-string separated string params into non-empty
   chunks (in order). *)
val coagulate : string list -> string list list

(* Non-recursively finds the first thing that passes [check]. If [next] returns
   [None], the function will terminate with [None]. *)
val non_rec_find : init:'a -> check:('a -> bool) -> next:('a -> 'a option) -> 'a option

(** Input parsing *)

(* split space (filtered for empty strings) *)
val sps : string -> string list

(* split comma (filtered for empty strings) *)
val spc : string -> string list

(* get ints *)
val spsi : string -> int list
val spci : string -> int list
val ints : string list -> int list

(* exactly N *)
val e1 : 'a list -> 'a
val e2 : 'a list -> 'a * 'a
val e3 : 'a list -> 'a * 'a * 'a
val e4 : 'a list -> 'a * 'a * 'a * 'a
val e5 : 'a list -> 'a * 'a * 'a * 'a * 'a
val e6 : 'a list -> 'a * 'a * 'a * 'a * 'a * 'a
val e7 : 'a list -> 'a * 'a * 'a * 'a * 'a * 'a * 'a
val e8 : 'a list -> 'a * 'a * 'a * 'a * 'a * 'a * 'a * 'a
val e9 : 'a list -> 'a * 'a * 'a * 'a * 'a * 'a * 'a * 'a * 'a

(* raise unparseable *)
val rip : unit -> 'a

(* sum list of ints *)
val sumi : int list -> int

(* conversions *)
val stoi : string -> int
val itos : int -> string

(* gets digit *)
val ctoi : char -> int
val itoc : int -> char

(* gets ASCII value *)
val actoi : char -> int
val itoac : int -> char

(* gets all things that look like something from a string *)
val regex_num : string -> int list
val regex_lower : string -> string list
val regex_upper : string -> string list
val regex_alpha : string -> string list
val regex_alphanum : string -> string list
