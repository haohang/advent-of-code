module type T = sig
  type t [@@deriving compare]

  val assoc : (char * t) list
end

module type S = sig
  type t

  val to_char : t -> char
  val of_char : char -> t
end

module Make (T : T) : S with type t := T.t
