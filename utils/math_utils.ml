open Core

let rec gcd a b = if b = 0 then a else gcd b (a mod b)
let lcm m n = if m = 0 || n = 0 then 0 else abs (m * n) / gcd m n

let decimal_to_binary_digits d =
  let rec aux val_ accum =
    if val_ = 0
    then accum
    else (
      let digit = val_ % 2 in
      aux (val_ / 2) (digit :: accum))
  in
  aux d []
;;

let binary_digits_to_decimal digits =
  List.foldi ~init:0 (List.rev digits) ~f:(fun i accum d -> (d * Int.pow 2 i) + accum)
;;

(* Find x such that nx ~ 1 (mod mod_)  *)
let find_mod_inverse n mod_ =
  List.init mod_ ~f:Fn.id |> List.find_exn ~f:(fun j -> j * n % mod_ = 1)
;;

let solve_congruences congruences =
  let product = List.map congruences ~f:snd |> List.reduce_exn ~f:( * ) in
  List.map congruences ~f:(fun (offset, mod_) ->
      let n = product / mod_ in
      let inverse = find_mod_inverse n mod_ in
      n * inverse * offset)
  |> List.reduce_exn ~f:( + )
  |> fun result -> result % product, product
;;
