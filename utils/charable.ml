open Core

module type T = sig
  type t [@@deriving compare]

  val assoc : (char * t) list
end

module type S = sig
  type t

  val to_char : t -> char
  val of_char : char -> t
end

module Make (T : T) : S with type t := T.t = struct
  include T

  let of_char c =
    List.find_map_exn assoc ~f:(fun (c', t) -> Option.some_if (Char.equal c c') t)
  ;;

  let to_char t =
    List.find_map_exn assoc ~f:(fun (c, t') ->
        Option.some_if ([%compare.equal: t] t t') c)
  ;;
end
