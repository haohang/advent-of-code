val gcd : int -> int -> int
val lcm : int -> int -> int
val decimal_to_binary_digits : int -> int list
val binary_digits_to_decimal : int list -> int

(* Given a system of congruences:

   x ~ b_i (mod m_i)

   Finds X, M such that 

   M = product(m_i)
   x ~ X (mod M)
*)
val solve_congruences : (int * int) list -> int * int
