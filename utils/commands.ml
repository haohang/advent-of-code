open! Core

let filename_param =
  let open Command.Param in
  anon ("filename" %: string)
;;

let cmd = sprintf "%02d"

let group ~day ~part1 ~part2 =
  cmd day, Command.group ~summary:(sprintf "Day %d" day) [ part1; part2 ]
;;

let with_args
    ~part
    ~(f : string list -> 'args -> unit)
    ~(args_param : 'args Command.Param.t)
  =
  let open Command.Let_syntax in
  ( Int.to_string part
  , Command.basic
      ~summary:(sprintf "Part %d" part)
      [%map_open
        let filename = filename_param
        and args = args_param in
        fun () ->
          let input = Stdio.In_channel.read_lines filename in
          f input args] )
;;

let one ~part ~f =
  let open Command.Let_syntax in
  ( Int.to_string part
  , Command.basic
      ~summary:(sprintf "Part %d" part)
      [%map_open
        let filename = filename_param in
        fun () ->
          let input = Stdio.In_channel.read_lines filename in
          f input] )
;;

let both ~day ~part1 ~part2 =
  group ~day ~part1:(one ~part:1 ~f:part1) ~part2:(one ~part:2 ~f:part2)
;;
