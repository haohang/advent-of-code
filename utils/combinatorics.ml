open Core

let ins_all_positions x l =
  let rec aux prev acc = function
    | []            -> (prev @ [ x ]) :: acc |> List.rev
    | hd :: tl as l -> aux (prev @ [ hd ]) ((prev @ [ x ] @ l) :: acc) tl
  in
  aux [] [] l
;;

let rec permutations = function
  | []      -> [ [] ]
  | x :: xs ->
    List.fold ~f:(fun acc p -> acc @ ins_all_positions x p) ~init:[] (permutations xs)
;;

let%expect_test "ins_all_positions" =
  Core.printf !"%{sexp: int list list}\n%!" (ins_all_positions 0 [ 1; 2; 3 ]);
  [%expect {| ((0 1 2 3) (1 0 2 3) (1 2 0 3) (1 2 3 0)) |}]
;;

let%expect_test "permutations" =
  Core.printf !"%{sexp: int list list}\n%!" (permutations [ 1; 2; 3 ]);
  [%expect {| ((1 2 3) (2 1 3) (2 3 1) (1 3 2) (3 1 2) (3 2 1)) |}]
;;

let permutations_fast elts ~equal =
  let num_elts = List.length elts in
  let rec helper accum =
    if List.length accum = num_elts
    then [ accum ]
    else
      List.concat_map elts ~f:(fun elt ->
          if List.mem accum elt ~equal then [] else helper (elt :: accum))
  in
  helper []
;;

let permutations_int = permutations_fast ~equal:Int.equal
let permutations_string = permutations_fast ~equal:String.equal

let%expect_test "permutations (fast)" =
  Core.printf !"%{sexp: int list list}\n%!" (permutations_int [ 1; 2; 3 ]);
  [%expect {| ((3 2 1) (2 3 1) (3 1 2) (1 3 2) (2 1 3) (1 2 3)) |}]
;;

let resolve_choices l =
  let rec aux accum rest =
    match rest with
    | []      -> List.map accum ~f:List.rev
    | x :: xs ->
      let accum =
        List.concat_map x ~f:(fun elt -> List.map accum ~f:(fun rest -> elt :: rest))
      in
      aux accum xs
  in
  aux [ [] ] l
;;
