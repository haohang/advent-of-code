open! Core

(* DEPRECATED: DO NOT USE *)

module Result : sig
  type t

  val get : t -> field:('t, 'field) Field.t -> f:(string -> 'field) -> 'field
  val get_int : t -> field:('t, int) Field.t -> int
  val get_string : t -> field:('t, string) Field.t -> string
end

val parse : s:string -> string list -> Result.t
