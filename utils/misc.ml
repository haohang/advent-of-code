open! Core

let coagulate input =
  let groups, current =
    List.fold ~init:([], []) input ~f:(fun (groups, current) next ->
      if String.is_empty next
      then List.rev current :: groups, []
      else groups, next :: current)
  in
  List.rev (List.rev current :: groups)
;;

let non_rec_find ~init ~check ~next =
  let i = ref (Some init) in
  let result = ref None in
  while Option.is_none !result && Option.is_some !i do
    match !i with
    | None -> ()
    | Some candidate ->
      if check candidate then result := Some candidate;
      i := next candidate
  done;
  !result
;;

let%expect_test "non_rec_find" =
  (* Find first integer greater than 10 *)
  let result =
    non_rec_find ~init:0 ~check:(fun i -> i > 10) ~next:(fun i -> Some (i + 1))
  in
  printf !"%{sexp: int option}\n%!" result;
  [%expect {| (11) |}]
;;

(* input parsing *)
let sps s = String.split s ~on:' ' |> List.filter ~f:(fun s -> not (String.is_empty s))
let spc s = String.split s ~on:','
let ints = List.map ~f:Int.of_string
let spsi s = sps s |> ints
let spci s = spc s |> ints

let e1 = function
  | [ a ] -> a
  | _ -> raise_s [%message "Unexpected number of elements"]
;;

let e2 = function
  | [ a; b ] -> a, b
  | _ -> raise_s [%message "Unexpected number of elements"]
;;

let e3 = function
  | [ a; b; c ] -> a, b, c
  | _ -> raise_s [%message "Unexpected number of elements"]
;;

let e4 = function
  | [ a; b; c; d ] -> a, b, c, d
  | _ -> raise_s [%message "Unexpected number of elements"]
;;

let e5 = function
  | [ a; b; c; d; e ] -> a, b, c, d, e
  | _ -> raise_s [%message "Unexpected number of elements"]
;;

let e6 = function
  | [ a; b; c; d; e; f ] -> a, b, c, d, e, f
  | _ -> raise_s [%message "Unexpected number of elements"]
;;

let e7 = function
  | [ a; b; c; d; e; f; g ] -> a, b, c, d, e, f, g
  | _ -> raise_s [%message "Unexpected number of elements"]
;;

let e8 = function
  | [ a; b; c; d; e; f; g; h ] -> a, b, c, d, e, f, g, h
  | _ -> raise_s [%message "Unexpected number of elements"]
;;

let e9 = function
  | [ a; b; c; d; e; f; g; h; i ] -> a, b, c, d, e, f, g, h, i
  | _ -> raise_s [%message "Unexpected number of elements"]
;;

let rip () = raise_s [%message "Fail"]
let sumi = List.reduce_exn ~f:( + )

(* Conversion functions *)
let stoi = Int.of_string
let itos = Int.to_string
let ctoi = Char.get_digit_exn
let itoc i = sprintf "%d" i |> String.to_list |> e1
let actoi = Char.to_int
let itoac = Char.of_int_exn

let%expect_test "documentation for conversion functions" =
  printf "%d\n%!" (stoi "123");
  [%expect {| 123 |}];
  printf "%s\n%!" (itos 123);
  [%expect {| 123 |}];
  printf "%d\n%!" (ctoi '5');
  [%expect {| 5 |}];
  printf "%c\n%!" (itoc 7);
  [%expect {| 7 |}];
  printf "%d\n%!" (actoi '5');
  [%expect {| 53 |}];
  printf "%c\n%!" (itoac 53);
  [%expect {| 5 |}]
;;

let regex_num s =
  let re2 = Re2.of_string "([0-9]+)" in
  Re2.find_all_exn re2 s |> ints
;;

let%expect_test "regex_num" =
  printf !"%{sexp: int list}\n%!" (regex_num "2-4,6-8");
  [%expect {| (2 4 6 8) |}];
  printf !"%{sexp: int list}\n%!" (regex_num "99 minutes and 23 seconds");
  [%expect {| (99 23) |}]
;;

let regex_str' pattern =
  let re2 = Re2.of_string pattern in
  Re2.find_all_exn re2
;;

let regex_lower = regex_str' "([a-z]+)"
let regex_upper = regex_str' "([A-Z]+)"
let regex_alpha = regex_str' "([a-zA-Z]+)"
let regex_alphanum = regex_str' "([a-zA-Z0-9]+)"
