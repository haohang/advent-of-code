open! Core

module Rotation = struct
  type t =
    | Left
    | Right
end

module Coord = struct
  module T = struct
    type t =
      { row : int
      ; col : int
      }
    [@@deriving hash, sexp, compare, fields]
  end

  include T
  include Hashable.Make_and_derive_hash_fold_t (T)
  include Comparable.Make (T)

  let manhattan_dist a b = abs (a.row - b.row) + abs (a.col - b.col)
  let origin = { row = 0; col = 0 }
  let north = { row = -1; col = 0 }
  let south = { row = 1; col = 0 }
  let west = { row = 0; col = -1 }
  let east = { row = 0; col = 1 }
  let cardinal_dirs = [ north; south; west; east ]
  let add a b = { row = a.row + b.row; col = a.col + b.col }
  let sub a b = { row = a.row - b.row; col = a.col - b.col }
  let scale a by = { row = a.row * by; col = a.col * by }

  let adjacent ({ row; col } as original) =
    let rows = List.range (row - 1) (row + 2) in
    let cols = List.range (col - 1) (col + 2) in
    List.concat_map rows ~f:(fun row -> List.map cols ~f:(fun col -> { row; col }))
    |> List.filter ~f:(fun elt -> not ([%compare.equal: t] elt original))
  ;;

  let rotate_right_once { row; col } = { col = -row; row = col }
  let rotate_left_once { row; col } = { col = row; row = -col }

  let rotation_fn = function
    | Rotation.Right -> rotate_right_once
    | Left -> rotate_left_once
  ;;

  let rotate t ~dir = (rotation_fn dir) t
  let rotate_n_times t ~dir ~n = Fn.apply_n_times ~n (rotation_fn dir) t

  let rotate_about_once t ~dir ~about =
    let relative = sub t about in
    add about ((rotation_fn dir) relative)
  ;;

  let rotate_about_n_times t ~dir ~about ~n =
    Fn.apply_n_times ~n (rotate_about_once ~dir ~about) t
  ;;

  let%expect_test "rotation" =
    let test ~f ?(about = origin) ?(n = 1) before =
      Core.printf !"%{sexp: t}\n%!" (f before ~about ~n)
    in
    let test_right = test ~f:(rotate_about_n_times ~dir:Right) in
    let test_left = test ~f:(rotate_about_n_times ~dir:Left) in
    let coord = { row = -1; col = 2 } in
    test_right coord;
    [%expect {| ((row 2) (col 1)) |}];
    test_right (rotate_left_once coord);
    [%expect {| ((row -1) (col 2)) |}];
    test_right coord ~n:2;
    [%expect {| ((row 1) (col -2)) |}];
    test_right (rotate_right_once coord);
    [%expect {| ((row 1) (col -2)) |}];
    test_right coord ~n:4;
    [%expect {| ((row -1) (col 2)) |}];
    test_left coord;
    [%expect {| ((row -2) (col -1)) |}];
    test_left (rotate_right_once coord);
    [%expect {| ((row -1) (col 2)) |}];
    test_left coord ~n:2;
    [%expect {| ((row 1) (col -2)) |}];
    test_left (rotate_left_once coord);
    [%expect {| ((row 1) (col -2)) |}];
    test_left coord ~n:4;
    [%expect {| ((row -1) (col 2)) |}];
    let test_right = test_right ~about:{ row = 1; col = 1 } in
    let test_left = test_left ~about:{ row = 1; col = 1 } in
    test_right coord;
    [%expect {| ((row 2) (col 3)) |}];
    test_right (rotate_left_once coord);
    [%expect {| ((row -1) (col 4)) |}];
    test_right coord ~n:2;
    [%expect {| ((row 3) (col 0)) |}];
    test_right (rotate_right_once coord);
    [%expect {| ((row 1) (col 0)) |}];
    test_right coord ~n:4;
    [%expect {| ((row -1) (col 2)) |}];
    test_left coord;
    [%expect {| ((row 0) (col -1)) |}];
    test_left (rotate_right_once coord);
    [%expect {| ((row 1) (col 2)) |}];
    test_left coord ~n:2;
    [%expect {| ((row 3) (col 0)) |}];
    test_left (rotate_left_once coord);
    [%expect {| ((row 3) (col -2)) |}];
    test_left coord ~n:4;
    [%expect {| ((row -1) (col 2)) |}]
  ;;

  let move t ~vec ~n = add t (scale vec n)

  let range coord1 coord2 =
    let min_row = Int.min coord1.row coord2.row in
    let min_col = Int.min coord1.col coord2.col in
    let max_row = Int.max coord1.row coord2.row in
    let max_col = Int.max coord1.col coord2.col in
    let rows = List.init (max_row - min_row + 1) ~f:(( + ) min_row) in
    let cols = List.init (max_col - min_col + 1) ~f:(( + ) min_col) in
    List.cartesian_product rows cols |> List.map ~f:(fun (row, col) -> { row; col })
  ;;

  let%expect_test _ =
    let test (r1, c1) (r2, c2) =
      let a = { row = r1; col = c1 } in
      let b = { row = r2; col = c2 } in
      Core.print_s [%message (range a b : t list)]
    in
    test (0, 0) (3, 3);
    [%expect
      {|
      ("range a b"
       (((row 0) (col 0)) ((row 0) (col 1)) ((row 0) (col 2)) ((row 0) (col 3))
        ((row 1) (col 0)) ((row 1) (col 1)) ((row 1) (col 2)) ((row 1) (col 3))
        ((row 2) (col 0)) ((row 2) (col 1)) ((row 2) (col 2)) ((row 2) (col 3))
        ((row 3) (col 0)) ((row 3) (col 1)) ((row 3) (col 2)) ((row 3) (col 3)))) |}];
    test (1, 2) (1, 2);
    [%expect {| ("range a b" (((row 1) (col 2)))) |}]
  ;;
end

module NCoord = struct
  module T = struct
    type t =
      { dim : int
      ; coords : int list
      }
    [@@deriving sexp, compare, hash, fields]

    let create_exn ~dim coords =
      if List.length coords = dim
      then { dim; coords }
      else
        raise_s
          [%message
            "NCoord.create_exn called with invalid dimension or coordinates"
              (dim : int)
              (coords : int list)]
    ;;

    let add_exn t1 t2 =
      if t1.dim = t2.dim
      then
        { t1 with
          coords =
            List.zip_exn t1.coords t2.coords |> List.map ~f:(fun (t1, t2) -> t1 + t2)
        }
      else
        raise_s
          [%message
            "Coord.N.add_exn called with two coordinates of different dimensions"
              (t1 : t)
              (t2 : t)]
    ;;

    let sub_exn t1 t2 =
      if t1.dim = t2.dim
      then
        { t1 with
          coords =
            List.zip_exn t1.coords t2.coords |> List.map ~f:(fun (t1, t2) -> t1 - t2)
        }
      else
        raise_s
          [%message
            "Coord.N.sub_exn called with two coordinates of different dimensions"
              (t1 : t)
              (t2 : t)]
    ;;

    let manhattan_exn t1 t2 =
      if t1.dim = t2.dim
      then
        List.zip_exn t1.coords t2.coords
        |> List.map ~f:(fun (t1, t2) -> abs (t1 - t2))
        |> List.reduce_exn ~f:( + )
      else
        raise_s
          [%message
            "Coord.N.sub_exn called with two coordinates of different dimensions"
              (t1 : t)
              (t2 : t)]
    ;;

    let adjacent ({ dim; coords } as original) =
      let diffs = [ -1; 0; 1 ] in
      List.map coords ~f:(fun coord -> List.map diffs ~f:(fun d -> d + coord))
      |> Combinatorics.resolve_choices
      |> List.map ~f:(fun coords -> { dim; coords })
      |> List.filter ~f:(fun elt -> not ([%compare.equal: t] elt original))
    ;;

    let get_exn t ~dim =
      if dim < t.dim
      then List.nth_exn t.coords dim
      else
        raise_s
          [%message "NCoord.get_exn called with invalid dimension" (dim : int) (t : t)]
    ;;

    let set_exn t ~dim with_ =
      if dim < t.dim
      then { t with coords = Lists_and_strings.list_replace t.coords ~i:dim ~with_ }
      else
        raise_s
          [%message "NCoord.set_exn called with invalid dimension" (dim : int) (t : t)]
    ;;
  end

  include T
  include Comparable.Make (T)
  include Hashable.Make (T)
end

module Direction = struct
  type t =
    | Up
    | Down
    | Left
    | Right
  [@@deriving sexp, enumerate]

  let of_string = function
    | "U" -> Up
    | "D" -> Down
    | "L" -> Left
    | "R" -> Right
    | s -> raise_s [%message "Invalid direction" (s : string)]
  ;;

  let deltas = function
    | Up -> { Coord.col = 0; row = -1 }
    | Down -> { col = 0; row = 1 }
    | Left -> { col = -1; row = 0 }
    | Right -> { col = 1; row = 0 }
  ;;
end

module Move = struct
  type t = Direction.t * int

  let of_string s : t =
    let dir = String.slice s 0 1 |> Direction.of_string in
    let num = String.slice s 1 Int.max_value |> Int.of_string in
    dir, num
  ;;

  let%expect_test "of_string" =
    Core.printf !"%{sexp: Direction.t * int}\n%!" (of_string "U12");
    [%expect {| (Up 12) |}]
  ;;

  let apply (dir, num) coord =
    let delta = Direction.deltas dir in
    let positions =
      List.init num ~f:(fun num -> Coord.add coord (Coord.scale delta (num + 1)))
    in
    List.last_exn positions, positions
  ;;

  let apply_with_step_count (dir, num) coord =
    let delta = Direction.deltas dir in
    let positions =
      List.init num ~f:(fun num -> Coord.add coord (Coord.scale delta (num + 1)), num + 1)
    in
    List.last_exn positions, positions
  ;;

  let apply_all moves coord =
    List.fold
      moves
      ~init:(coord, Coord.Set.singleton coord)
      ~f:(fun (current, visited) move ->
        let next, next_visited = apply move current in
        next, Coord.Set.union visited (Coord.Set.of_list next_visited))
  ;;
end

module Grid = struct
  type 'a t =
    { height : int
    ; width : int
    ; grid : 'a Array.t Array.t
    }
  [@@deriving sexp, compare]

  let to_lists { grid; _ } = Array.to_list grid |> List.map ~f:Array.to_list

  let to_string t ~to_char =
    to_lists t
    |> List.map ~f:(List.map ~f:to_char)
    |> List.map ~f:String.of_char_list
    |> String.concat ~sep:"\n"
  ;;

  let in_bounds t { Coord.row; col } =
    0 <= row && row < t.height && 0 <= col && col < t.width
  ;;

  let on_edge t ({ Coord.row; col } as c) =
    assert (in_bounds t c);
    row = 0 || col = 0 || row = t.height - 1 || col = t.height - 1
  ;;

  let of_lists lists =
    let height = List.length lists in
    let width = List.hd_exn lists |> List.length in
    if List.exists lists ~f:(fun l -> not (List.length l = width))
    then raise_s [%message "Invalid grid" (List.map lists ~f:List.length : int list)]
    else (
      let grid = List.map lists ~f:Array.of_list |> Array.of_list in
      { height; width; grid })
  ;;

  let create ~width ~height elt =
    List.init height ~f:(const (List.init width ~f:(const elt))) |> of_lists
  ;;

  let of_strings' input ~transform_row ~transform_cell =
    List.map input ~f:String.rstrip
    |> List.map ~f:transform_row
    |> List.map ~f:(List.map ~f:transform_cell)
    |> of_lists
  ;;

  let of_strings = of_strings' ~transform_row:String.to_list ~transform_cell:Fn.id

  let of_char_strings input ~f =
    of_strings' input ~transform_row:String.to_list ~transform_cell:f
  ;;

  let of_csv_strings input ~f =
    of_strings' input ~transform_row:(String.split ~on:',') ~transform_cell:f
  ;;

  let of_digit_strings input =
    of_strings'
      input
      ~transform_row:(fun row -> String.to_list row |> List.map ~f:Misc.ctoi)
      ~transform_cell:Fn.id
  ;;

  let get t (coord : Coord.t) =
    if coord.row >= t.height || coord.col >= t.width || coord.row < 0 || coord.col < 0
    then None
    else Some t.grid.(coord.row).(coord.col)
  ;;

  let get_exn t coord = get t coord |> Option.value_exn
  let set_exn t { Coord.row; col } val_ = t.grid.(row).(col) <- val_

  let get_tiling t (coord : Coord.t) =
    get_exn t { Coord.row = coord.row % t.height; col = coord.col % t.height }
  ;;

  let coords t =
    List.cartesian_product (List.init t.height ~f:Fn.id) (List.init t.width ~f:Fn.id)
    |> List.map ~f:(fun (row, col) -> { Coord.row; col })
  ;;

  let iteri t ~f =
    Array.iteri t.grid ~f:(fun row line ->
      Array.iteri line ~f:(fun col elt -> f { Coord.row; col } elt))
  ;;

  let foldi t ~init ~f =
    Array.foldi t.grid ~init ~f:(fun row accum line ->
      Array.foldi line ~init:accum ~f:(fun col elt -> f { Coord.row; col } elt))
  ;;

  let move_until_edge t coord ~vec =
    let rec aux accum curr =
      let next = Coord.add curr vec in
      if in_bounds t next
      then aux ((next, get_exn t next) :: accum) next
      else List.rev accum
    in
    aux [] coord
  ;;

  let get_row t i = t.grid.(i) |> Array.to_list
  let get_col t i = List.init t.height ~f:(fun row -> t.grid.(row).(i))
  let copy t = Array.to_list t.grid |> List.map ~f:Array.to_list |> of_lists

  let rotate_cw t n =
    match n % 4 with
    | 0 -> copy t
    | 1 -> List.init t.width ~f:(fun col -> get_col t col |> List.rev) |> of_lists
    | 2 ->
      List.init t.height ~f:(fun row -> get_row t row |> List.rev) |> List.rev |> of_lists
    | 3 -> List.init t.width ~f:(fun col -> get_col t col) |> List.rev |> of_lists
    | n -> raise_s [%message "BUG" (n : int)]
  ;;

  let reflect_over_y t =
    Array.to_list t.grid
    |> List.map ~f:(fun row -> Array.to_list row |> List.rev)
    |> of_lists
  ;;

  let neighbors t c =
    List.map Direction.all ~f:Direction.deltas
    |> List.map ~f:(Coord.add c)
    |> List.filter ~f:(in_bounds t)
  ;;

  let%expect_test "of_strings" =
    let strings =
      {|
..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#
|}
      |> String.split ~on:'\n'
      |> List.filter ~f:(fun s -> not (String.is_empty s))
    in
    Core.printf !"%{sexp: char t}\n%!" (of_strings strings);
    [%expect
      {|
      ((height 11) (width 11)
       (grid
        ((. . # # . . . . . . .) (# . . . # . . . # . .) (. # . . . . # . . # .)
         (. . # . # . . . # . #) (. # . . . # # . . # .) (. . # . # # . . . . .)
         (. # . # . # . . . . #) (. # . . . . . . . . #) (# . # # . . . # . . .)
         (# . . . # # . . . . #) (. # . . # . . . # . #)))) |}]
  ;;

  let%expect_test "reflect_over_y" =
    let grid =
      {|
0123
4567
8910
2345
|}
      |> String.split ~on:'\n'
      |> List.filter ~f:(fun s -> not (String.is_empty s))
      |> of_strings
    in
    Core.printf !"%s\n%!" (to_string ~to_char:Fn.id (reflect_over_y grid));
    [%expect {|
      3210
      7654
      0198
      5432 |}]
  ;;

  let%expect_test "rotate_cw" =
    let grid =
      {|
0123
4567
8910
2345
|}
      |> String.split ~on:'\n'
      |> List.filter ~f:(fun s -> not (String.is_empty s))
      |> of_strings
    in
    let test i = Core.printf !"%s\n%!" (to_string ~to_char:Fn.id (rotate_cw grid i)) in
    test 0;
    [%expect
      {|
              0123
              4567
              8910
              2345 |}];
    test 1;
    [%expect
      {|
              2840
              3951
              4162
              5073 |}];
    test 2;
    [%expect
      {|
              5432
              0198
              7654
              3210 |}];
    test 3;
    [%expect
      {|
              3705
              2614
              1593
              0482 |}]
  ;;
end

module Ring_buffer = struct
  type 'a t =
    { buf : 'a Array.t
    ; len : int
    }

  let create elts =
    let buf = Array.of_list elts in
    let len = Array.length buf in
    { buf; len }
  ;;

  let get t i = t.buf.(i % t.len)
  let set t i elt = t.buf.(i % t.len) <- elt
  let values t = Array.to_list t.buf
end
