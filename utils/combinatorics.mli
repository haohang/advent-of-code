(* Given a list of length n containing lists of possible values at each
   position, return a list of lists, all of length n, representing all of the
   possible choices. *)
val resolve_choices : 'a list list -> 'a list list
val permutations : 'a list -> 'a list list
val permutations_int : int list -> int list list
val permutations_string : string list -> string list list
val permutations_fast : 'a list -> equal:('a -> 'a -> bool) -> 'a list list
