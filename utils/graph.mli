open! Core

module Rotation : sig
  type t =
    | Left
    | Right
end

module Coord : sig
  type t =
    { row : int
    ; col : int
    }
  [@@deriving sexp, compare, hash, fields]

  include Comparable.S with type t := t
  include Hashable.S with type t := t

  val manhattan_dist : t -> t -> int
  val origin : t
  val north : t
  val south : t
  val west : t
  val east : t
  val cardinal_dirs : t list
  val add : t -> t -> t
  val sub : t -> t -> t
  val scale : t -> int -> t
  val adjacent : t -> t list
  val rotate : t -> dir:Rotation.t -> t
  val rotate_n_times : t -> dir:Rotation.t -> n:int -> t
  val rotate_about_once : t -> dir:Rotation.t -> about:t -> t
  val rotate_about_n_times : t -> dir:Rotation.t -> about:t -> n:int -> t
  val move : t -> vec:t -> n:int -> t

  (* [range a b] returns the coordinates (inclusive) of the rectangle with [a] and [b] as opposite corners. *)
  val range : t -> t -> t list
end

(* N-dimensional coordinate *)
module NCoord : sig
  type t [@@deriving sexp, compare]

  include Comparable.S with type t := t
  include Hashable.S with type t := t

  val dim : t -> int
  val create_exn : dim:int -> int list -> t
  val add_exn : t -> t -> t
  val sub_exn : t -> t -> t
  val adjacent : t -> t list
  val get_exn : t -> dim:int -> int
  val set_exn : t -> dim:int -> int -> t
  val coords : t -> int list
  val manhattan_exn : t -> t -> int
end

module Direction : sig
  type t =
    | Up
    | Down
    | Left
    | Right
  [@@deriving sexp, enumerate]

  val of_string : string -> t

  (* We use a coord to represent a change in position as well. *)
  val deltas : t -> Coord.t
end

module Move : sig
  type t = Direction.t * int

  val of_string : string -> t

  (* Returns the position after making a move, as well as all the coords
     visited. *)
  val apply : t -> Coord.t -> Coord.t * Coord.t list
  val apply_with_step_count : t -> Coord.t -> (Coord.t * int) * (Coord.t * int) list
  val apply_all : t list -> Coord.t -> Coord.t * Coord.Set.t
end

module Grid : sig
  type 'a t =
    { height : int
    ; width : int
    ; grid : 'a Array.t Array.t
    }
  [@@deriving sexp, compare]

  val copy : 'a t -> 'a t
  val create : width:int -> height:int -> 'a -> 'a t
  val coords : 'a t -> Coord.t list
  val to_lists : 'a t -> 'a list list
  val to_string : 'a t -> to_char:('a -> char) -> string
  val of_lists : 'a list list -> 'a t
  val of_strings : string list -> char t
  val of_char_strings : string list -> f:(char -> 'a) -> 'a t
  val of_csv_strings : string list -> f:(string -> 'a) -> 'a t
  val of_digit_strings : string list -> int t

  (* The origin is at the top left of the grid. *)
  val get : 'a t -> Coord.t -> 'a option
  val get_exn : 'a t -> Coord.t -> 'a
  val set_exn : 'a t -> Coord.t -> 'a -> unit

  (* Gets the value, interpreting the [grid] as infinitely tiling. *)
  val get_tiling : 'a t -> Coord.t -> 'a
  val in_bounds : 'a t -> Coord.t -> bool
  val on_edge : 'a t -> Coord.t -> bool
  val iteri : 'a t -> f:(Coord.t -> 'a -> unit) -> unit
  val foldi : 'a t -> init:'accum -> f:(Coord.t -> 'accum -> 'a -> 'accum) -> 'accum
  val move_until_edge : 'a t -> Coord.t -> vec:Coord.t -> (Coord.t * 'a) list
  val get_row : 'a t -> int -> 'a list
  val get_col : 'a t -> int -> 'a list
  val rotate_cw : 'a t -> int -> 'a t
  val reflect_over_y : 'a t -> 'a t

  (* Returns coordinates of cardinal neighbors that are in bound *)
  val neighbors : 'a t -> Coord.t -> Coord.t list
end

module Ring_buffer : sig
  type 'a t

  val get : 'a t -> int -> 'a
  val set : 'a t -> int -> 'a -> unit
  val values : 'a t -> 'a list
  val create : 'a list -> 'a t
end
