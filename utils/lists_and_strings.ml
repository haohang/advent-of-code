open Core

let better_semantics_slice s i j ~empty ~slice ~length =
  if j = Int.max_value
  then slice s i (length s)
  else (
    let j = if j < 0 then length s + j else j in
    if i >= j then empty else slice s i j)
;;

let slice s i j =
  better_semantics_slice s i j ~empty:"" ~slice:String.slice ~length:String.length
;;

let slice_list l i j =
  better_semantics_slice l i j ~empty:[] ~slice:List.slice ~length:List.length
;;

let%expect_test "slice" =
  let test s i j = printf "%s" (slice s i j) in
  test "hello" 0 0;
  [%expect {||}];
  test "hello" 0 1;
  [%expect {| h |}];
  test "hello" 0 (-1);
  [%expect {| hell |}];
  test "hello" 0 Int.max_value;
  [%expect {| hello |}];
  test "hello" 2 1;
  [%expect {||}]
;;

let list_replace l ~i ~with_ = slice_list l 0 i @ [ with_ ] @ slice_list l (i + 1) 0
let min_elt_exn l ~compare = List.min_elt l ~compare |> Option.value_exn
let min_int_in_list_exn = min_elt_exn ~compare:Int.compare

let consecutive_pairs l =
  let a = List.drop_last_exn l in
  let b = List.tl_exn l in
  List.zip_exn a b
;;

let rec contains_two_in_a_row l ~equal =
  match l with
  | [] | [ _ ]     -> false
  | x :: y :: rest -> if equal x y then true else contains_two_in_a_row (y :: rest) ~equal
;;

let enumerate l = List.mapi l ~f:(fun i elt -> i, elt)

let partitioni l ~f =
  let first, second =
    List.foldi l ~init:([], []) ~f:(fun i (first, second) elt ->
        match f i elt with
        | `First  -> elt :: first, second
        | `Second -> first, elt :: second)
  in
  List.rev first, List.rev second
;;

let pad_left l ~len ~with_ = List.init (len - List.length l) ~f:(const with_) @ l
let pad_right l ~len ~with_ = l @ List.init (len - List.length l) ~f:(const with_)

let exactly_one_exn l =
  match l with
  | [] | _ :: _ :: _ -> raise_s [%message "List does not have exactly one input"]
  | [ elt ]          -> elt
;;
