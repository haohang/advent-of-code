(** Utilities for manipulating lists and strings. *)

(** Slices with better semantics than [Core]:

    [slice s a b] will take the substring of s from a to b
    - if b is [Int.max_value], then take the substring to the end
    - if b is negative, follow the same semantics as in python *)
val slice : string -> int -> int -> string

val slice_list : 'a list -> int -> int -> 'a list
val list_replace : 'a list -> i:int -> with_:'a -> 'a list
val min_elt_exn : 'a list -> compare:('a -> 'a -> int) -> 'a
val min_int_in_list_exn : int list -> int
val consecutive_pairs : 'a list -> ('a * 'a) list
val contains_two_in_a_row : 'a list -> equal:('a -> 'a -> bool) -> bool
val enumerate : 'a list -> (int * 'a) list
val partitioni : 'a list -> f:(int -> 'a -> [ `First | `Second ]) -> 'a list * 'a list
val exactly_one_exn : 'a list -> 'a
val pad_left : 'a list -> len:int -> with_:'a -> 'a list
val pad_right : 'a list -> len:int -> with_:'a -> 'a list
