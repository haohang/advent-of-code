open! Core

module Spec = struct
  (* Wrap some commonly used match elements *)
  module Atom = struct
    type t =
      | String
      | Int
      | Alphanumeric
      | Custom       of string
    [@@deriving sexp_of]

    let to_regex t =
      match t with
      | String       -> "[a-zA-Z]+"
      | Int          -> "[0-9]+"
      | Alphanumeric -> "[a-zA-Z0-9]+"
      | Custom s     -> s
    ;;

    let of_string s =
      match s with
      | "string"   -> String
      | "int"      -> Int
      | "alphanum" -> Alphanumeric
      | s          -> Custom s
    ;;
  end

  (* A captured element needs a label *)
  module Capture = struct
    type t = string * Atom.t [@@deriving sexp_of]

    let to_regex (_, atom) = sprintf !"(%s)" (Atom.to_regex atom)

    let of_string s =
      let name, atom = String.lsplit2_exn s ~on:' ' in
      name, Atom.of_string atom
    ;;
  end

  (* A single element can be captured or not *)
  module Elt = struct
    type t =
      | Capture    of Capture.t
      | No_capture of Atom.t
    [@@deriving sexp_of]

    let to_regex = function
      | Capture c    -> Capture.to_regex c
      | No_capture a -> Atom.to_regex a
    ;;

    let of_string s =
      match
        Option.bind (String.chop_prefix s ~prefix:"(") ~f:(String.chop_suffix ~suffix:")")
      with
      | None   -> No_capture (Atom.of_string s)
      | Some s -> Capture (Capture.of_string s)
    ;;
  end

  type t = Elt.t list [@@deriving sexp_of]

  let to_regex t = List.map t ~f:Elt.to_regex |> String.concat ~sep:""
  let create s = List.map s ~f:Elt.of_string

  let capture_groups t =
    List.filter_map t ~f:(function
        | Elt.Capture c -> Some c
        | No_capture _  -> None)
  ;;
end

module Result = struct
  type t = string String.Map.t

  let get t ~(field : ('t, 'field) Field.t) ~(f : string -> 'field) =
    String.Map.find_exn t (Field.name field) |> f
  ;;

  let get_int = get ~f:Int.of_string
  let get_string = get ~f:Fn.id
end

let parse ~s spec =
  let spec = Spec.create spec in
  let re2 = Spec.to_regex spec |> Re2.of_string in
  let result = Re2.find_submatches_exn re2 s in
  let capture_groups = Spec.capture_groups spec in
  List.mapi capture_groups ~f:(fun idx (name, _) ->
      name, Option.value_exn result.(idx + 1))
  |> String.Map.of_alist_exn
;;

let%expect_test "create" =
  let spec =
    [ "(lower int)"
    ; "-"
    ; "(upper int)"
    ; " "
    ; "(letter string)"
    ; ": "
    ; "(password string)"
    ]
  in
  Core.printf !"%{sexp: string String.Map.t}\n%!" (parse spec ~s:"1-3 a: abcde");
  [%expect {| ((letter a) (lower 1) (password abcde) (upper 3)) |}]
;;

let%test_module _ =
  (module struct
    module Target = struct
      type t =
        { lower : int
        ; upper : int
        ; letter : string
        ; password : string
        }
      [@@deriving fields, sexp_of]

      let creator, () =
        Fields.make_creator
          ~lower:(fun field () -> Result.get_int ~field, ())
          ~upper:(fun field () -> Result.get_int ~field, ())
          ~letter:(fun field () -> Result.get_string ~field, ())
          ~password:(fun field () -> Result.get_string ~field, ())
          ()
      ;;

      let spec =
        [ "(lower int)"
        ; "-"
        ; "(upper int)"
        ; " "
        ; "(letter string)"
        ; ": "
        ; "(password string)"
        ]
      ;;

      let of_string s =
        let result = parse ~s spec in
        creator result
      ;;

      let%expect_test "of_string" =
        Core.printf !"%{sexp: t}\n%!" (of_string "1-3 a: abcde");
        [%expect {| ((lower 1) (upper 3) (letter a) (password abcde)) |}]
      ;;
    end
  end)
;;
