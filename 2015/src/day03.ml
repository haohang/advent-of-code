open! Core
open! Std_internal

module Direction = struct
  module T = struct
    type t = Graph.Direction.t =
      | Up
      | Down
      | Left
      | Right
    [@@deriving compare]

    let assoc = [ '^', Up; '<', Left; '>', Right; 'v', Down ]
  end

  include T
  include Charable.Make (T)
end

let parse input = exactly_one_exn input |> String.to_list |> List.map ~f:Direction.of_char

let make_moves directions =
  let moves = List.map directions ~f:(fun d -> d, 1) in
  let _, visited = Graph.Move.apply_all moves Graph.Coord.origin in
  visited
;;

module Part1 = struct
  let f input =
    let directions = parse input in
    let result = make_moves directions |> Graph.Coord.Set.length in
    print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let f input =
    let directions = parse input in
    let first, second =
      List.foldi directions ~init:([], []) ~f:(fun i (first, second) elt ->
          if i % 2 = 0 then elt :: first, second else first, elt :: second)
    in
    let santa = make_moves (List.rev first) in
    let robo_santa = make_moves (List.rev second) in
    let houses_with_presents = Graph.Coord.Set.union santa robo_santa in
    print_s [%message "Done" ~result:(Graph.Coord.Set.length houses_with_presents : int)]
  ;;
end

let command = Commands.both ~day:3 ~part1:Part1.f ~part2:Part2.f
