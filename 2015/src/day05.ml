open! Core
open! Std_internal

let rec _contains_two_in_a_row l ~equal =
  match l with
  | [] | [ _ ]     -> false
  | x :: y :: rest ->
    if equal x y then true else _contains_two_in_a_row (y :: rest) ~equal
;;

let contains_two_in_a_row l ~equal =
  let a = List.drop_last_exn l in
  let b = List.tl_exn l in
  List.zip_exn a b |> List.exists ~f:(fun (a, b) -> equal a b)
;;

let is_vowel = List.mem [ 'a'; 'e'; 'i'; 'o'; 'u' ] ~equal:Char.equal
let contains_three_vowels s = String.count s ~f:is_vowel >= 3
let contains_twice_in_a_row s = contains_two_in_a_row (String.to_list s) ~equal:Char.equal

let contains_bad_strings s =
  List.exists [ "ab"; "cd"; "pq"; "xy" ] ~f:(fun substring ->
      String.is_substring s ~substring)
;;

module Part1 = struct
  let check s =
    (not (contains_bad_strings s)) && contains_twice_in_a_row s && contains_three_vowels s
  ;;

  let%expect_test _ =
    let test s = Core.printf !"nice? %b\n%!" (check s) in
    test "ugknbfddgicrmopn";
    [%expect {| nice? true |}];
    test "aaa";
    [%expect {| nice? true |}];
    test "jchzalrnumimnmhp";
    [%expect {| nice? false |}]
  ;;

  let f input =
    let result = List.count input ~f:check in
    print_s [%message "Done" (result : int)]
  ;;
end

let rec _check_consecutive_pairs s =
  match s with
  | [] | [ _ ] | [ _; _ ] | [ _; _; _ ] -> false
  | a :: b :: c :: d :: rest ->
    if [%compare.equal: char * char] (a, b) (c, d)
    then true
    else _check_consecutive_pairs (b :: c :: d :: rest)
;;

let check_consecutive_pairs s =
  let chars = String.to_list s in
  let a = List.drop_last_exn chars in
  let b = List.tl_exn chars in
  let pairs = List.zip_exn a b in
  List.existsi pairs ~f:(fun i pair ->
      List.existsi pairs ~f:(fun j candidate ->
          j > i + 1 && [%compare.equal: char * char] pair candidate))
;;

let check_alt_letters s =
  let chars = String.to_list s in
  let a = List.filteri chars ~f:(fun i _ -> i % 2 = 0) in
  let b = List.filteri chars ~f:(fun i _ -> i % 2 = 1) in
  contains_two_in_a_row a ~equal:Char.equal || contains_two_in_a_row b ~equal:Char.equal
;;

module Part2 = struct
  let check s =
    let a = check_alt_letters s in
    let b = check_consecutive_pairs s in
    (* printf "%b %b\n%!" a b; *)
    a && b
  ;;

  let%expect_test _ =
    let test s = Core.printf !"nice? %b\n%!" (check s) in
    test "qjhvhtzxzqqjkmpb";
    [%expect {| nice? true |}];
    test "xxyxx";
    [%expect {| nice? true |}];
    test "uurcxstgmygtbstg";
    [%expect {| nice? false |}];
    test "ieodomkazucvgmuy";
    [%expect {| nice? false |}]
  ;;

  let f input =
    let result = List.count input ~f:check in
    print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:5 ~part1:Part1.f ~part2:Part2.f
