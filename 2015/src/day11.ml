open! Core
open! Std_internal

let a_int = Char.to_int 'a'
let z_int = Char.to_int 'z'

let add c i =
  let next = Char.to_int c + i in
  if next > z_int
  then 1, Char.of_int_exn (a_int + (next - z_int - 1))
  else 0, Char.of_int_exn next
;;

let next_pw s =
  let carry, next =
    List.rev s |> List.fold_map ~init:1 ~f:(fun carry next -> add next carry)
  in
  let next = List.rev next in
  if carry = 1 then 'a' :: next else next
;;

let exists_invalid s =
  String.exists s ~f:(function
      | 'i' | 'o' | 'l' -> true
      | _               -> false)
;;

let sequences_of_three =
  List.init
    (z_int - a_int - 1)
    ~f:(fun i ->
      [ a_int + i; a_int + i + 1; a_int + i + 2 ]
      |> List.map ~f:Char.of_int_exn
      |> String.of_char_list)
;;

let exists_sequence s =
  List.exists sequences_of_three ~f:(fun substring -> String.is_substring s ~substring)
;;

let count_doubles s =
  List.group s ~break:(fun c1 c2 -> not (Char.equal c1 c2))
  |> List.fold ~init:0 ~f:(fun accum c -> accum + (List.length c / 2))
;;

let is_valid s =
  let doubles = count_doubles s in
  let s = String.of_char_list s in
  doubles >= 2 && exists_sequence s && not (exists_invalid s)
;;

module Part1 = struct
  let f input =
    let result = exactly_one_exn input |> String.to_list in
    let rec find s =
      let next = next_pw s in
      if is_valid next then next else find next
    in
    let result = find result |> String.of_char_list in
    print_s [%message "Done" (result : string)]
  ;;
end

module Part2 = struct
  let f input =
    let result = exactly_one_exn input |> String.to_list in
    let rec find s =
      let next = next_pw s in
      if is_valid next then next else find next
    in
    let result = find result |> find |> String.of_char_list in
    print_s [%message "Done" (result : string)]
  ;;
end

let command = Commands.both ~day:11 ~part1:Part1.f ~part2:Part2.f
