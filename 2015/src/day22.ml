open! Core
open! Std_internal

module Move = struct
  module Effect = struct
    type t =
      | Recharge
      | Shield
      | Poison
    [@@deriving enumerate, sexp]

    let cost = function
      | Recharge -> 229
      | Shield   -> 113
      | Poison   -> 173
    ;;

    let value = function
      | Recharge -> 101
      | Shield   -> 7
      | Poison   -> 3
    ;;

    let turns = function
      | Recharge        -> 5
      | Shield | Poison -> 6
    ;;
  end

  module Immediate = struct
    type t =
      | Magic_missile
      | Drain
    [@@deriving enumerate, sexp]

    let cost = function
      | Magic_missile -> 53
      | Drain         -> 73
    ;;

    let damage = function
      | Magic_missile -> 4
      | Drain         -> 2
    ;;

    let heal = function
      | Magic_missile -> 0
      | Drain         -> 2
    ;;
  end

  type t =
    | Immediate of Immediate.t
    | Effect    of Effect.t
  [@@deriving enumerate, sexp]

  let cost = function
    | Immediate i -> Immediate.cost i
    | Effect e    -> Effect.cost e
  ;;

  (* let min_cost =
   *   List.min_elt (List.map all ~f:cost) ~compare:Int.compare |> Option.value_exn
   * ;; *)
end

module State = struct
  type t =
    { shield_turns : int
    ; poison_turns : int
    ; recharge_turns : int
    ; mana : int
    ; mana_spent : int
    ; hp : int
    ; armor : int
    ; boss_hp : int
    ; whose_turn : [ `player | `boss ]
    }
  [@@deriving sexp]

  let apply_effects
      { shield_turns
      ; poison_turns
      ; recharge_turns
      ; mana
      ; mana_spent
      ; hp
      ; boss_hp
      ; whose_turn
      ; armor = _
      }
    =
    let armor = if shield_turns > 0 then Move.Effect.(value Shield) else 0 in
    let boss_hp =
      if poison_turns > 0 then boss_hp - Move.Effect.(value Poison) else boss_hp
    in
    let mana = if recharge_turns > 0 then mana + Move.Effect.(value Recharge) else mana in
    let shield_turns = Int.max 0 (shield_turns - 1) in
    let poison_turns = Int.max 0 (poison_turns - 1) in
    let recharge_turns = Int.max 0 (recharge_turns - 1) in
    { shield_turns
    ; poison_turns
    ; recharge_turns
    ; mana
    ; mana_spent
    ; hp
    ; armor
    ; boss_hp
    ; whose_turn
    }
  ;;

  let play_move ({ mana; mana_spent; hp; boss_hp; _ } as t) move =
    let cost = Move.cost move in
    if cost > t.mana
    then None
    else (
      let mana = mana - cost in
      let mana_spent = mana_spent + cost in
      let t = { t with mana; mana_spent; whose_turn = `boss } in
      match move with
      | Immediate i ->
        let boss_hp = boss_hp - Move.Immediate.damage i in
        let hp = hp + Move.Immediate.heal i in
        Some { t with boss_hp; hp }
      | Effect e    ->
        let turns = Move.Effect.turns e in
        (match e with
        | Move.Effect.Recharge when t.recharge_turns > 0 -> None
        | Poison when t.poison_turns > 0 -> None
        | Shield when t.shield_turns > 0 -> None
        | Recharge -> Some { t with recharge_turns = turns }
        | Poison -> Some { t with poison_turns = turns }
        | Shield -> Some { t with shield_turns = turns }))
  ;;
end

let play_one ?(hard_mode = false) state damage =
  let state =
    match hard_mode, state.State.whose_turn with
    | true, `player -> { state with hp = state.hp - 1 }
    | _             -> state
  in
  if state.hp <= 0
  then `lost
  else (
    (* apply effects *)
    let state = State.apply_effects state in
    (* loss conditions *)
    if state.boss_hp <= 0
    then `won state.mana_spent
    else if state.hp <= 0
    then `lost
    else (
      match state.whose_turn with
      | `player ->
        (match List.filter_map Move.all ~f:(fun move -> State.play_move state move) with
        | []     -> `lost
        | states -> `boss_to_move states)
      | `boss   ->
        let hp = state.hp - Int.max 1 (damage - state.armor) in
        `player_to_move { state with hp; whose_turn = `player }))
;;

let _play_test ~hp ~mana ~boss:(boss_hp, damage) =
  let state =
    `player_to_move
      { State.shield_turns = 0
      ; poison_turns = 0
      ; recharge_turns = 0
      ; hp
      ; mana
      ; boss_hp
      ; whose_turn = `player
      ; mana_spent = 0
      ; armor = 0
      }
  in
  let rec play result =
    match result with
    | `won _                -> Core.printf "Won\n%!"
    | `lost                 -> Core.printf "Lost\n%!"
    | `player_to_move state ->
      Core.print_s [%message (state : State.t)];
      let next = play_one state damage in
      play next
    | `boss_to_move states  ->
      List.iter states ~f:(fun state ->
          Core.print_s [%message (state : State.t)];
          let next = play_one state damage in
          play next)
  in
  play state
;;

let play_game ~hp ~mana ~boss:(boss_hp, damage) ~hard_mode =
  let state : State.t =
    { shield_turns = 0
    ; poison_turns = 0
    ; recharge_turns = 0
    ; hp
    ; mana
    ; boss_hp
    ; whose_turn = `player
    ; mana_spent = 0
    ; armor = 0
    }
  in
  let min_so_far = ref Int.max_value in
  let rec play state =
    if state.State.mana_spent >= !min_so_far
    then (
      Core.printf "Short circuit\n%!";
      None)
    else (
      match play_one state damage ~hard_mode with
      | `lost                 ->
        Core.printf "Lost\n%!";
        None
      | `won mana_spent       ->
        Core.printf "Won\n%!";
        min_so_far := mana_spent;
        Some mana_spent
      | `player_to_move state -> play state
      | `boss_to_move states  ->
        List.filter_map states ~f:play |> List.min_elt ~compare:Int.compare)
  in
  play state
;;

let parse s =
  match List.map s ~f:(String.split ~on:' ') with
  | [ [ "Hit"; "Points:"; hp ]; [ "Damage:"; damage ] ] ->
    Int.of_string hp, Int.of_string damage
  | _ -> raise_s [%message "Unparseable"]
;;

module Part1 = struct
  let f input =
    let boss = parse input in
    (* let () = play_test ~hp:10 ~mana:250 ~boss:(14, 8) in *)
    let result = play_game ~hp:50 ~mana:500 ~boss ~hard_mode:false in
    print_s [%message "Done" (result : int option)]
  ;;
end

module Part2 = struct
  let f input =
    let boss = parse input in
    (* let () = play_test ~hp:10 ~mana:250 ~boss:(14, 8) in *)
    let result = play_game ~hp:50 ~mana:500 ~boss ~hard_mode:true in
    print_s [%message "Done" (result : int option)]
  ;;
end

let command = Commands.both ~day:22 ~part1:Part1.f ~part2:Part2.f
