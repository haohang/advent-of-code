open! Core
open! Std_internal
open Graph

let parse_coord s =
  String.split s ~on:','
  |> function
  | [ a; b ] -> { Coord.row = Int.of_string a; col = Int.of_string b }
  | _        -> raise_s [%message "Invalid coordinate"]
;;

let gen_range coord1 coord2 =
  let coord1 = parse_coord coord1 in
  let coord2 = parse_coord coord2 in
  Coord.range coord1 coord2
;;

let parse s =
  String.split s ~on:' '
  |> function
  | [ "turn"; "on"; first; "through"; second ] -> `Turn_on (gen_range first second)
  | [ "turn"; "off"; first; "through"; second ] -> `Turn_off (gen_range first second)
  | [ "toggle"; first; "through"; second ] -> `Toggle (gen_range first second)
  | _ -> raise_s [%message "Invalid instruction" (s : string)]
;;

module Part1 = struct
  let execute instructions =
    List.fold instructions ~init:Coord.Set.empty ~f:(fun lights_on instruction ->
        match instruction with
        | `Turn_on l  -> Coord.Set.union lights_on (Coord.Set.of_list l)
        | `Turn_off l -> Coord.Set.diff lights_on (Coord.Set.of_list l)
        | `Toggle l   ->
          List.fold l ~init:lights_on ~f:(fun lights_on light ->
              if Coord.Set.mem lights_on light
              then Coord.Set.remove lights_on light
              else Coord.Set.add lights_on light))
  ;;

  let f input =
    let result = List.map input ~f:parse |> execute |> Coord.Set.length in
    print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let execute instructions =
    List.fold instructions ~init:Coord.Map.empty ~f:(fun lights_on instruction ->
        match instruction with
        | `Turn_on l  ->
          List.fold l ~init:lights_on ~f:(fun lights_on light ->
              match Coord.Map.find lights_on light with
              | None            -> Coord.Map.set lights_on ~key:light ~data:1
              | Some brightness ->
                Coord.Map.set lights_on ~key:light ~data:(brightness + 1))
        | `Turn_off l ->
          List.fold l ~init:lights_on ~f:(fun lights_on light ->
              match Coord.Map.find lights_on light with
              | None            -> Coord.Map.set lights_on ~key:light ~data:0
              | Some brightness ->
                Coord.Map.set lights_on ~key:light ~data:(Int.max (brightness - 1) 0))
        | `Toggle l   ->
          List.fold l ~init:lights_on ~f:(fun lights_on light ->
              match Coord.Map.find lights_on light with
              | None            -> Coord.Map.set lights_on ~key:light ~data:2
              | Some brightness ->
                Coord.Map.set lights_on ~key:light ~data:(brightness + 2)))
  ;;

  let f input =
    let result =
      List.map input ~f:parse
      |> execute
      |> Coord.Map.fold ~init:0 ~f:(fun ~key:_ ~data:brightness accum ->
             accum + brightness)
    in
    print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:6 ~part1:Part1.f ~part2:Part2.f
