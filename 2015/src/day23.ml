open! Core
open! Std_internal

module Register : sig
  type t [@@deriving sexp_of]

  include Hashable.S with type t := t

  val of_string : string -> t
  val create : char -> t
end = struct
  include Char

  let create = Fn.id
end

module Offset : sig
  type t [@@deriving sexp_of]

  val to_instr : t -> current_instr:int -> int
  val of_string : string -> t
end = struct
  type t = int [@@deriving sexp_of]

  let to_instr offset ~current_instr = offset + current_instr
  let of_string = Int.of_string
end

module Instruction = struct
  type t =
    | Hlf of Register.t
    | Tpl of Register.t
    | Inc of Register.t
    | Jmp of Offset.t
    | Jie of (Register.t * Offset.t)
    | Jio of (Register.t * Offset.t)
  [@@deriving sexp_of]

  (* TODO: Utility function to split on first of a char *)
  let of_string s =
    let first_space = String.index_exn s ' ' in
    let instr = slice s 0 first_space in
    let rest = slice s (first_space + 1) Int.max_value in
    let get_r_and_offset s' =
      match
        String.split_on_chars s' ~on:[ ' '; ',' ]
        |> List.filter ~f:(fun s -> not (String.is_empty s))
      with
      | [ r; offset ] -> Register.of_string r, Offset.of_string offset
      | _             -> raise_s
                           [%message "Unparseable r and offset" s s' (first_space : int)]
    in
    match instr with
    | "hlf" -> Hlf (Register.of_string rest)
    | "tpl" -> Tpl (Register.of_string rest)
    | "inc" -> Inc (Register.of_string rest)
    | "jmp" -> Jmp (Offset.of_string rest)
    | "jie" ->
      let r, offset = get_r_and_offset rest in
      Jie (r, offset)
    | "jio" ->
      let r, offset = get_r_and_offset rest in
      Jio (r, offset)
    | _     -> raise_s [%message "Unparseable" s]
  ;;
end

module Machine : sig
  type t

  val create : registers:Register.t list -> t
  val get : t -> Register.t -> int
  val update : t -> Register.t -> f:(int -> int) -> unit
end = struct
  type t = int Register.Table.t

  (* hlf r sets register r to half its current value, then continues with the next instruction.
     tpl r sets register r to triple its current value, then continues with the next instruction.
     inc r increments register r, adding 1 to it, then continues with the next instruction.
     jmp offset is a jump; it continues with the instruction offset away relative to itself.
     jie r, offset is like jmp, but only jumps if register r is even ("jump if even").
     jio r, offset is like jmp, but only jumps if register r is 1 ("jump if one", not odd). *)
  let create ~registers =
    Register.Table.of_alist_exn (List.map registers ~f:(fun r -> r, 0))
  ;;

  let get t r = Register.Table.find_exn t r
  let set t r data = Register.Table.set t ~key:r ~data
  let update t r ~f = set t r (f (get t r))
end

let execute instrs ~machine =
  let instrs = Array.of_list instrs in
  let num_instrs = Array.length instrs in
  let pc = ref 0 in
  let run_one () =
    let instr = instrs.(!pc) in
    match instr with
    | Instruction.Hlf r ->
      Machine.update machine r ~f:(fun i -> i / 2);
      incr pc
    | Tpl r             ->
      Machine.update machine r ~f:(( * ) 3);
      incr pc
    | Inc r             ->
      Machine.update machine r ~f:(( + ) 1);
      incr pc
    | Jmp o             -> pc := Offset.to_instr o ~current_instr:!pc
    | Jie (r, o)        ->
      if Machine.get machine r % 2 = 0
      then pc := Offset.to_instr o ~current_instr:!pc
      else incr pc
    | Jio (r, o)        ->
      if Machine.get machine r = 1
      then pc := Offset.to_instr o ~current_instr:!pc
      else incr pc
  in
  let rec helper () =
    if !pc < 0 || !pc >= num_instrs
    then Machine.get machine (Register.create 'b')
    else (
      run_one ();
      helper ())
  in
  helper ()
;;

module Part1 = struct
  let f input =
    let instructions = List.map input ~f:Instruction.of_string in
    let machine =
      Machine.create ~registers:[ Register.create 'a'; Register.create 'b' ]
    in
    let result = execute instructions ~machine in
    print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let f input =
    let instructions = List.map input ~f:Instruction.of_string in
    let machine =
      Machine.create ~registers:[ Register.create 'a'; Register.create 'b' ]
    in
    Machine.update machine (Register.create 'a') ~f:(( + ) 1);
    let result = execute instructions ~machine in
    print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:23 ~part1:Part1.f ~part2:Part2.f
