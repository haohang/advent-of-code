open! Core
open! Std_internal

let apply_look_and_say s =
  List.group s ~break:(fun c1 c2 -> not (Char.equal c1 c2))
  |> List.concat_map ~f:(fun l ->
         [ List.length l |> Int.to_string |> String.to_list |> exactly_one_exn
         ; List.hd_exn l
         ])
;;

module Part1 = struct
  let f input =
    let result =
      exactly_one_exn input
      |> String.to_list
      |> Fn.apply_n_times ~n:40 apply_look_and_say
      |> List.length
    in
    print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let f input =
    let result =
      exactly_one_exn input
      |> String.to_list
      |> Fn.apply_n_times ~n:50 apply_look_and_say
      |> List.length
    in
    print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:10 ~part1:Part1.f ~part2:Part2.f
