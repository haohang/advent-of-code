open! Core
open! Std_internal
open Graph

let board_dim = 100
let runs = 100

let parse s =
  Grid.of_char_strings s ~f:(Char.equal '#')
  |> Grid.foldi ~init:Coord.Set.empty ~f:(fun coord accum active ->
         if active then Coord.Set.add accum coord else accum)
;;

let adjacent c =
  let in_bounds i = i >= 0 && i < board_dim in
  Coord.adjacent c |> List.filter ~f:(fun { row; col } -> in_bounds row && in_bounds col)
;;

let run_one ?(force_active = const false) state =
  let currently_active = Coord.Set.to_list state in
  let all_to_consider =
    currently_active @ List.concat_map currently_active ~f:adjacent |> Coord.Set.of_list
  in
  Coord.Set.fold all_to_consider ~init:Coord.Set.empty ~f:(fun accum elt ->
      if force_active elt
      then Coord.Set.add accum elt
      else (
        let num_active_neighbors = adjacent elt |> List.count ~f:(Coord.Set.mem state) in
        let is_active = Coord.Set.mem state elt in
        match is_active, num_active_neighbors with
        | true, (2 | 3) | false, 3 -> Coord.Set.add accum elt
        | _                        -> accum))
;;

let run ?force_active state = Fn.apply_n_times ~n:runs (run_one ?force_active) state

module Part1 = struct
  let f input =
    let active = parse input in
    let result = run active |> Coord.Set.length in
    Core.print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let corners =
    [ { Coord.row = 0; col = 0 }
    ; { row = 0; col = board_dim - 1 }
    ; { row = board_dim - 1; col = 0 }
    ; { row = board_dim - 1; col = board_dim - 1 }
    ]
  ;;

  let is_corner c = List.mem corners c ~equal:Coord.equal

  let f input =
    let active = parse input |> Coord.Set.union (Coord.Set.of_list corners) in
    let result = run ~force_active:is_corner active |> Coord.Set.length in
    Core.print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:18 ~part1:Part1.f ~part2:Part2.f
