open! Core
open! Std_internal

module Edge = struct
  module T = struct
    type t =
      { start : string
      ; stop : string
      }
    [@@deriving sexp, compare, fields]
  end

  include T
  include Comparable.Make (T)
end

let parse_one input =
  match String.split input ~on:' ' with
  | [ a; "to"; b; "="; c ] -> a, b, Int.of_string c
  | _                      -> raise_s [%message "Invalid input line" (input : string)]
;;

let parse input =
  let distance_map =
    List.concat_map input ~f:(fun line ->
        let a, b, c = parse_one line in
        [ { Edge.start = a; stop = b }, c; { Edge.start = b; stop = a }, c ])
    |> Edge.Map.of_alist_exn
  in
  let cities =
    Edge.Map.keys distance_map
    |> List.map ~f:Edge.start
    |> List.dedup_and_sort ~compare:String.compare
  in
  cities, distance_map
;;

let distance trip distance_map =
  match trip with
  | first :: rest ->
    List.fold rest ~init:(0, first) ~f:(fun (dist, prev) next ->
        let edge = { Edge.start = prev; stop = next } in
        let d = Edge.Map.find_exn distance_map edge in
        d + dist, next)
    |> fst
  | _             -> raise_s [%message "invalid trip"]
;;

let find_shortest_path input =
  let cities, distance_map = parse input in
  let all_permutations = permutations cities in
  List.map all_permutations ~f:(fun perm -> distance perm distance_map)
  |> List.min_elt ~compare:Int.compare
  |> Option.value_exn
;;

let find_longest_path input =
  let cities, distance_map = parse input in
  let all_permutations = permutations cities in
  List.map all_permutations ~f:(fun perm -> distance perm distance_map)
  |> List.max_elt ~compare:Int.compare
  |> Option.value_exn
;;

module Part1 = struct
  let f input =
    let result = find_shortest_path input in
    print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let f input =
    let result = find_longest_path input in
    print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:9 ~part1:Part1.f ~part2:Part2.f
