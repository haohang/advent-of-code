open! Core
open! Std_internal

let shop =
  let parse_objects s =
    String.split_lines s
    |> List.map ~f:(fun s ->
           match String.split ~on:' ' (String.strip s) with
           | [ _name; cost; damage; armor ] ->
             Int.of_string cost, Int.of_string damage, Int.of_string armor
           | _ -> raise_s [%message "Unparseable" s])
  in
  let weapons =
    {|Dagger 8 4 0
Shortsword 10 5 0
Warhammer 25 6 0
Longsword 40 7 0
Greataxe 74 8 0|}
    |> parse_objects
  in
  let armor =
    {|Leather 13 0 1
 Chainmail 31 0 2
 Splintmail 53 0 3
 Bandedmail 75 0 4
 Platemail 102 0 5|}
    |> parse_objects
  in
  let rings =
    {|Damage+1 25 1 0
 Damage+2 50 2 0
 Damage+3 100 3 0
 Defense+1 20 0 1
 Defense+2 40 0 2
 Defense+3 80 0 3|}
    |> parse_objects
  in
  let ring_combos =
    let pick_two =
      List.concat_mapi rings ~f:(fun i (cost1, damage1, armor1) ->
          List.map
            (slice_list rings (i + 1) Int.max_value)
            ~f:(fun (cost2, damage2, armor2) ->
              cost1 + cost2, damage1 + damage2, armor1 + armor2))
    in
    ((0, 0, 0) :: rings) @ pick_two
  in
  List.cartesian_product weapons ((0, 0, 0) :: armor)
  |> List.cartesian_product ring_combos
  |> List.map
       ~f:(fun
            ( (r_cost, r_damage, r_armor)
            , ((w_cost, w_damage, w_armor), (a_cost, a_damage, a_armor)) )
          ->
         ( r_cost + w_cost + a_cost
         , r_damage + w_damage + a_damage
         , r_armor + w_armor + a_armor ))
;;

let%expect_test _ =
  Core.printf "%d\n%!" (List.length shop);
  [%expect {||}]
;;

let p1_wins (hp1, d1, a1) (hp2, d2, a2) =
  let per_turn_damage_dealt_to_p1 = d2 - a1 in
  let per_turn_damage_dealt_to_p2 = d1 - a2 in
  match per_turn_damage_dealt_to_p1 <= 0, per_turn_damage_dealt_to_p2 <= 0 with
  | true, false  -> `yes
  | false, true  -> `no
  | true, true   -> `tie
  | false, false ->
    let turns_survived hp damage = ((hp - 1) / damage) + 1 in
    let turns_p1 = turns_survived hp1 per_turn_damage_dealt_to_p1 in
    let turns_p2 = turns_survived hp2 per_turn_damage_dealt_to_p2 in
    if turns_p1 >= turns_p2 then `yes else `no
;;

let parse s =
  match List.map s ~f:(String.split ~on:' ') with
  | [ [ "Hit"; "Points:"; hp ]; [ "Damage:"; damage ]; [ "Armor:"; armor ] ] ->
    Int.of_string hp, Int.of_string damage, Int.of_string armor
  | _ -> raise_s [%message "Unparseable"]
;;

module Part1 = struct
  let f input =
    let boss = parse input in
    let player_hp = 100 in
    let result =
      List.filter shop ~f:(fun (_cost, damage, armor) ->
          match p1_wins (player_hp, damage, armor) boss with
          | `yes -> true
          | _    -> false)
      |> List.min_elt ~compare:(Comparable.lift Int.compare ~f:fst3)
      |> Option.value_exn
      |> fst3
    in
    print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let f input =
    let boss = parse input in
    let player_hp = 100 in
    let result =
      let p1_loses =
        List.filter shop ~f:(fun (_cost, damage, armor) ->
            match p1_wins (player_hp, damage, armor) boss with
            | `no -> true
            | _   -> false)
      in
      Core.printf !"%{sexp: (int * int * int) list}\n%!" p1_loses;
      p1_loses
      |> List.max_elt ~compare:(Comparable.lift Int.compare ~f:fst3)
      |> Option.value_exn
      |> fst3
    in
    print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:21 ~part1:Part1.f ~part2:Part2.f
