open! Core
open! Std_internal

let md5 text = Md5.to_hex (Md5.digest_string text)

let check_hash text ~num_zeros =
  let hash = md5 text in
  String.is_prefix hash ~prefix:(String.make num_zeros '0')
;;

let find_number key ~num_zeros =
  non_rec_find
    ~init:1
    ~check:(fun i -> check_hash ~num_zeros (sprintf "%s%d" key i))
    ~next:(fun i -> Some (i + 1))
  |> Option.value_exn
;;

module Part1 = struct
  let f input =
    let key = exactly_one_exn input in
    let result = find_number key ~num_zeros:5 in
    print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let f input =
    let key = exactly_one_exn input in
    let result = find_number key ~num_zeros:6 in
    print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:4 ~part1:Part1.f ~part2:Part2.f
