open! Core
open! Std_internal

let parse_one s =
  let prefix =
    "To continue, please consult the code grid in the manual.  Enter the code at "
  in
  match
    String.chop_prefix_exn s ~prefix
    |> String.split_on_chars ~on:[ ','; ' '; '.' ]
    |> List.filter ~f:(fun s -> not (String.is_empty s))
  with
  | [ "row"; row; "column"; column ] -> Int.of_string row, Int.of_string column
  | _ -> raise_s [%message "Unparseable"]
;;

let to_ordinal ~row ~column =
  let diagonal = row + column - 1 in
  let offset = column in
  let n = diagonal - 1 in
  (n * (n + 1) / 2) + offset
;;

let next_code n = n * 252533 % 33554393
let run ~n = Fn.apply_n_times ~n next_code 20151125

module Part1 = struct
  let f input =
    let row, column = List.map input ~f:parse_one |> exactly_one_exn in
    let n = to_ordinal ~row ~column - 1 in
    let result = run ~n in
    print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let f input =
    let info = List.map input ~f:parse_one in
    let result = List.length info in
    print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:25 ~part1:Part1.f ~part2:Part2.f
