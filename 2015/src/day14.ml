open! Core
open! Std_internal

let parse_one s =
  match String.split s ~on:' ' with
  | [ name
    ; "can"
    ; "fly"
    ; speed
    ; "km/s"
    ; "for"
    ; fly
    ; "seconds,"
    ; "but"
    ; "then"
    ; "must"
    ; "rest"
    ; "for"
    ; rest
    ; "seconds."
    ] -> name, (Int.of_string speed, Int.of_string fly, Int.of_string rest)
  | _ -> raise_s [%message "Unparseable" s]
;;

let compute (_name, (speed, fly, rest)) ~seconds =
  let full_cycles = seconds / (fly + rest) in
  let remaining_time = seconds % (fly + rest) in
  let seconds_in_flight = (full_cycles * fly) + Int.min remaining_time fly in
  seconds_in_flight * speed
;;

module Part1 = struct
  let f input =
    let info = List.map input ~f:parse_one in
    let result = List.map info ~f:(compute ~seconds:2503) |> List.reduce_exn ~f:Int.max in
    print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let find_winners infos ~seconds =
    let scores =
      List.map infos ~f:(fun ((name, _) as info) -> name, compute info ~seconds)
      |> List.sort ~compare:(Comparable.lift Int.compare ~f:snd)
      |> List.rev
    in
    let best = List.hd_exn scores |> snd in
    List.take_while scores ~f:(fun (_, score) -> score = best) |> List.map ~f:fst
  ;;

  let f input =
    let total_time = 2503 in
    let info = List.map input ~f:parse_one in
    let winners =
      List.init total_time ~f:(( + ) 1)
      |> List.fold ~init:String.Map.empty ~f:(fun accum seconds ->
             let winners = find_winners info ~seconds in
             List.fold winners ~init:accum ~f:(fun accum name ->
                 String.Map.update accum name ~f:(function
                     | None   -> 1
                     | Some i -> i + 1)))
      |> String.Map.to_alist
    in
    let result =
      List.max_elt winners ~compare:(Comparable.lift Int.compare ~f:snd)
      |> Option.value_exn
    in
    print_s [%message "Done" (result : string * int) (winners : (string * int) list)]
  ;;
end

let command = Commands.both ~day:14 ~part1:Part1.f ~part2:Part2.f
