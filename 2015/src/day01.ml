open! Core
open! Std_internal

module Part1 = struct
  let f input =
    let instructions = exactly_one_exn input in
    let result =
      String.count instructions ~f:(Char.equal '(')
      - String.count instructions ~f:(Char.equal ')')
    in
    print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let f input =
    let instructions = exactly_one_exn input in
    let result =
      String.foldi instructions ~init:(`Not_hit_basement_yet 0) ~f:(fun i state c ->
          match state with
          | `Hit_basement_at_instruction _ -> state
          | `Not_hit_basement_yet curr_floor ->
            let change_in_floor =
              match c with
              | '(' -> 1
              | ')' -> -1
              | _   -> 0
            in
            let next_floor = curr_floor + change_in_floor in
            if next_floor = -1
            then `Hit_basement_at_instruction (i + 1)
            else `Not_hit_basement_yet next_floor)
    in
    print_s
      [%message
        "Done"
          (result
            : [ `Hit_basement_at_instruction of int
              | `Not_hit_basement_yet        of int
              ])]
  ;;
end

let command = Commands.both ~day:1 ~part1:Part1.f ~part2:Part2.f
