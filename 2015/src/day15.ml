open! Core
open! Std_internal

module Axes = struct
  type t =
    { capacity : int
    ; durability : int
    ; flavor : int
    ; texture : int
    ; calories : int
    }

  let add t1 t2 =
    { capacity = t1.capacity + t2.capacity
    ; durability = t1.durability + t2.durability
    ; flavor = t1.flavor + t2.flavor
    ; texture = t1.texture + t2.texture
    ; calories = t1.calories + t2.calories
    }
  ;;

  let scale t c =
    { capacity = t.capacity * c
    ; durability = t.durability * c
    ; flavor = t.flavor * c
    ; texture = t.texture * c
    ; calories = t.calories * c
    }
  ;;

  let reduce { capacity; durability; flavor; texture; calories = _ } =
    let normalize = Int.max 0 in
    normalize capacity * normalize durability * normalize flavor * normalize texture
  ;;
end

module Ingredient = struct
  type t =
    { axes : Axes.t
    ; name : string
    }

  let of_string s =
    match String.split s ~on:' ' with
    | [ name
      ; "capacity"
      ; capacity
      ; "durability"
      ; durability
      ; "flavor"
      ; flavor
      ; "texture"
      ; texture
      ; "calories"
      ; calories
      ] ->
      let name = String.chop_suffix_exn name ~suffix:":" in
      let to_int s = String.strip s ~drop:(Char.equal ',') |> Int.of_string in
      { name
      ; axes =
          { Axes.capacity = to_int capacity
          ; durability = to_int durability
          ; flavor = to_int flavor
          ; texture = to_int texture
          ; calories = to_int calories
          }
      }
    | _ -> raise_s [%message "Unparseable" s]
  ;;
end

let allocate2 =
  Memo.general (fun total -> List.init (total + 1) ~f:(fun i -> i, total - i))
;;

let allocate3 =
  Memo.general (fun total ->
      List.init (total + 1) ~f:Fn.id
      |> List.concat_map ~f:(fun i ->
             List.map (allocate2 (total - i)) ~f:(fun (a, b) -> i, a, b)))
;;

let allocate4 =
  Memo.general (fun total ->
      List.init (total + 1) ~f:Fn.id
      |> List.concat_map ~f:(fun i ->
             List.map (allocate3 (total - i)) ~f:(fun (a, b, c) -> [ i; a; b; c ])))
;;

let rec allocate' total n =
  if n = 1
  then [ [ total ] ]
  else
    List.init (total + 1) ~f:Fn.id
    |> List.concat_map ~f:(fun i ->
           let prev = allocate' (total - i) (n - 1) in
           List.map prev ~f:(fun p -> i :: p))
;;

let _allocate = Memo.general (fun (total, n) -> allocate' total n)

module Part1 = struct
  let score ingredients =
    List.map ingredients ~f:(fun (tsp, ingredient) ->
        Axes.scale ingredient.Ingredient.axes tsp)
    |> List.reduce_exn ~f:Axes.add
    |> Axes.reduce
  ;;

  let f input =
    let ingredients = List.map input ~f:Ingredient.of_string in
    let allocations = allocate4 100 in
    let result =
      List.map allocations ~f:(fun a -> List.zip_exn a ingredients |> score)
      |> List.max_elt ~compare:Int.compare
      |> Option.value_exn
    in
    print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let score ingredients =
    List.map ingredients ~f:(fun (tsp, ingredient) ->
        Axes.scale ingredient.Ingredient.axes tsp)
    |> List.reduce_exn ~f:Axes.add
    |> fun t -> if t.Axes.calories > 500 then 0 else Axes.reduce t
  ;;

  let f input =
    let ingredients = List.map input ~f:Ingredient.of_string in
    let allocations = allocate4 100 in
    let result =
      List.map allocations ~f:(fun a -> List.zip_exn a ingredients |> score)
      |> List.max_elt ~compare:Int.compare
      |> Option.value_exn
    in
    print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:15 ~part1:Part1.f ~part2:Part2.f
