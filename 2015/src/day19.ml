open! Core
open! Std_internal

let parse_replacement s =
  match String.split s ~on:' ' with
  | [ original; "=>"; replacement ] -> original, replacement
  | _ -> raise_s [%message "Unparseable" s]
;;

let parse s =
  match coagulate s with
  | [ replacements; medicine ] ->
    let replacements = List.map replacements ~f:parse_replacement in
    let medicine = exactly_one_exn medicine in
    replacements, medicine
  | _                          -> raise_s [%message "Unparseable" (s : string list)]
;;

let parse' s =
  match coagulate s with
  | [ replacements; medicine ] ->
    let replacements =
      List.map replacements ~f:(fun s ->
          let a, b = parse_replacement s in
          b, a)
    in
    let medicine = exactly_one_exn medicine in
    replacements, medicine
  | _                          -> raise_s [%message "Unparseable" (s : string list)]
;;

let find_all_replacements replacements medicine =
  List.fold replacements ~init:String.Set.empty ~f:(fun accum (original, replacement) ->
      let original_len = String.length original in
      let new_compounds =
        String.substr_index_all medicine ~may_overlap:false ~pattern:original
        |> List.map ~f:(fun i ->
               slice medicine 0 i
               ^ replacement
               ^ slice medicine (i + original_len) Int.max_value)
        |> String.Set.of_list
      in
      String.Set.union accum new_compounds)
;;

let find_all_replacements_with_path replacements medicine =
  List.concat_map replacements ~f:(fun (original, replacement) ->
      let original_len = String.length original in
      String.substr_index_all medicine ~may_overlap:false ~pattern:original
      |> List.map ~f:(fun i ->
             ( (original, replacement)
             , slice medicine 0 i
               ^ replacement
               ^ slice medicine (i + original_len) Int.max_value )))
;;

module Part1 = struct
  let f input =
    let replacements, medicine = parse input in
    let all_new_compounds = find_all_replacements replacements medicine in
    let result = String.Set.length all_new_compounds in
    print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let find_medicine_dfs replacements medicine =
    let start = medicine in
    let target = "e" in
    let queue = String.Hash_queue.create () in
    let seen = String.Hash_set.create () in
    String.Hash_queue.enqueue_back_exn queue start (0, start, []);
    Hash_set.add seen start;
    let rec run () =
      let n, next, path = Hash_queue.dequeue_back_exn queue in
      if String.equal next target
      then n, path
      else (
        let new_compounds = find_all_replacements_with_path replacements next in
        List.iter new_compounds ~f:(fun (rule, elt) ->
            if not (Hash_set.mem seen elt)
            then (
              Hash_set.add seen elt;
              Hash_queue.enqueue_back_exn queue elt (n + 1, elt, rule :: path)));
        run ())
    in
    run ()
  ;;

  let _find_medicine_bfs replacements medicine =
    let last = ref 0 in
    let start = medicine in
    let target = "e" in
    let queue = String.Hash_queue.create () in
    let seen = String.Hash_set.create () in
    String.Hash_queue.enqueue_back_exn queue start (0, start);
    Hash_set.add seen start;
    let rec run () =
      let n, next = Hash_queue.dequeue_front_exn queue in
      if n <> !last
      then (
        printf !"depth %d\n%!" n;
        last := n);
      if String.equal next target
      then n
      else (
        let new_compounds = find_all_replacements replacements next in
        String.Set.iter new_compounds ~f:(fun elt ->
            if not (Hash_set.mem seen elt)
            then (
              Hash_set.add seen elt;
              Hash_queue.enqueue_back_exn queue elt (n + 1, elt)));
        run ())
    in
    run ()
  ;;

  let _find_medicine replacements target =
    let loop = ref 0 in
    let current = ref (String.Set.singleton "e") in
    let seen = String.Hash_set.create () in
    Hash_set.add seen "e";
    let rec run () =
      if String.Set.mem !current target
      then !loop
      else (
        let new_set =
          String.Set.to_list !current
          |> List.map ~f:(find_all_replacements replacements)
          |> String.Set.union_list
          |> String.Set.filter ~f:(fun elt ->
                 if not (Hash_set.mem seen elt)
                 then (
                   Hash_set.add seen elt;
                   true)
                 else false)
        in
        current := new_set;
        incr loop;
        run ())
    in
    run ()
  ;;

  let f input =
    let replacements, medicine = parse' input in
    let result, path = find_medicine_dfs replacements medicine in
    print_s [%message "Done" (result : int) (path : (string * string) list)]
  ;;
end

let command = Commands.both ~day:19 ~part1:Part1.f ~part2:Part2.f
