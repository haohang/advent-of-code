open! Core
open! Std_internal

let sum' json ~sum_list =
  match json with
  | `Bool _ | `Float _ | `Null | `String _ -> 0
  | `Int i -> i
  | `Assoc l -> List.map l ~f:snd |> sum_list
  | `List l -> sum_list l
;;

module Part1 = struct
  let rec sum =
    let sum_list l = List.map l ~f:sum |> List.fold ~init:0 ~f:( + ) in
    sum' ~sum_list
  ;;

  let f input =
    let l = List.map input ~f:Yojson.Basic.from_string in
    let result = List.map l ~f:sum |> List.fold ~init:0 ~f:( + ) in
    print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let is_red json =
    match json with
    | `String "red" -> true
    | _             -> false
  ;;

  let should_ignore json =
    match json with
    | `Assoc l -> List.exists l ~f:(fun (_, j) -> is_red j)
    | _        -> false
  ;;

  let rec sum =
    let sum_list l =
      List.filter l ~f:(fun j -> not (should_ignore j))
      |> List.map ~f:sum
      |> List.fold ~init:0 ~f:( + )
    in
    sum' ~sum_list
  ;;

  let%expect_test _ =
    let json = {| [1,{"c":"red","b":2},3] |} |> Yojson.Basic.from_string in
    printf "%d\n%!" (sum json);
    [%expect {||}]
  ;;

  let f input =
    let l = List.map input ~f:Yojson.Basic.from_string in
    let result = List.map l ~f:sum |> List.fold ~init:0 ~f:( + ) in
    print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:12 ~part1:Part1.f ~part2:Part2.f
