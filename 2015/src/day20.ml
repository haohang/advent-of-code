open! Core
open! Std_internal

let parse s = exactly_one_exn s |> Int.of_string

let find_threshold threshold ~num_presents =
  let found = Set_once.create () in
  let i = ref 1 in
  while Set_once.is_none found do
    let next = num_presents !i in
    if next >= threshold then Set_once.set_exn found [%here] !i else incr i
  done;
  Set_once.get_exn found [%here]
;;

module Part1 = struct
  let num_presents i =
    let total = ref 0 in
    for elf = 1 to Float.of_int i ** 0.5 |> Float.to_int do
      if i % elf = 0
      then (
        total := !total + elf;
        if i / elf <> elf then total := !total + (i / elf))
    done;
    !total * 10
  ;;

  let f input =
    let threshold = parse input in
    let result = find_threshold threshold ~num_presents in
    print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let num_presents i =
    let total = ref 0 in
    for elf = 1 to Float.of_int i ** 0.5 |> Float.to_int do
      if i % elf = 0
      then (
        let n = i / elf in
        if n <= 50 then total := !total + elf;
        if n <> elf && elf <= 50 then total := !total + n)
    done;
    !total * 11
  ;;

  let f input =
    let threshold = parse input in
    let result = find_threshold threshold ~num_presents in
    print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:20 ~part1:Part1.f ~part2:Part2.f
