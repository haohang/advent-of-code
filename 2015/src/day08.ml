open! Core
open! Std_internal

module Part1 = struct
  let how_much_shorter_in_memory s =
    let s =
      String.chop_prefix_if_exists s ~prefix:{|"|}
      |> String.chop_suffix_if_exists ~suffix:{|"|}
    in
    let replace s pattern =
      let count = String.substr_index_all s ~may_overlap:false ~pattern |> List.length in
      let s = String.substr_replace_all s ~pattern ~with_:"REPLACED" in
      s, count
    in
    let s, num_quotes = replace s {|\"|} in
    let s, num_slash = replace s {|\\|} in
    let _s, num_hex = replace s {|\x|} in
    num_quotes + num_slash + (num_hex * 3) + 2
  ;;

  let f input =
    let result =
      List.map input ~f:how_much_shorter_in_memory |> List.reduce_exn ~f:( + )
    in
    print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let how_much_longer_escaped s =
    let num_quotes = String.count s ~f:(Char.equal '"') in
    let num_slash = String.count s ~f:(Char.equal '\\') in
    num_quotes + num_slash + 2
  ;;

  let f input =
    let result = List.map input ~f:how_much_longer_escaped |> List.reduce_exn ~f:( + ) in
    print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:8 ~part1:Part1.f ~part2:Part2.f
