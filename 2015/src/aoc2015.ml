open Core

let command =
  Command.group
    ~summary:"Advent of Code 2015"
    [ Day01.command
    ; Day02.command
    ; Day03.command
    ; Day04.command
    ; Day05.command
    ; Day06.command
    ; Day07.command
    ; Day08.command
    ; Day09.command
    ; Day10.command
    ; Day11.command
    ; Day12.command
    ; Day13.command
    ; Day14.command
    ; Day15.command
    ; Day16.command
    ; Day17.command
    ; Day18.command
    ; Day19.command
    ; Day20.command
    ; Day21.command
    ; Day22.command
    ; Day23.command
    ; Day24.command
    ; Day25.command
    ]
;;
