open! Core
open! Std_internal

let parse_one s =
  match String.split s ~on:' ' with
  | [ person1
    ; "would"
    ; direction
    ; happiness
    ; "happiness"
    ; "units"
    ; "by"
    ; "sitting"
    ; "next"
    ; "to"
    ; person2
    ] ->
    let sign =
      match direction with
      | "gain" -> 1
      | "lose" -> -1
      | _      -> raise_s [%message "Unparseable" s]
    in
    person1, (String.chop_suffix_exn person2 ~suffix:".", Int.of_string happiness * sign)
  | _ -> raise_s [%message "Unparseable" s]
;;

let make_happiness_map edges =
  String.Map.of_alist_multi edges |> String.Map.map ~f:String.Map.of_alist_exn
;;

let compute_happiness ~get_happiness arrangement =
  List.zip_exn
    arrangement
    (slice_list arrangement 1 Int.max_value @ [ List.hd_exn arrangement ])
  |> List.map ~f:(fun (p1, p2) ->
         let p1_happiness = get_happiness p1 p2 in
         let p2_happiness = get_happiness p2 p1 in
         p1_happiness + p2_happiness)
  |> List.reduce_exn ~f:( + )
;;

module Part1 = struct
  let f input =
    let happiness_map = List.map input ~f:parse_one |> make_happiness_map in
    let get_happiness p1 p2 =
      let p1_map = String.Map.find_exn happiness_map p1 in
      String.Map.find_exn p1_map p2
    in
    let permutations = permutations_string (String.Map.keys happiness_map) in
    let result =
      List.map permutations ~f:(compute_happiness ~get_happiness)
      |> List.reduce_exn ~f:Int.max
    in
    print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let f input =
    let happiness_map = List.map input ~f:parse_one |> make_happiness_map in
    let get_happiness p1 p2 =
      if String.equal p1 "Haohang" || String.equal p2 "Haohang"
      then 0
      else (
        let p1_map = String.Map.find_exn happiness_map p1 in
        String.Map.find_exn p1_map p2)
    in
    let people = String.Map.keys happiness_map in
    let permutations = permutations_string ("Haohang" :: people) in
    Core.print_s [%message "HXU- done with permutations"];
    let result =
      List.map permutations ~f:(compute_happiness ~get_happiness)
      |> List.reduce_exn ~f:Int.max
    in
    print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:13 ~part1:Part1.f ~part2:Part2.f
