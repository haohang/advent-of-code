open! Core
open! Std_internal

let parse_one input =
  match String.split input ~on:'x' |> List.map ~f:Int.of_string with
  | [ x; y; z ] -> x, y, z
  | _           -> raise_s [%message "Invalid input line" (input : string)]
;;

let parse input = List.map input ~f:parse_one

module Part1 = struct
  let paper_area (l, w, h) =
    let surface_area = (2 * l * w) + (2 * l * h) + (2 * w * h) in
    let extra =
      Option.value_exn (List.min_elt ~compare:Int.compare [ l * w; l * h; w * h ])
    in
    surface_area + extra
  ;;

  let f input =
    let gifts = parse input in
    let result = List.map gifts ~f:paper_area |> List.reduce_exn ~f:( + ) in
    print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let ribbon_length (l, w, h) =
    let wrap =
      Option.value_exn
        (List.min_elt ~compare:Int.compare [ 2 * (l + w); 2 * (l + h); 2 * (w + h) ])
    in
    let bow = l * w * h in
    wrap + bow
  ;;

  let f input =
    let gifts = parse input in
    let result = List.map gifts ~f:ribbon_length |> List.reduce_exn ~f:( + ) in
    print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:2 ~part1:Part1.f ~part2:Part2.f
