open! Core
open! Std_internal

let parse_component c =
  match String.split (String.strip c) ~on:':' with
  | [ element; amount ] -> element, Int.of_string (String.strip amount)
  | _                   -> raise_s [%message "Unparseable" c]
;;

let parse_one s =
  let first_colon = String.index_exn s ':' in
  let number =
    slice s 0 first_colon |> String.chop_prefix_exn ~prefix:"Sue " |> Int.of_string
  in
  let components =
    slice s (first_colon + 2) Int.max_value
    |> String.split ~on:','
    |> List.map ~f:parse_component
  in
  number, components
;;

let analysis =
  {|
children: 3
cats: 7
samoyeds: 2
pomeranians: 3
akitas: 0
vizslas: 0
goldfish: 5
trees: 3
cars: 2
perfumes: 1
|}
  |> String.strip
  |> String.split_lines
  |> List.map ~f:parse_component
  |> String.Map.of_alist_exn
;;

module Part1 = struct
  let check info =
    List.for_all info ~f:(fun (component, amount) ->
        amount = String.Map.find_exn analysis component)
  ;;

  let f input =
    let info = List.map input ~f:parse_one in
    let result, _ =
      List.filter info ~f:(fun (_i, info) -> check info) |> exactly_one_exn
    in
    print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let check info =
    List.for_all info ~f:(fun (component, amount) ->
        let sample_amount = String.Map.find_exn analysis component in
        match component with
        | "cats" | "trees"           -> amount > sample_amount
        | "pomeranians" | "goldfish" -> amount < sample_amount
        | _                          -> amount = sample_amount)
  ;;

  let f input =
    let info = List.map input ~f:parse_one in
    let result, _ =
      List.filter info ~f:(fun (_i, info) -> check info) |> exactly_one_exn
    in
    print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:16 ~part1:Part1.f ~part2:Part2.f
