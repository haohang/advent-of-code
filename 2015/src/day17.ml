open! Core
open! Std_internal

let total_eggnog = 150
let parse_one = Int.of_string

let rec allocate eggnog containers =
  match containers with
  | []        -> if eggnog = 0 then 1 else 0
  | c :: rest -> allocate (eggnog - c) rest + allocate eggnog rest
;;

let%expect_test _ =
  Core.printf "%d\n%!" (allocate 25 [ 10; 15; 20; 5; 5 ]);
  [%expect {| 4 |}]
;;

let rec allocate_with_n eggnog containers used =
  match containers with
  | []        -> if eggnog = 0 then [ used ] else []
  | c :: rest ->
    List.concat
      [ allocate_with_n (eggnog - c) rest (used + 1); allocate_with_n eggnog rest used ]
;;

let%expect_test _ =
  Core.printf !"%{sexp: int list}\n%!" (allocate_with_n 25 [ 10; 15; 20; 5; 5 ] 0);
  [%expect {| (2 3 2 2) |}]
;;

module Part1 = struct
  let f input =
    let containers = List.map input ~f:parse_one in
    let result = allocate total_eggnog containers in
    print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let f input =
    let containers = List.map input ~f:parse_one in
    let result =
      let allocations = allocate_with_n total_eggnog containers 0 in
      let min = List.min_elt allocations ~compare:Int.min |> Option.value_exn in
      List.count allocations ~f:(( = ) min)
    in
    print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:17 ~part1:Part1.f ~part2:Part2.f
