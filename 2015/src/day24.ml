open! Core
open! Std_internal

let parse_one = Int.of_string

module Compartment = struct
  type t =
    { num_gifts : int
    ; total_weight : int
    ; quantum_entanglement : int
    }
  [@@deriving sexp, compare, hash]

  let create () = { num_gifts = 0; total_weight = 0; quantum_entanglement = 1 }

  let add_one t weight =
    { num_gifts = t.num_gifts + 1
    ; total_weight = t.total_weight + weight
    ; quantum_entanglement = t.quantum_entanglement * weight
    }
  ;;
end

let bfs weights ~target =
  let queue = Deque.create () in
  let best_so_far = ref None in
  let is_best c1 =
    match !best_so_far with
    | None   -> true
    | Some c ->
      c1.Compartment.num_gifts < c.Compartment.num_gifts
      || (c1.num_gifts = c.num_gifts && c1.quantum_entanglement < c.quantum_entanglement)
  in
  Deque.enqueue_back queue (weights, Compartment.create ());
  while not (Deque.is_empty queue) do
    let remaining_gifts, c1 = Deque.dequeue_front_exn queue in
    if c1.total_weight = target
    then (if is_best c1 then best_so_far := Some c1)
    else (
      match remaining_gifts with
      | []           -> ()
      | next :: rest ->
        let next = Compartment.add_one c1 next in
        if next.total_weight <= target && is_best next
        then Deque.enqueue_back queue (rest, next);
        Deque.enqueue_back queue (rest, c1))
  done;
  !best_so_far
;;

let%expect_test _ =
  let result = bfs [ 1; 2; 3; 4; 5; 7; 8; 9; 10; 11 ] ~target:20 in
  Core.print_s [%message (result : Compartment.t option)];
  [%expect {||}]
;;

module Part1 = struct
  let f input =
    let info = List.map input ~f:parse_one in
    let total = List.reduce_exn info ~f:( + ) in
    let result' = bfs info ~target:(total / 3) |> Option.value_exn in
    let result = result'.Compartment.quantum_entanglement in
    print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let f input =
    let info = List.map input ~f:parse_one in
    let total = List.reduce_exn info ~f:( + ) in
    let result' = bfs info ~target:(total / 4) |> Option.value_exn in
    let result = result'.Compartment.quantum_entanglement in
    print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:24 ~part1:Part1.f ~part2:Part2.f
