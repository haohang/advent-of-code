open! Core
open! Std_internal
module Wire = String

module Wire_or_value = struct
  type t =
    | Wire  of Wire.t
    | Value of int
  [@@deriving sexp]

  let of_string s =
    match Or_error.try_with (fun () -> Int.of_string s) with
    | Ok i    -> Value i
    | Error _ -> Wire s
  ;;
end

module Signal_input = struct
  type t =
    | And    of Wire_or_value.t * Wire_or_value.t
    | Assign of Wire_or_value.t
    | LShift of Wire_or_value.t * int
    | Not    of Wire_or_value.t
    | Or     of Wire_or_value.t * Wire_or_value.t
    | RShift of Wire_or_value.t * int
  [@@deriving sexp_of]
end

let parse_one input =
  let w = Wire_or_value.of_string in
  match String.split input ~on:' ' with
  | [ value; "->"; target ] -> target, Signal_input.Assign (w value)
  | [ x; "AND"; y; "->"; target ] -> target, And (w x, w y)
  | [ x; "OR"; y; "->"; target ] -> target, Or (w x, w y)
  | [ x; "LSHIFT"; n; "->"; target ] -> target, LShift (w x, Int.of_string n)
  | [ x; "RSHIFT"; n; "->"; target ] -> target, RShift (w x, Int.of_string n)
  | [ "NOT"; x; "->"; target ] -> target, Not (w x)
  | _ -> raise_s [%message "Invalid input line" (input : string)]
;;

let%expect_test "parse_one" =
  let test s = Core.printf !"%{sexp: string * Signal_input.t}\n%!" (parse_one s) in
  test "123 -> x";
  [%expect {| (x (Assign 123)) |}];
  test "456 -> y";
  [%expect {| (y (Assign 456)) |}];
  test "x AND y -> d";
  [%expect {| (d (And (x y))) |}];
  test "x OR y -> e";
  [%expect {| (e (Or (x y))) |}];
  test "x LSHIFT 2 -> f";
  [%expect {| (f (LShift (x 2))) |}];
  test "y RSHIFT 2 -> g";
  [%expect {| (g (RShift (y 2))) |}];
  test "NOT x -> h";
  [%expect {| (h (Not x)) |}];
  test "NOT y -> i";
  [%expect {| (i (Not y)) |}]
;;

let process input = List.map input ~f:parse_one |> String.Map.of_alist_exn

let resolve input wire =
  let resolved = String.Table.create () in
  let rec find input wire =
    match String.Map.find_exn input wire with
    | Signal_input.Assign a -> get_wire_or_value input a
    | And (a, b)            -> get_wire_or_value input a land get_wire_or_value input b
    | Or (a, b)             -> get_wire_or_value input a lor get_wire_or_value input b
    | Not a                 -> lnot (get_wire_or_value input a)
    | LShift (a, n)         -> get_wire_or_value input a lsl n
    | RShift (a, n)         -> get_wire_or_value input a lsr n
  and get_wire_or_value input wire_or_value =
    match wire_or_value with
    | Value v -> v
    | Wire w  -> String.Table.find_or_add resolved w ~default:(fun () -> find input w)
  in
  find input wire
;;

module Part1 = struct
  let f input =
    let input = process input in
    let result = resolve input "a" in
    print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let f input =
    let input = process input |> String.Map.set ~key:"b" ~data:(Assign (Value 16076)) in
    let result = resolve input "a" in
    print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:7 ~part1:Part1.f ~part2:Part2.f
