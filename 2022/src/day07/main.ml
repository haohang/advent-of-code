open! Core
open Common_2022.Common

module Config = struct
  let day = 07

  let short_input =
    {|$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k|}
  ;;

  let test_inputs = [||]
end

module Parsed = struct
  type node =
    [ `leaf of int
    | `dir
    ]
  [@@deriving sexp]

  type t = string list String.Map.t * node String.Map.t [@@deriving sexp]

  let path_to_string = String.concat ~sep:""

  let parse input =
    let _, children, nodes =
      List.group input ~break:(fun _ next -> String.is_prefix next ~prefix:"$")
      |> List.fold
           ~init:([], String.Map.empty, String.Map.singleton "/" `dir)
           ~f:(fun (cwd, children, nodes) next ->
             match next with
             | [ chdir ] ->
               (match String.chop_prefix_exn chdir ~prefix:"$ cd " with
                | ".." -> List.tl_exn cwd, children, nodes
                | dir -> dir :: cwd, children, nodes)
             | "$ ls" :: rest ->
               let children, nodes =
                 List.fold rest ~init:(children, nodes) ~f:(fun (children, nodes) line ->
                   let name, kind =
                     match String.chop_prefix line ~prefix:"dir " with
                     | Some s -> s, `dir
                     | None ->
                       let size, name = String.split line ~on:' ' |> e2 in
                       let size = stoi size in
                       name, `leaf size
                   in
                   ( String.Map.add_multi
                       children
                       ~key:(path_to_string cwd)
                       ~data:(path_to_string (name :: cwd))
                   , String.Map.add_exn
                       nodes
                       ~key:(path_to_string (name :: cwd))
                       ~data:kind ))
               in
               cwd, children, nodes
             | _ -> rip ())
    in
    children, nodes
  ;;
end

module Inputs = struct
  let real_input () = Stdio.In_channel.read_lines (input_file Config.day) |> Parsed.parse
  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect
      {|
      (((/ (d/ c.dat/ b.txt/ a/)) (a/ (h.lsta/ ga/ fa/ ea/))
        (d/ (kd/ d.extd/ d.logd/ jd/)) (ea/ (iea/)))
       ((/ dir) (a/ dir) (b.txt/ (leaf 14848514)) (c.dat/ (leaf 8504156))
        (d.extd/ (leaf 5626152)) (d.logd/ (leaf 8033020)) (d/ dir) (ea/ dir)
        (fa/ (leaf 29116)) (ga/ (leaf 2557)) (h.lsta/ (leaf 62596))
        (iea/ (leaf 584)) (jd/ (leaf 4060174)) (kd/ (leaf 7214296)))) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

let rec get_size (children, nodes) ~node =
  match String.Map.find_exn nodes node with
  | `dir ->
    List.fold ~init:0 (String.Map.find_exn children node) ~f:(fun accum child ->
      accum + get_size (children, nodes) ~node:child)
  | `leaf size -> size
;;

let all_sizes (children, nodes) =
  String.Map.to_alist nodes
  |> List.filter_map ~f:(fun (node, kind) ->
       match kind with
       | `dir -> Some (get_size (children, nodes) ~node)
       | `leaf _ -> None)
;;

module Part1 = struct
  let f (children, nodes) =
    let all_sizes = all_sizes (children, nodes) in
    let result = List.filter all_sizes ~f:(fun s -> s <= 100_000) |> sumi in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 1" =
    f (Inputs.short_input ());
    [%expect {| result: 95437 |}];
    f (Inputs.real_input ());
    [%expect {| result: 1915606 |}]
  ;;
end

module Part2 = struct
  let have = 70_000_000
  let need = 30_000_000

  let f input =
    let all_sizes = all_sizes input in
    let used = get_size input ~node:"/" in
    let need_to_free = used - (have - need) in
    let result =
      List.filter all_sizes ~f:(fun s -> s >= need_to_free)
      |> List.sort ~compare:Int.compare
      |> List.hd_exn
    in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 2" =
    f (Inputs.short_input ());
    [%expect {| result: 24933642 |}];
    f (Inputs.real_input ());
    [%expect {| result: 5025657 |}]
  ;;
end

let with_parsed f input =
  let parsed = Parsed.parse input in
  f parsed
;;

let command =
  Commands.both ~day:Config.day ~part1:(with_parsed Part1.f) ~part2:(with_parsed Part2.f)
;;
