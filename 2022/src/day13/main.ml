open! Core
open Common_2022.Common

module Config = struct
  let day = 13

  let short_input =
    {|[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]|}
  ;;

  let test_inputs = [||]
end

module Parsed = struct
  type elt =
    [ `list of elt list
    | `int of int
    ]
  [@@deriving sexp, equal]

  let rec elt_compare left right =
    match (left : elt), (right : elt) with
    | `int l, `int r -> Int.compare l r
    | `int _, `list _ -> elt_compare (`list [ left ]) right
    | `list _, `int _ -> elt_compare left (`list [ right ])
    | `list l, `list r ->
      let zipped, remainder = List.zip_with_remainder l r in
      let result =
        List.find_map zipped ~f:(fun (l, r) ->
          let compare = elt_compare l r in
          Option.some_if (compare <> 0) compare)
      in
      (match result with
       | Some i -> i
       | None ->
         (match remainder with
          | None -> 0
          | Some (First _) -> 1
          | Some (Second _) -> -1))
  ;;

  let%expect_test "elt_compare" =
    printf
      !"%d\n%!"
      (elt_compare
         (`list [ `int 1; `int 1; `int 3; `int 1; `int 1 ])
         (`list [ `int 1; `int 1; `int 5; `int 1; `int 1 ]));
    [%expect {|
      -1 |}];
    let left, right =
      {|((list
         ((int 1)
          (list
           ((int 2)
            (list ((int 3) (list ((int 4) (list ((int 5) (int 6) (int 7)))))))))
          (int 8) (int 9)))
        (list
         ((int 1)
          (list
           ((int 2)
            (list ((int 3) (list ((int 4) (list ((int 5) (int 6) (int 0)))))))))
          (int 8) (int 9))))|}
      |> Sexp.of_string
      |> [%of_sexp: elt * elt]
    in
    printf "%d\n%!" (elt_compare left right);
    [%expect {|
      1 |}]
  ;;

  type t = (elt * elt) list [@@deriving sexp]

  let rec elt_of_string s =
    match Or_error.try_with (fun () -> stoi s) with
    | Ok i -> `int i
    | Error _ ->
      let stripped =
        String.chop_prefix_exn s ~prefix:"[" |> String.chop_suffix_exn ~suffix:"]"
      in
      if String.is_empty stripped
      then `list []
      else (
        let _, last, tokens =
          String.fold
            stripped
            ~init:(0, [], [])
            ~f:(fun (num_opens, fragments, accum) c ->
            if Char.equal c '['
            then num_opens + 1, c :: fragments, accum
            else if Char.equal c ']'
            then num_opens - 1, c :: fragments, accum
            else if Char.equal c ',' && num_opens = 0
            then num_opens, [], (List.rev fragments |> String.of_char_list) :: accum
            else num_opens, c :: fragments, accum)
        in
        let tokens =
          (List.rev last |> String.of_char_list) :: tokens
          |> List.rev
          |> List.map ~f:elt_of_string
        in
        `list tokens)
  ;;

  let parse input =
    coagulate input
    |> List.map ~f:(fun s ->
         let left, right = e2 s in
         elt_of_string left, elt_of_string right)
  ;;
end

module Inputs = struct
  let real_input () = Stdio.In_channel.read_lines (input_file Config.day) |> Parsed.parse
  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect
      {|
      (((list ((int 1) (int 1) (int 3) (int 1) (int 1)))
        (list ((int 1) (int 1) (int 5) (int 1) (int 1))))
       ((list ((list ((int 1))) (list ((int 2) (int 3) (int 4)))))
        (list ((list ((int 1))) (int 4))))
       ((list ((int 9))) (list ((list ((int 8) (int 7) (int 6))))))
       ((list ((list ((int 4) (int 4))) (int 4) (int 4)))
        (list ((list ((int 4) (int 4))) (int 4) (int 4) (int 4))))
       ((list ((int 7) (int 7) (int 7) (int 7))) (list ((int 7) (int 7) (int 7))))
       ((list ()) (list ((int 3))))
       ((list ((list ((list ()))))) (list ((list ()))))
       ((list
         ((int 1)
          (list
           ((int 2)
            (list ((int 3) (list ((int 4) (list ((int 5) (int 6) (int 7)))))))))
          (int 8) (int 9)))
        (list
         ((int 1)
          (list
           ((int 2)
            (list ((int 3) (list ((int 4) (list ((int 5) (int 6) (int 0)))))))))
          (int 8) (int 9))))) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

module Part1 = struct
  let f input =
    let result =
      List.filter_mapi input ~f:(fun i (left, right) ->
        Option.some_if (Parsed.elt_compare left right < 1) (i + 1))
      |> List.reduce_exn ~f:( + )
    in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 1" =
    f (Inputs.short_input ());
    [%expect {| result: 13 |}];
    f (Inputs.real_input ());
    [%expect {| result: 5350 |}]
  ;;
end

module Part2 = struct
  let f input =
    let input = List.concat_map input ~f:(fun (left, right) -> [ left; right ]) in
    let packet1 = `list [ `list [ `int 2 ] ] in
    let packet2 = `list [ `list [ `int 6 ] ] in
    let sorted = List.sort (packet1 :: packet2 :: input) ~compare:Parsed.elt_compare in
    let result =
      let packet_idx p =
        1 + (List.findi_exn sorted ~f:(fun _ -> Parsed.equal_elt p) |> fst)
      in
      packet_idx packet1 * packet_idx packet2
    in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 2" =
    f (Inputs.short_input ());
    [%expect {| result: 140 |}];
    f (Inputs.real_input ());
    [%expect {| result: 19570 |}]
  ;;
end

let with_parsed f input =
  let parsed = Parsed.parse input in
  f parsed
;;

let command =
  Commands.both ~day:Config.day ~part1:(with_parsed Part1.f) ~part2:(with_parsed Part2.f)
;;
