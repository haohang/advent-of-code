open! Core
open Common_2022.Common

module Config = struct
  let day = 05

  let short_input =
    {|    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2|}
  ;;

  let test_inputs = [||]
end

module Parsed = struct
  type t = char list array * (int * int * int) list [@@deriving sexp]

  let parse input =
    let stacks, instructions = coagulate input |> e2 in
    let stacks =
      let stacks, labels = List.split_n stacks (List.length stacks - 1) in
      let labels = e1 labels in
      let num_stacks = List.length (regex_num labels) in
      (* Hardcoded based on inputs *)
      assert (num_stacks <= 9);
      Array.init num_stacks ~f:(fun i ->
        let idx = String.index_exn labels (itoc (i + 1)) in
        List.map stacks ~f:(fun s -> String.get s idx)
        |> List.filter ~f:(fun c -> not (Char.is_whitespace c)))
    in
    let instructions = List.map instructions ~f:(fun l -> regex_num l |> e3) in
    stacks, instructions
  ;;
end

module Inputs = struct
  let real_input () = Stdio.In_channel.read_lines (input_file Config.day) |> Parsed.parse
  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect {| (((N Z) (D C M) (P)) ((1 2 1) (3 1 3) (2 2 1) (1 1 2))) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

module Part1 = struct
  let f (stacks, instructions) =
    let result =
      List.iter instructions ~f:(fun (num, from, to_) ->
        Fn.apply_n_times
          ~n:num
          (fun () ->
            match stacks.(from - 1) with
            | hd :: tl ->
              stacks.(to_ - 1) <- hd :: stacks.(to_ - 1);
              stacks.(from - 1) <- tl
            | _ -> rip ())
          ());
      Array.map stacks ~f:List.hd
      |> Array.to_list
      |> List.filter_opt
      |> String.of_char_list
    in
    print_endline [%string "result: %{result#String}"]
  ;;

  let%expect_test "part 1" =
    f (Inputs.short_input ());
    [%expect {| result: CMZ |}];
    f (Inputs.real_input ());
    [%expect {| result: ZRLJGSCTR |}]
  ;;
end

module Part2 = struct
  let f (stacks, instructions) =
    let result =
      List.iter instructions ~f:(fun (num, from, to_) ->
        let hd, tl = List.split_n stacks.(from - 1) num in
        stacks.(to_ - 1) <- hd @ stacks.(to_ - 1);
        stacks.(from - 1) <- tl);
      Array.map stacks ~f:List.hd
      |> Array.to_list
      |> List.filter_opt
      |> String.of_char_list
    in
    print_endline [%string "result: %{result#String}"]
  ;;

  let%expect_test "part 2" =
    f (Inputs.short_input ());
    [%expect {| result: MCD |}];
    f (Inputs.real_input ());
    [%expect {| result: PRTTGRFPB |}]
  ;;
end

let with_parsed f input =
  let parsed = Parsed.parse input in
  f parsed
;;

let command =
  Commands.both ~day:Config.day ~part1:(with_parsed Part1.f) ~part2:(with_parsed Part2.f)
;;
