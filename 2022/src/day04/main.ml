open! Core
open Common_2022.Common

module Config = struct
  let day = 04

  let short_input = {|2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8|}

  let test_inputs = [||]
end

module Parsed = struct
  type t = (int * int * int * int) list [@@deriving sexp]

  let parse input = List.map input ~f:(fun s -> regex_num s |> e4)
end

module Inputs = struct
  let real_input () = Stdio.In_channel.read_lines (input_file Config.day) |> Parsed.parse
  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect {| ((2 4 6 8) (2 3 4 5) (5 7 7 9) (2 8 3 7) (6 6 4 6) (2 6 4 8)) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

module Part1 = struct
  let f input =
    let result =
      List.count input ~f:(fun (a, b, c, d) -> (a <= c && b >= d) || (c <= a && d >= b))
    in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 1" =
    f (Inputs.short_input ());
    [%expect {| result: 2 |}];
    f (Inputs.real_input ());
    [%expect {| result: 515 |}]
  ;;
end

module Part2 = struct
  let f input =
    let result = List.count input ~f:(fun (a, b, c, d) -> not (b < c || d < a)) in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 2" =
    f (Inputs.short_input ());
    [%expect {| result: 4 |}];
    f (Inputs.real_input ());
    [%expect {| result: 883 |}]
  ;;
end

let with_parsed f input =
  let parsed = Parsed.parse input in
  f parsed
;;

let command =
  Commands.both ~day:Config.day ~part1:(with_parsed Part1.f) ~part2:(with_parsed Part2.f)
;;
