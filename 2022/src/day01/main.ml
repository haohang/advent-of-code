open! Core
open Common_2022.Common
include Utils

module Config = struct
  let day = 1

  let short_input = {|1000
2000
3000

4000

5000
6000

7000
8000
9000

10000|}

  let test_inputs = [||]
end

module Parsed = struct
  type t = int list [@@deriving sexp]

  let parse input =
    coagulate input
    |> List.map ~f:(fun lst -> List.map lst ~f:stoi |> sumi)
    |> List.sort ~compare:(fun a b -> Int.compare b a)
  ;;
end

module Inputs = struct
  let real_input () = Stdio.In_channel.read_lines (input_file Config.day) |> Parsed.parse
  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect {| (24000 11000 10000 6000 4000) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

module Part1 = struct
  let f input =
    let result = List.max_elt input ~compare:Int.compare |> Option.value_exn in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 1" =
    f (Inputs.short_input ());
    [%expect {| result: 24000 |}];
    f (Inputs.real_input ());
    [%expect {| result: 70698 |}]
  ;;
end

module Part2 = struct
  let f input =
    let result =
      match input with
      | a :: b :: c :: _ -> a + b + c
      | _ -> rip ()
    in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 2" =
    f (Inputs.short_input ());
    [%expect {| result: 45000 |}];
    f (Inputs.real_input ());
    [%expect {| result: 206643 |}]
  ;;
end

let with_parsed f input =
  let parsed = Parsed.parse input in
  f parsed
;;

let command =
  Commands.both ~day:Config.day ~part1:(with_parsed Part1.f) ~part2:(with_parsed Part2.f)
;;
