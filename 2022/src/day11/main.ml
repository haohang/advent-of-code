open! Core
open Common_2022.Common

module Config = struct
  let day = 11

  let short_input =
    {|Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1|}
  ;;

  let test_inputs = [||]
end

module Parsed = struct
  type monkey =
    { mutable items : int list
    ; operation : int -> int
    ; test_divisible_by : int
    ; if_true : int
    ; if_false : int
    ; mutable throws : int
    }
  [@@deriving sexp]

  type t = monkey array [@@deriving sexp]

  let parse input =
    let monkeys = coagulate input in
    List.map monkeys ~f:(fun line ->
      let _monkey_number, starting, op, test, if_true, if_false = e6 line in
      let items = regex_num starting in
      let operation =
        match Or_error.try_with (fun () -> regex_num op) with
        | Error _ -> fun x -> x * x
        | Ok [ i ] ->
          let op = if String.contains op '*' then ( * ) else ( + ) in
          fun x -> op i x
        | _ -> rip ()
      in
      let test_divisible_by = regex_num test |> e1 in
      let if_true = regex_num if_true |> e1 in
      let if_false = regex_num if_false |> e1 in
      { items; operation; test_divisible_by; if_true; if_false; throws = 0 })
    |> Array.of_list
  ;;
end

module Inputs = struct
  let real_input () = Stdio.In_channel.read_lines (input_file Config.day) |> Parsed.parse
  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect
      {|
      (((items (79 98)) (operation <fun>) (test_divisible_by 23) (if_true 2)
        (if_false 3) (throws 0))
       ((items (54 65 75 74)) (operation <fun>) (test_divisible_by 19) (if_true 2)
        (if_false 0) (throws 0))
       ((items (79 60 97)) (operation <fun>) (test_divisible_by 13) (if_true 1)
        (if_false 3) (throws 0))
       ((items (74)) (operation <fun>) (test_divisible_by 17) (if_true 0)
        (if_false 1) (throws 0))) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

let run_one_round monkeys ~modulate_stress =
  Array.iter monkeys ~f:(fun monkey ->
    let items = monkey.Parsed.items in
    monkey.items <- [];
    List.iter items ~f:(fun i ->
      monkey.throws <- monkey.throws + 1;
      let worry = monkey.operation i |> modulate_stress in
      let test_result = worry % monkey.test_divisible_by = 0 in
      let target = if test_result then monkey.if_true else monkey.if_false in
      monkeys.(target).items <- monkeys.(target).items @ [ worry ]))
;;

let run_n_rounds monkeys ~n ~modulate_stress =
  Fn.apply_n_times ~n (fun () -> run_one_round monkeys ~modulate_stress) ();
  let throws =
    Array.map monkeys ~f:(fun { Parsed.throws; _ } -> throws)
    |> Array.to_list
    |> List.sort ~compare:(fun a b -> Int.compare b a)
  in
  List.reduce_exn (List.take throws 2) ~f:( * )
;;

module Part1 = struct
  let f input =
    let result = run_n_rounds input ~n:20 ~modulate_stress:(fun x -> x / 3) in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 1" =
    f (Inputs.short_input ());
    [%expect {| result: 10605 |}];
    f (Inputs.real_input ());
    [%expect {| result: 66802 |}]
  ;;
end

module Part2 = struct
  let f input =
    let modulus =
      Array.fold input ~init:1 ~f:(fun i monkey -> i * monkey.Parsed.test_divisible_by)
    in
    let result = run_n_rounds input ~n:10_000 ~modulate_stress:(fun x -> x % modulus) in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 2" =
    f (Inputs.short_input ());
    [%expect {| result: 2713310158 |}];
    f (Inputs.real_input ());
    [%expect {| result: 21800916620 |}]
  ;;
end

let with_parsed f input =
  let parsed = Parsed.parse input in
  f parsed
;;

let command =
  Commands.both ~day:Config.day ~part1:(with_parsed Part1.f) ~part2:(with_parsed Part2.f)
;;
