open Core

let command =
  Command.group
    ~summary:"Advent of Code TEMPLATE"
    [ Day01_2022.Main.command
    ; Day02_2022.Main.command
    ; Day03_2022.Main.command
    ; Day04_2022.Main.command
    ; Day05_2022.Main.command
    ; Day06_2022.Main.command
    ; Day07_2022.Main.command
    ; Day08_2022.Main.command
    ; Day09_2022.Main.command
    ; Day10_2022.Main.command
    ; Day11_2022.Main.command
    ; Day12_2022.Main.command
    ; Day13_2022.Main.command
    ; Day14_2022.Main.command
    ; Day15_2022.Main.command
    ; Day16_2022.Main.command
    ; Day17_2022.Main.command
    ; Day18_2022.Main.command
    ; Day19_2022.Main.command
    ; Day20_2022.Main.command
    ; Day21_2022.Main.command
    ; Day22_2022.Main.command
    ; Day23_2022.Main.command
    ; Day24_2022.Main.command
    ; Day25_2022.Main.command
    ]
;;
