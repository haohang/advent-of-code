open! Core
open Common_2022.Common
open Graph

module Config = struct
  let day = 14

  let short_input = {|498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9|}

  let test_inputs = [||]
end

module Parsed = struct
  type t = Coord.Set.t [@@deriving sexp]

  let parse input =
    let coords =
      List.map input ~f:(fun line ->
        regex_num line
        |> List.chunks_of ~length:2
        |> List.map ~f:(fun l ->
             let col, row = e2 l in
             { Coord.col; row }))
    in
    List.fold coords ~init:Coord.Set.empty ~f:(fun accum path ->
      match path with
      | [] | [ _ ] -> rip ()
      | first :: rest ->
        List.fold rest ~init:(first, accum) ~f:(fun (prev, accum) next ->
          next, Coord.range prev next |> Coord.Set.of_list |> Coord.Set.union accum)
        |> snd)
  ;;
end

module Inputs = struct
  let real_input () = Stdio.In_channel.read_lines (input_file Config.day) |> Parsed.parse
  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect
      {|
      (((row 4) (col 498)) ((row 4) (col 502)) ((row 4) (col 503))
       ((row 5) (col 498)) ((row 5) (col 502)) ((row 6) (col 496))
       ((row 6) (col 497)) ((row 6) (col 498)) ((row 6) (col 502))
       ((row 7) (col 502)) ((row 8) (col 502)) ((row 9) (col 494))
       ((row 9) (col 495)) ((row 9) (col 496)) ((row 9) (col 497))
       ((row 9) (col 498)) ((row 9) (col 499)) ((row 9) (col 500))
       ((row 9) (col 501)) ((row 9) (col 502))) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

let drop_one_sand path sand ~is_infinite_abyss =
  let drop_start = { Coord.row = 0; col = 500 } in
  let max_row =
    Coord.Set.to_list path |> List.map ~f:Coord.row |> List.reduce_exn ~f:Int.max
  in
  let can_move coord =
    (not (Coord.Set.mem path coord))
    && (not (Coord.Set.mem sand coord))
    && (is_infinite_abyss || coord.row < max_row + 2)
  in
  let move_down curr =
    let down = Coord.(move curr ~vec:south ~n:1) in
    let down_left = Coord.(move down ~vec:west ~n:1) in
    let down_right = Coord.(move down ~vec:east ~n:1) in
    List.find [ down; down_left; down_right ] ~f:can_move
  in
  let rec move_until_stopped curr =
    match move_down curr with
    | None -> if Coord.equal curr drop_start then None else Some curr
    | Some next ->
      if is_infinite_abyss && next.row > max_row then None else move_until_stopped next
  in
  move_until_stopped { Coord.row = 0; col = 500 }
;;

let drop_sand path ~is_infinite_abyss =
  let rec drop sand =
    match drop_one_sand path sand ~is_infinite_abyss with
    | None -> Coord.Set.length sand
    | Some next -> drop (Coord.Set.add sand next)
  in
  drop Coord.Set.empty
;;

module Part1 = struct
  let f input =
    let result = drop_sand input ~is_infinite_abyss:true in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 1" =
    f (Inputs.short_input ());
    [%expect {| result: 24 |}];
    f (Inputs.real_input ());
    [%expect {| result: 618 |}]
  ;;
end

module Part2 = struct
  let f input =
    let result = 1 + drop_sand input ~is_infinite_abyss:false in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 2" =
    f (Inputs.short_input ());
    [%expect {| result: 93 |}];
    f (Inputs.real_input ());
    [%expect {| result: 26358 |}]
  ;;
end

let with_parsed f input =
  let parsed = Parsed.parse input in
  f parsed
;;

let command =
  Commands.both ~day:Config.day ~part1:(with_parsed Part1.f) ~part2:(with_parsed Part2.f)
;;
