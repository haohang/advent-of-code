open! Core
open Common_2022.Common
open Graph

module Config = struct
  let day = 09

  let short_input = {|R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2|}

  let test_inputs = [||]
end

module Parsed = struct
  type t = (Direction.t * int) list [@@deriving sexp]

  let parse input =
    List.map input ~f:(fun l ->
      let dir, n = String.split l ~on:' ' |> e2 in
      let dir = Direction.of_string dir in
      let n = stoi n in
      dir, n)
  ;;
end

module Inputs = struct
  let real_input () = Stdio.In_channel.read_lines (input_file Config.day) |> Parsed.parse
  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect
      {| ((Right 4) (Up 4) (Left 3) (Down 1) (Right 4) (Down 1) (Left 5) (Right 2)) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

let clamp { Coord.col; row } =
  { Coord.row = (if row >= 1 then 1 else if row <= -1 then -1 else 0)
  ; col = (if col >= 1 then 1 else if col <= -1 then -1 else 0)
  }
;;

let apply_one_move knots dir =
  List.fold
    (List.init (List.length knots) ~f:Fn.id)
    ~init:[]
    ~f:(fun accum i ->
      if i = 0
      then [ Coord.add (List.hd_exn knots) (Direction.deltas dir) ]
      else (
        let prev = List.hd_exn accum in
        let curr = List.nth_exn knots i in
        let ({ Coord.row; col } as delta) = Coord.sub prev curr in
        if Int.abs row <= 1 && Int.abs col <= 1
        then curr :: accum
        else (
          let diff = clamp delta in
          Coord.add diff curr :: accum)))
  |> List.rev
;;

let simulate ~length input =
  List.fold
    input
    ~init:(List.init length ~f:(const Coord.origin), Coord.Set.singleton Coord.origin)
    ~f:(fun (knots, visited) (dir, n) ->
      Fn.apply_n_times
        ~n
        (fun (knots, visited) ->
          let new_knots = apply_one_move knots dir in
          let visited = Coord.Set.add visited (List.last_exn new_knots) in
          new_knots, visited)
        (knots, visited))
  |> snd
;;

module Part1 = struct
  let f input =
    let result = simulate ~length:2 input |> Coord.Set.length in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 1" =
    f (Inputs.short_input ());
    [%expect {|
      result: 13 |}];
    f (Inputs.real_input ());
    [%expect {|
      result: 5902 |}]
  ;;
end

module Part2 = struct
  let f input =
    let result = simulate ~length:10 input |> Coord.Set.length in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 2" =
    f (Inputs.short_input ());
    [%expect {| result: 1 |}];
    f (Inputs.real_input ());
    [%expect {| result: 2445 |}]
  ;;
end

let with_parsed f input =
  let parsed = Parsed.parse input in
  f parsed
;;

let command =
  Commands.both ~day:Config.day ~part1:(with_parsed Part1.f) ~part2:(with_parsed Part2.f)
;;
