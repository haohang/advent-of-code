open! Core
open Common_2022.Common
open Graph

module Config = struct
  let day = 12

  let short_input = {|Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi|}

  let test_inputs = [||]
end

module Parsed = struct
  type t = char Grid.t [@@deriving sexp]

  let parse input = Grid.of_strings input
end

module Inputs = struct
  let real_input () = Stdio.In_channel.read_lines (input_file Config.day) |> Parsed.parse
  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect
      {|
      ((height 5) (width 8)
       (grid
        ((S a b q p o n m) (a b c r y x x l) (a c c s z E x k) (a c c t u v w j)
         (a b d e f g h i)))) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

let elevation = function
  | 'S' -> actoi 'a'
  | 'E' -> actoi 'z'
  | c -> actoi c
;;

let bfs input ~start =
  let queue = Queue.create () in
  let visited = Coord.Hash_set.create () in
  Queue.enqueue queue (start, 0);
  let rec run () =
    let%bind.Option next, len = Queue.dequeue queue in
    if Char.equal (Grid.get_exn input next) 'E'
    then Some len
    else (
      if not (Hash_set.mem visited next)
      then (
        Hash_set.add visited next;
        let next_val = Grid.get_exn input next in
        let neighbors =
          Grid.neighbors input next
          |> List.filter ~f:(fun c ->
               elevation (Grid.get_exn input c) - elevation next_val <= 1)
        in
        List.iter neighbors ~f:(fun c -> Queue.enqueue queue (c, len + 1)));
      run ())
  in
  run ()
;;

module Part1 = struct
  let f input =
    let start =
      Grid.coords input
      |> List.find_exn ~f:(fun c -> Char.equal (Grid.get_exn input c) 'S')
    in
    let result = bfs input ~start |> Option.value_exn in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 1" =
    f (Inputs.short_input ());
    [%expect {| result: 31 |}];
    f (Inputs.real_input ());
    [%expect {| result: 468 |}]
  ;;
end

module Part2 = struct
  let f input =
    let starts =
      Grid.coords input
      |> List.filter ~f:(fun c ->
           Char.equal (Grid.get_exn input c) 'a' || Char.equal (Grid.get_exn input c) 'S')
    in
    let result =
      List.filter_map starts ~f:(fun start -> bfs input ~start)
      |> List.min_elt ~compare:Int.compare
      |> Option.value_exn
    in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 2" =
    f (Inputs.short_input ());
    [%expect {| result: 29 |}];
    f (Inputs.real_input ());
    [%expect {| result: 459 |}]
  ;;
end

let with_parsed f input =
  let parsed = Parsed.parse input in
  f parsed
;;

let command =
  Commands.both ~day:Config.day ~part1:(with_parsed Part1.f) ~part2:(with_parsed Part2.f)
;;
