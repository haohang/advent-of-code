open! Core
open Common_2022.Common

module Config = struct
  let day = 06
  let short_input = {|mjqjpqmgbljsphdztnvjfqwrcgsmlb|}
  let test_inputs = [||]
end

module Parsed = struct
  type t = string [@@deriving sexp]

  let parse = e1
end

module Inputs = struct
  let real_input () = Stdio.In_channel.read_lines (input_file Config.day) |> Parsed.parse
  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect {| mjqjpqmgbljsphdztnvjfqwrcgsmlb |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

let find_marker s ~len =
  let idx, _ =
    List.init (String.length s - len + 1) ~f:(fun i -> slice s i (i + len))
    |> List.findi_exn ~f:(fun _ s ->
         Set.length (String.to_list s |> Char.Set.of_list) = len)
  in
  idx + len
;;

module Part1 = struct
  let f input =
    let result = find_marker input ~len:4 in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 1" =
    f (Inputs.short_input ());
    [%expect {| result: 7 |}];
    f (Inputs.real_input ());
    [%expect {| result: 1034 |}]
  ;;
end

module Part2 = struct
  let f input =
    let result = find_marker input ~len:14 in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 2" =
    f (Inputs.short_input ());
    [%expect {| result: 19 |}];
    f (Inputs.real_input ());
    [%expect {| result: 2472 |}]
  ;;
end

let with_parsed f input =
  let parsed = Parsed.parse input in
  f parsed
;;

let command =
  Commands.both ~day:Config.day ~part1:(with_parsed Part1.f) ~part2:(with_parsed Part2.f)
;;
