open! Core
open Common_2022.Common

module Config = struct
  let day = 03

  let short_input =
    {|vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw|}
  ;;

  let test_inputs = [||]
end

module Parsed = struct
  type t = string list [@@deriving sexp]

  let parse = Fn.id
end

module Inputs = struct
  let real_input () = Stdio.In_channel.read_lines (input_file Config.day) |> Parsed.parse
  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect
      {|
      (vJrwpWtwJgWrhcsFMMfFFhFp jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL PmmdzqPrVvPwwTWBwg
       wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn ttgJtRGJQctTZtZT CrZsJsPPZsGzwwsLwLmpwMDw) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

let score c =
  let v = actoi c in
  let a = actoi 'a' in
  let b = actoi 'A' in
  if Char.is_lowercase c then v - a + 1 else v - b + 27
;;

module Part1 = struct
  let f input =
    let result =
      List.map input ~f:(fun s ->
        let len = String.length s in
        let first_half = slice s 0 (len / 2) in
        let second_half = slice s (len / 2) Int.max_value in
        let overlapping_char =
          String.find first_half ~f:(fun char -> String.contains second_half char)
          |> Option.value_exn
        in
        score overlapping_char)
      |> sumi
    in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 1" =
    f (Inputs.short_input ());
    [%expect {| result: 157 |}];
    f (Inputs.real_input ());
    [%expect {| result: 8039 |}]
  ;;
end

module Part2 = struct
  let f input =
    let result =
      let inputs = List.chunks_of input ~length:3 |> List.map ~f:e3 in
      List.map inputs ~f:(fun (a, b, c) ->
        let overlapping_char =
          String.find a ~f:(fun char -> String.contains b char && String.contains c char)
          |> Option.value_exn
        in
        score overlapping_char)
      |> sumi
    in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 2" =
    f (Inputs.short_input ());
    [%expect {| result: 70 |}];
    f (Inputs.real_input ());
    [%expect {| result: 2510 |}]
  ;;
end

let with_parsed f input =
  let parsed = Parsed.parse input in
  f parsed
;;

let command =
  Commands.both ~day:Config.day ~part1:(with_parsed Part1.f) ~part2:(with_parsed Part2.f)
;;
