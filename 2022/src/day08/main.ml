open! Core
open Common_2022.Common
open Graph

module Config = struct
  let day = 08

  let short_input = {|30373
25512
65332
33549
35390|}

  let test_inputs = [||]
end

module Parsed = struct
  type t = int Grid.t [@@deriving sexp]

  let parse = Grid.of_digit_strings
end

module Inputs = struct
  let real_input () = Stdio.In_channel.read_lines (input_file Config.day) |> Parsed.parse
  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect
      {|
      ((height 5) (width 5)
       (grid ((3 0 3 7 3) (2 5 5 1 2) (6 5 3 3 2) (3 3 5 4 9) (3 5 3 9 0)))) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

module Part1 = struct
  let f input =
    let result =
      Grid.foldi input ~init:0 ~f:(fun coord accum height ->
        let is_visible =
          if Grid.on_edge input coord
          then true
          else (
            let is_visible_in_dir vec =
              List.for_all (Grid.move_until_edge input coord ~vec) ~f:(fun (_, h) ->
                h < height)
            in
            List.exists Coord.cardinal_dirs ~f:is_visible_in_dir)
        in
        if is_visible then accum + 1 else accum)
    in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 1" =
    f (Inputs.short_input ());
    [%expect {| result: 21 |}];
    f (Inputs.real_input ());
    [%expect {| result: 1719 |}]
  ;;
end

module Part2 = struct
  let f input =
    let result =
      Grid.foldi input ~init:0 ~f:(fun coord accum height ->
        let score =
          if Grid.on_edge input coord
          then 0
          else (
            let score_in_dir vec =
              Grid.move_until_edge input coord ~vec
              |> List.fold_until
                   ~init:0
                   ~f:(fun total (_, h) ->
                     if h >= height then Stop (total + 1) else Continue (total + 1))
                   ~finish:Fn.id
            in
            List.map Coord.cardinal_dirs ~f:score_in_dir |> List.reduce_exn ~f:( * ))
        in
        if score > accum then score else accum)
    in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 2" =
    f (Inputs.short_input ());
    [%expect {| result: 8 |}];
    f (Inputs.real_input ());
    [%expect {| result: 590824 |}]
  ;;
end

let with_parsed f input =
  let parsed = Parsed.parse input in
  f parsed
;;

let command =
  Commands.both ~day:Config.day ~part1:(with_parsed Part1.f) ~part2:(with_parsed Part2.f)
;;
