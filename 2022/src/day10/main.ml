open! Core
open Common_2022.Common

module Config = struct
  let day = 10

  let short_input =
    {|addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop|}
  ;;

  let test_inputs = [| {|noop
addx 3
addx -5|} |]
end

module Parsed = struct
  type instruction =
    [ `noop
    | `addx of int
    ]
  [@@deriving sexp]

  type t = instruction list [@@deriving sexp]

  let parse input =
    List.map input ~f:(function
      | "noop" -> `noop
      | s ->
        (match String.split s ~on:' ' with
         | [ "addx"; i ] -> `addx (stoi i)
         | _ -> rip ()))
  ;;
end

module Inputs = struct
  let real_input () = Stdio.In_channel.read_lines (input_file Config.day) |> Parsed.parse
  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect
      {|
      ((addx 15) (addx -11) (addx 6) (addx -3) (addx 5) (addx -1) (addx -8)
       (addx 13) (addx 4) noop (addx -1) (addx 5) (addx -1) (addx 5) (addx -1)
       (addx 5) (addx -1) (addx 5) (addx -1) (addx -35) (addx 1) (addx 24)
       (addx -19) (addx 1) (addx 16) (addx -11) noop noop (addx 21) (addx -15) noop
       noop (addx -3) (addx 9) (addx 1) (addx -3) (addx 8) (addx 1) (addx 5) noop
       noop noop noop noop (addx -36) noop (addx 1) (addx 7) noop noop noop
       (addx 2) (addx 6) noop noop noop noop noop (addx 1) noop noop (addx 7)
       (addx 1) noop (addx -13) (addx 13) (addx 7) noop (addx 1) (addx -33) noop
       noop noop (addx 2) noop noop noop (addx 8) noop (addx -1) (addx 2) (addx 1)
       noop (addx 17) (addx -9) (addx 1) (addx 1) (addx -3) (addx 11) noop noop
       (addx 1) noop (addx 1) noop noop (addx -13) (addx -19) (addx 1) (addx 3)
       (addx 26) (addx -30) (addx 12) (addx -1) (addx 3) (addx 1) noop noop noop
       (addx -9) (addx 18) (addx 1) (addx 2) noop noop (addx 9) noop noop noop
       (addx -1) (addx 2) (addx -37) (addx 1) (addx 3) noop (addx 15) (addx -21)
       (addx 22) (addx -6) (addx 1) noop (addx 2) (addx 1) noop (addx -10) noop
       noop (addx 20) (addx 1) (addx 2) (addx 2) (addx -6) (addx -11) noop noop
       noop) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| ((noop (addx 3) (addx -5))) |}]
  ;;
end

let simulate instructions =
  List.fold instructions ~init:(0, 1, []) ~f:(fun (cycle, register, history) next ->
    match next with
    | `noop -> cycle + 1, register, register :: history
    | `addx i -> cycle + 2, register + i, register :: register :: history)
  |> trd3
  |> List.rev
;;

module Part1 = struct
  let f input =
    let result =
      let sim = simulate input in
      List.map [ 20; 60; 100; 140; 180; 220 ] ~f:(fun i -> List.nth_exn sim (i - 1) * i)
      |> List.reduce_exn ~f:( + )
    in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 1" =
    f (Inputs.short_input ());
    [%expect {| result: 13140 |}];
    f (Inputs.real_input ());
    [%expect {| result: 14520 |}]
  ;;
end

module Part2 = struct
  let f input =
    let result =
      simulate input
      |> List.mapi ~f:(fun i value ->
           if Int.abs (value - (i % 40)) <= 1 then '#' else '.')
      |> List.chunks_of ~length:40
      |> Graph.Grid.of_lists
    in
    print_endline (Graph.Grid.to_string result ~to_char:Fn.id)
  ;;

  let%expect_test "part 2" =
    f (Inputs.short_input ());
    [%expect
      {|
      ##..##..##..##..##..##..##..##..##..##..
      ###...###...###...###...###...###...###.
      ####....####....####....####....####....
      #####.....#####.....#####.....#####.....
      ######......######......######......####
      #######.......#######.......#######..... |}];
    f (Inputs.real_input ());
    [%expect
      {|
      ###..####.###...##..####.####...##.###..
      #..#....#.#..#.#..#....#.#.......#.#..#.
      #..#...#..###..#......#..###.....#.###..
      ###...#...#..#.#.##..#...#.......#.#..#.
      #....#....#..#.#..#.#....#....#..#.#..#.
      #....####.###...###.####.####..##..###.. |}]
  ;;
end

let with_parsed f input =
  let parsed = Parsed.parse input in
  f parsed
;;

let command =
  Commands.both ~day:Config.day ~part1:(with_parsed Part1.f) ~part2:(with_parsed Part2.f)
;;
