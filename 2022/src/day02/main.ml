open! Core
open Common_2022.Common

module Config = struct
  let day = 02

  let short_input = {|A Y
B X
C Z|}

  let test_inputs = [||]
end

module Parsed = struct
  type t = (string * string) list [@@deriving sexp]

  let parse input = List.map input ~f:(fun l -> String.split ~on:' ' l |> e2)
end

module Inputs = struct
  let real_input () = Stdio.In_channel.read_lines (input_file Config.day) |> Parsed.parse
  let short_input () = Config.short_input |> String.split ~on:'\n' |> Parsed.parse

  let test_inputs =
    Array.map Config.test_inputs ~f:(fun one -> String.split ~on:'\n' one |> Parsed.parse)
  ;;

  let%expect_test "short_input" =
    printf !"%{sexp: Parsed.t}\n%!" (short_input ());
    [%expect {| ((A Y) (B X) (C Z)) |}]
  ;;

  let%expect_test "test_input" =
    printf !"%{sexp: Parsed.t array}\n%!" test_inputs;
    [%expect {| () |}]
  ;;
end

let parse_op = function
  | "A" -> `rock
  | "B" -> `paper
  | "C" -> `scissors
  | _ -> rip ()
;;

let played_score = function
  | `rock -> 1
  | `paper -> 2
  | `scissors -> 3
;;

module Part1 = struct
  let f input =
    let result =
      List.fold input ~init:0 ~f:(fun accum (a, b) ->
        let op = parse_op a in
        let you =
          match b with
          | "X" -> `rock
          | "Y" -> `paper
          | "Z" -> `scissors
          | _ -> rip ()
        in
        let your_score = played_score you in
        let match_score =
          match op, you with
          | `rock, `rock | `paper, `paper | `scissors, `scissors -> 3
          | `rock, `paper | `paper, `scissors | `scissors, `rock -> 6
          | `rock, `scissors | `paper, `rock | `scissors, `paper -> 0
          | _ -> rip ()
        in
        accum + match_score + your_score)
    in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 1" =
    f (Inputs.short_input ());
    [%expect {| result: 15 |}];
    f (Inputs.real_input ());
    [%expect {| result: 17189 |}]
  ;;
end

module Part2 = struct
  let f input =
    let result =
      List.fold input ~init:0 ~f:(fun accum (a, b) ->
        let op = parse_op a in
        let you =
          match b with
          | "X" -> `lose
          | "Y" -> `tie
          | "Z" -> `win
          | _ -> rip ()
        in
        let match_score =
          match you with
          | `lose -> 0
          | `tie -> 3
          | `win -> 6
          | _ -> rip ()
        in
        let your_score =
          match op, you with
          | `rock, `tie | `paper, `lose | `scissors, `win -> 1
          | `rock, `win | `paper, `tie | `scissors, `lose -> 2
          | `rock, `lose | `paper, `win | `scissors, `tie -> 3
          | _ -> rip ()
        in
        accum + match_score + your_score)
    in
    print_endline [%string "result: %{result#Int}"]
  ;;

  let%expect_test "part 2" =
    f (Inputs.short_input ());
    [%expect {| result: 12 |}];
    f (Inputs.real_input ());
    [%expect {| result: 13490 |}]
  ;;
end

let with_parsed f input =
  let parsed = Parsed.parse input in
  f parsed
;;

let command =
  Commands.both ~day:Config.day ~part1:(with_parsed Part1.f) ~part2:(with_parsed Part2.f)
;;
