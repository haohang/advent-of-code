open Core

let command =
  Command.group
    ~summary:"Advent of Code TEMPLATE"
    [ Day01_YEAR.Main.command
    ; Day02_YEAR.Main.command
    ; Day03_YEAR.Main.command
    ; Day04_YEAR.Main.command
    ; Day05_YEAR.Main.command
    ; Day06_YEAR.Main.command
    ; Day07_YEAR.Main.command
    ; Day08_YEAR.Main.command
    ; Day09_YEAR.Main.command
    ; Day10_YEAR.Main.command
    ; Day11_YEAR.Main.command
    ; Day12_YEAR.Main.command
    ; Day13_YEAR.Main.command
    ; Day14_YEAR.Main.command
    ; Day15_YEAR.Main.command
    ; Day16_YEAR.Main.command
    ; Day17_YEAR.Main.command
    ; Day18_YEAR.Main.command
    ; Day19_YEAR.Main.command
    ; Day20_YEAR.Main.command
    ; Day21_YEAR.Main.command
    ; Day22_YEAR.Main.command
    ; Day23_YEAR.Main.command
    ; Day24_YEAR.Main.command
    ; Day25_YEAR.Main.command
    ]
;;
