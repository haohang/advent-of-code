open! Core
open! Utils

let play_game input ~num_iters =
  let already_counted = List.length input - 1 in
  let last_number = List.last_exn input in
  let mem =
    Int.Map.of_alist_exn
      (List.mapi (List.take input already_counted) ~f:(fun i elt -> elt, i))
  in
  let rec aux mem prev idx_of_prev said_nums =
    if idx_of_prev = num_iters - 1
    then prev, said_nums
    else (
      let next =
        Option.value_map ~default:0 (Int.Map.find mem prev) ~f:(fun prev_idx_of_prev ->
            idx_of_prev - prev_idx_of_prev)
      in
      let mem = Int.Map.set mem ~key:prev ~data:idx_of_prev in
      aux mem next (idx_of_prev + 1) (next :: said_nums))
  in
  let result, _said_nums =
    aux mem last_number already_counted (List.rev input |> List.tl_exn)
  in
  result
;;

module Part1 = struct
  let f input =
    let nums =
      exactly_one_exn input |> String.split ~on:',' |> List.map ~f:Int.of_string
    in
    let result = play_game nums ~num_iters:2_020 in
    Core.print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let f input =
    let nums =
      exactly_one_exn input |> String.split ~on:',' |> List.map ~f:Int.of_string
    in
    let result = play_game nums ~num_iters:30_000_000 in
    Core.print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:15 ~part1:Part1.f ~part2:Part2.f
