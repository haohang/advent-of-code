open! Core
open! Utils

let parse_one elt =
  match String.split elt ~on:':' with
  | [ key; data ] -> key, data
  | _             -> raise_s [%message "Invalid entry" (elt : string)]
;;

let coagulate input =
  List.map
    (coagulate input)
    ~f:(List.concat_map ~f:(fun elt -> String.split elt ~on:' ' |> List.map ~f:parse_one))
;;

let count_valid input ~is_valid =
  let passports = coagulate input in
  let count = List.count passports ~f:is_valid in
  Core.print_s [%message "Done" (count : int)]
;;

module Part1 = struct
  let all_required_fields = [ "byr"; "iyr"; "eyr"; "hgt"; "hcl"; "ecl"; "pid" ]

  let is_valid passport =
    List.for_all all_required_fields ~f:(fun field ->
        List.exists passport ~f:(fun (key, _) -> String.equal key field))
  ;;

  let f = count_valid ~is_valid
end

module Part2 = struct
  let check_int_var s ~lower ~upper =
    let val_ = Int.of_string s in
    lower <= val_ && val_ <= upper
  ;;

  let check_byr = check_int_var ~lower:1920 ~upper:2002
  let check_iyr = check_int_var ~lower:2010 ~upper:2020
  let check_eyr = check_int_var ~lower:2020 ~upper:2030

  let check_hgt s =
    let check_cm = check_int_var ~lower:150 ~upper:193 in
    let check_in = check_int_var ~lower:59 ~upper:76 in
    match String.chop_suffix s ~suffix:"cm", String.chop_suffix s ~suffix:"in" with
    | None, None     -> false
    | Some cm, None  -> check_cm cm
    | None, Some in_ -> check_in in_
    | Some _, Some _ ->
      raise_s [%message "Weird! Height matched both inch and cm" (s : string)]
  ;;

  let check_hcl hcl =
    let regex = "^\\#[a-f0-9]{6}$" |> Re2.of_string in
    Re2.matches regex hcl
  ;;

  let check_ecl ecl =
    let valid = [ "amb"; "blu"; "brn"; "gry"; "grn"; "hzl"; "oth" ] in
    List.mem valid ~equal:String.equal ecl
  ;;

  let check_pid pid =
    let regex = "^[0-9]{9}$" |> Re2.of_string in
    Re2.matches regex pid
  ;;

  let%expect_test _ =
    Core.printf "%b\n" (check_hcl "#888785");
    [%expect {| true |}];
    Core.printf "%b\n" (check_hcl "#8887850");
    [%expect {| false |}];
    Core.printf "%b\n" (check_hcl "887850");
    [%expect {| false |}];
    Core.printf "%b\n" (check_hcl "#88878z");
    [%expect {| false |}];
    Core.printf "%b\n" (check_hgt "158cm");
    [%expect {| true |}];
    Core.printf "%b\n" (check_hgt "164cm");
    [%expect {| true |}];
    Core.printf "%b\n" (check_pid "545766238");
    [%expect {| true |}];
    Core.printf "%b\n" (check_ecl "wat");
    [%expect {| false |}];
    Core.printf "%b\n" (check_pid "000000000");
    [%expect {| true |}];
    Core.printf "%b\n" (check_hgt "163in");
    [%expect {| false |}]
  ;;

  let all_requirements =
    [ "byr", check_byr
    ; "iyr", check_iyr
    ; "eyr", check_eyr
    ; "hgt", check_hgt
    ; "hcl", check_hcl
    ; "ecl", check_ecl
    ; "pid", check_pid
    ]
  ;;

  let is_valid passport =
    let passport = String.Map.of_alist_exn passport in
    List.for_all all_requirements ~f:(fun (field, f) ->
        match String.Map.find passport field with
        | None      -> false
        | Some val_ -> f val_)
  ;;

  let f = count_valid ~is_valid
end

let command = Commands.both ~day:4 ~part1:Part1.f ~part2:Part2.f
