open! Core
open! Utils

module Rule = struct
  module Condition = struct
    type t =
      { lower : int
      ; upper : int
      }

    module Fail = struct
      module Fail_reason = struct
        type t =
          | Less_than_lower    of int
          | Greater_than_upper of int
        [@@deriving sexp_of]
      end

      type t =
        { field_value : int
        ; fail_reason : Fail_reason.t
        }
      [@@deriving sexp_of]
    end

    let passes' t num : (unit, Fail.t) Result.t =
      if t.lower > num
      then Error { field_value = num; fail_reason = Less_than_lower t.lower }
      else if t.upper < num
      then Error { field_value = num; fail_reason = Greater_than_upper t.upper }
      else Ok ()
    ;;

    let of_string s =
      match String.split s ~on:'-' with
      | [ lower; upper ] -> { lower = Int.of_string lower; upper = Int.of_string upper }
      | _                -> raise_s [%message "Invalid range" (s : string)]
    ;;
  end

  type t =
    { name : string
    ; cond1 : Condition.t
    ; cond2 : Condition.t
    }
  [@@deriving fields]

  let regex = "^([a-z ]+): ([0-9]+\\-[0-9]+) or ([0-9]+\\-[0-9]+)$" |> Re2.of_string

  let of_string s =
    let result = Re2.find_submatches_exn regex s in
    let name = Option.value_exn result.(1) in
    let cond1 = Condition.of_string (Option.value_exn result.(2)) in
    let cond2 = Condition.of_string (Option.value_exn result.(3)) in
    { name; cond1; cond2 }
  ;;

  module Check_result = struct
    type t =
      { name : string
      ; cond1_result : (unit, Condition.Fail.t) Result.t
      ; cond2_result : (unit, Condition.Fail.t) Result.t
      }
    [@@deriving sexp_of]

    let check { name; cond1; cond2 } num =
      let cond1_result = Condition.passes' cond1 num in
      let cond2_result = Condition.passes' cond2 num in
      { name; cond1_result; cond2_result }
    ;;

    let check_error t num =
      let result = check t num in
      Result.combine_errors_unit [ result.cond1_result; result.cond2_result ]
    ;;
  end

  let passes_rule t num = Result.is_ok (Check_result.check_error t num)
end

module Ticket = struct
  let of_string s = String.split s ~on:',' |> List.map ~f:Int.of_string

  let find_passing_rules rules num =
    List.filter rules ~f:(fun rule -> Rule.passes_rule rule num)
  ;;

  let find_invalid_numbers_one_ticket rules ticket =
    List.filter_map ticket ~f:(fun num ->
        let passing_rules = find_passing_rules rules num in
        Option.some_if (List.is_empty passing_rules) num)
  ;;

  let check rules ticket =
    List.mapi ticket ~f:(fun field_num num ->
        field_num, List.map rules ~f:(fun rule -> Rule.Check_result.check rule num))
  ;;

  let%expect_test "check" =
    let rules =
      {|class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50|}
      |> String.split ~on:'\n'
      |> List.map ~f:Rule.of_string
    in
    let ticket = [ 7; 3; 47 ] in
    let failures = check rules ticket in
    Core.print_s [%message "" (failures : (int * Rule.Check_result.t list) list)];
    [%expect
      {|
      (failures
       ((0
         (((name class)
           (cond1_result
            (Error ((field_value 7) (fail_reason (Greater_than_upper 3)))))
           (cond2_result (Ok ())))
          ((name row) (cond1_result (Ok ()))
           (cond2_result
            (Error ((field_value 7) (fail_reason (Less_than_lower 33))))))
          ((name seat)
           (cond1_result
            (Error ((field_value 7) (fail_reason (Less_than_lower 13)))))
           (cond2_result
            (Error ((field_value 7) (fail_reason (Less_than_lower 45))))))))
        (1
         (((name class) (cond1_result (Ok ()))
           (cond2_result
            (Error ((field_value 3) (fail_reason (Less_than_lower 5))))))
          ((name row)
           (cond1_result
            (Error ((field_value 3) (fail_reason (Less_than_lower 6)))))
           (cond2_result
            (Error ((field_value 3) (fail_reason (Less_than_lower 33))))))
          ((name seat)
           (cond1_result
            (Error ((field_value 3) (fail_reason (Less_than_lower 13)))))
           (cond2_result
            (Error ((field_value 3) (fail_reason (Less_than_lower 45))))))))
        (2
         (((name class)
           (cond1_result
            (Error ((field_value 47) (fail_reason (Greater_than_upper 3)))))
           (cond2_result
            (Error ((field_value 47) (fail_reason (Greater_than_upper 7))))))
          ((name row)
           (cond1_result
            (Error ((field_value 47) (fail_reason (Greater_than_upper 11)))))
           (cond2_result
            (Error ((field_value 47) (fail_reason (Greater_than_upper 44))))))
          ((name seat)
           (cond1_result
            (Error ((field_value 47) (fail_reason (Greater_than_upper 40)))))
           (cond2_result (Ok ()))))))) |}]
  ;;

  let check_valid rules ticket =
    List.is_empty (find_invalid_numbers_one_ticket rules ticket)
  ;;
end

let parse_mine lines =
  match lines with
  | [ "your ticket:"; ticket ] -> Ticket.of_string ticket
  | _                          -> raise_s
                                    [%message "Invalid ticket lines" (lines : string list)]
;;

let parse_others lines =
  match lines with
  | "nearby tickets:" :: tickets -> List.map tickets ~f:Ticket.of_string
  | _                            -> raise_s
                                      [%message
                                        "Invalid nearby ticket lines" (lines : string list)]
;;

let parse input =
  let input = coagulate input in
  match input with
  | [ rules; ticket; others ] ->
    List.map rules ~f:Rule.of_string, parse_mine ticket, parse_others others
  | _                         -> raise_s [%message "Invalid input"]
;;

module Part1 = struct
  let f input =
    let rules, _ticket, others = parse input in
    let result =
      List.concat_map others ~f:(Ticket.find_invalid_numbers_one_ticket rules)
      |> List.reduce_exn ~f:( + )
    in
    Core.print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let find_valid_permissions rules valid_tickets =
    (* [possibilities] is a list of sets representing the possible rules each
       position could represent. *)
    let possibilities =
      List.map valid_tickets ~f:(fun ticket ->
          List.map ticket ~f:(fun elt ->
              Ticket.find_passing_rules rules elt
              |> List.map ~f:Rule.name
              |> String.Set.of_list))
      |> List.reduce_exn ~f:(fun ticket1 ticket2 ->
             List.zip_exn ticket1 ticket2
             |> List.map ~f:(fun (pos1, pos2) -> String.Set.inter pos1 pos2))
    in
    (* We need to apply enough times to eliminate all the possibilities that need
       eliminating, which is at most the number of rules there are. *)
    let possibilities =
      let rec aux set possibilities =
        match
          List.findi possibilities ~f:(fun i elt ->
              (not (Int.Set.mem set i)) && String.Set.length elt = 1)
        with
        (* We assume that there's exactly one valid arrangement of rules. If
           nothing if found here, that means that all of the positions have been
           assigned rules, so we're done. *)
        | None -> possibilities
        | Some (i, elt) ->
          let elt = String.Set.to_list elt |> exactly_one_exn in
          aux
            (Int.Set.add set i)
            (List.mapi possibilities ~f:(fun j other ->
                 if i <> j then String.Set.remove other elt else other))
      in
      aux Int.Set.empty possibilities
    in
    List.map possibilities ~f:(fun elt -> String.Set.elements elt |> exactly_one_exn)
  ;;

  let f input =
    let rules, ticket, others = parse input in
    let valid_tickets = List.filter others ~f:(Ticket.check_valid rules) in
    let valid_perms = find_valid_permissions rules valid_tickets in
    let result =
      List.zip_exn valid_perms ticket
      |> List.filter_map ~f:(fun (rule, num) ->
             Option.some_if (String.is_prefix rule ~prefix:"departure") num)
      |> List.reduce_exn ~f:( * )
    in
    Core.print_s [%message "Done" (result : int) (valid_perms : string list)]
  ;;
end

let command = Commands.both ~day:16 ~part1:Part1.f ~part2:Part2.f
