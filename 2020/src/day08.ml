open! Core
open! Utils

module Instruction = struct
  type t =
    | Acc of int
    | Nop of int
    | Jmp of int

  let of_string s =
    match String.split s ~on:' ' with
    | [ op; arg ] ->
      (match op with
      | "acc" -> Acc (Int.of_string arg)
      | "nop" -> Nop (Int.of_string arg)
      | "jmp" -> Jmp (Int.of_string arg)
      | _     -> raise_s [%message "Invalid instruction" (s : string)])
    | _           -> raise_s [%message "Invalid instruction" (s : string)]
  ;;
end

let run instructions =
  let accum = ref 0 in
  let pc = ref 0 in
  let visited = Int.Hash_set.create () in
  let rec exec () =
    if Hash_set.mem visited !pc
    then `Dup !accum
    else if !pc >= Array.length instructions
    then `Finished !accum
    else (
      Hash_set.add visited !pc;
      match instructions.(!pc) with
      | Instruction.Acc amt ->
        accum := !accum + amt;
        incr pc;
        exec ()
      | Jmp amt             ->
        pc := !pc + amt;
        exec ()
      | Nop _               ->
        incr pc;
        exec ())
  in
  exec ()
;;

module Part1 = struct
  let f input =
    let instructions = List.map input ~f:Instruction.of_string |> Array.of_list in
    match run instructions with
    | `Finished _ -> raise_s [%message "BUG - expected dup but finished"]
    | `Dup result -> Core.print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let f input =
    let instructions = List.map input ~f:Instruction.of_string in
    let result =
      List.find_mapi_exn instructions ~f:(fun i instr ->
          match instr with
          | Jmp arg ->
            let instr_array = Array.of_list instructions in
            instr_array.(i) <- Nop arg;
            (match run instr_array with
            | `Dup _           -> None
            | `Finished result -> Some result)
          | Nop arg ->
            let instr_array = Array.of_list instructions in
            instr_array.(i) <- Jmp arg;
            (match run instr_array with
            | `Dup _           -> None
            | `Finished result -> Some result)
          | Acc _   -> None)
    in
    Core.print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:8 ~part1:Part1.f ~part2:Part2.f
