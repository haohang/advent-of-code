open! Core
open! Utils

let parse input =
  let target = List.hd_exn input |> Int.of_string in
  let buses =
    List.last_exn input
    |> String.split ~on:','
    |> List.map ~f:(function
           | "x" -> None
           | i   -> Some (Int.of_string i))
  in
  target, buses
;;

module Part1 = struct
  let smallest_multiple_greater_than i ~than_ = ((than_ / i) + 1) * i

  let f input =
    let target, buses = parse input in
    let best_bus, best_time =
      List.filter_opt buses
      |> List.map ~f:(fun elt -> elt, smallest_multiple_greater_than elt ~than_:target)
      |> List.min_elt ~compare:(fun (_, time1) (_, time2) -> Int.compare time1 time2)
      |> Option.value_exn
    in
    Core.print_s
      [%message
        "Done"
          (best_time : int)
          (best_bus : int)
          ~answer:(best_bus * (best_time - target) : int)]
  ;;
end

module Part2 = struct
  let f input =
    let _, buses = parse input in
    let congruences =
      List.filter_mapi buses ~f:(fun offset bus ->
          Option.map bus ~f:(fun bus -> offset, bus))
      |> List.map ~f:(fun (offset, bus) ->
             (* If we want a bus that arrives every N minutes to arrive X
                minutes after t, then t ~ (N - X) (mod N) *)
             -offset % bus, bus)
    in
    let result, _ = solve_congruences congruences in
    Core.print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:13 ~part1:Part1.f ~part2:Part2.f
