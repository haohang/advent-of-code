open! Core
open! Utils

module Instruction = struct
  type t =
    | Mask of int option list
    | Set  of (int * int)
  [@@deriving sexp_of]

  let mem_regex = Re2.of_string "mem\\[([0-9]+)\\] = ([0-9]+)"

  let of_string s =
    match String.chop_prefix s ~prefix:"mask = " with
    | Some s ->
      let mask =
        String.to_list s
        |> List.map ~f:(function
               | '0' -> Some 0
               | '1' -> Some 1
               | 'X' -> None
               | _   -> raise_s [%message "BUG"])
      in
      Mask mask
    | None   ->
      let results = Re2.find_submatches_exn mem_regex s in
      let addr = Int.of_string (Option.value_exn results.(1)) in
      let val_ = Int.of_string (Option.value_exn results.(2)) in
      Set (addr, val_)
  ;;

  let%expect_test "_" =
    let s = of_string "mem[8] = 123" in
    Core.print_s [%message "" (s : t)];
    [%expect {| (s (Set (8 123))) |}]
  ;;
end

let run instructions ~update_mem =
  List.fold instructions ~init:(Int.Map.empty, []) ~f:(fun (mem, mask) instruction ->
      match instruction with
      | Instruction.Mask mask -> mem, mask
      | Set (addr, val_)      -> update_mem mem ~addr ~mask ~val_, mask)
;;

let f input ~run =
  let mem, _ = run (List.map input ~f:Instruction.of_string) in
  let _, result =
    Int.Map.to_alist mem |> List.reduce_exn ~f:(fun (_, val1) (_, val2) -> 0, val1 + val2)
  in
  Core.print_s [%message "Done" (result : int)]
;;

module Part1 = struct
  let run instructions =
    let update_mem mem ~addr ~mask ~val_ =
      let val_ = decimal_to_binary_digits val_ |> pad_left ~len:36 ~with_:0 in
      let val_ =
        List.zip_exn mask val_
        |> List.map ~f:(fun (mask, val_) -> Option.value mask ~default:val_)
        |> binary_digits_to_decimal
      in
      Int.Map.set mem ~key:addr ~data:val_
    in
    run instructions ~update_mem
  ;;

  let f input = f input ~run
end

module Part2 = struct
  let run instructions =
    let update_mem mem ~addr ~mask ~val_ =
      let addr' = decimal_to_binary_digits addr |> pad_left ~len:36 ~with_:0 in
      let addrs =
        List.zip_exn mask addr'
        |> List.map ~f:(fun (mask, addr) ->
               match mask with
               | Some 1 -> [ 1 ]
               | Some _ -> [ addr ]
               | None   -> [ 0; 1 ])
        |> resolve_choices
        |> List.map ~f:binary_digits_to_decimal
      in
      List.fold addrs ~init:mem ~f:(fun mem addr -> Int.Map.set mem ~key:addr ~data:val_)
    in
    run instructions ~update_mem
  ;;

  let f input = f input ~run
end

let command = Commands.both ~day:14 ~part1:Part1.f ~part2:Part2.f
