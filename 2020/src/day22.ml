open! Core
open! Utils

let parse_hands input =
  match coagulate input with
  | [ ("Player 1:" :: player1); ("Player 2:" :: player2) ] ->
    List.map ~f:Int.of_string player1, List.map ~f:Int.of_string player2
  | _ -> raise_s [%message "Invalid input" (input : string list)]
;;

let f input ~play =
  let p1, p2 = parse_hands input in
  let p1, p2 = play p1 p2 in
  let result =
    List.foldi
      (List.rev (p1 @ p2))
      ~init:0
      ~f:(fun i accum elt -> accum + ((i + 1) * elt))
  in
  Core.print_s [%message "Done" (result : int) (p1 : int list) (p2 : int list)]
;;

module Part1 = struct
  let rec play player1 player2 =
    match player1, player2 with
    | [], _ | _, []                -> player1, player2
    | p1 :: p1_rest, p2 :: p2_rest ->
      if p1 > p2
      then play (p1_rest @ [ p1; p2 ]) p2_rest
      else play p1_rest (p2_rest @ [ p2; p1 ])
  ;;

  let f = f ~play
end

module Part2 = struct
  module Deck = struct
    module T = struct
      type t = int list [@@deriving compare, hash, sexp]
    end

    include T
    include Comparable.Make (T)
  end

  let play player1 player2 =
    let rec aux player1 player2 decks1_seen decks2_seen =
      match player1, player2 with
      | [], _ | _, []                -> player1, player2
      | p1 :: p1_rest, p2 :: p2_rest ->
        let p1_win =
          if Deck.Set.mem decks1_seen player1 || Deck.Set.mem decks2_seen player2
          then true
          else (
            match List.length p1_rest >= p1, List.length p2_rest >= p2 with
            | true, true ->
              let player1 = List.take p1_rest p1 in
              let player2 = List.take p2_rest p2 in
              let player1, _player2 = aux player1 player2 Deck.Set.empty Deck.Set.empty in
              not (List.is_empty player1)
            | _          -> p1 > p2)
        in
        let decks1_seen = Deck.Set.add decks1_seen player1 in
        let decks2_seen = Deck.Set.add decks2_seen player2 in
        if p1_win
        then aux (p1_rest @ [ p1; p2 ]) p2_rest decks1_seen decks2_seen
        else aux p1_rest (p2_rest @ [ p2; p1 ]) decks1_seen decks2_seen
    in
    aux player1 player2 Deck.Set.empty Deck.Set.empty
  ;;

  let f = f ~play
end

let command = Commands.both ~day:22 ~part1:Part1.f ~part2:Part2.f
