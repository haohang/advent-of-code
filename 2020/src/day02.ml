open! Core
open! Std_internal

module Entry = struct
  type t =
    { first : int
    ; second : int
    ; letter : string
    ; password : string
    }
  [@@deriving fields, sexp_of]

  let creator, () =
    Fields.make_creator
      ~first:(fun field () -> Simple_parser.Result.get_int ~field, ())
      ~second:(fun field () -> Simple_parser.Result.get_int ~field, ())
      ~letter:(fun field () -> Simple_parser.Result.get_string ~field, ())
      ~password:(fun field () -> Simple_parser.Result.get_string ~field, ())
      ()
  ;;

  let spec =
    [ "(first int)"
    ; "-"
    ; "(second int)"
    ; " "
    ; "(letter string)"
    ; ": "
    ; "(password string)"
    ]
  ;;

  let of_string s =
    let result = Simple_parser.parse ~s spec in
    creator result
  ;;

  let%expect_test "of_string" =
    Core.printf !"%{sexp: t}\n%!" (of_string "1-3 a: abcde");
    [%expect {| ((first 1) (second 3) (letter a) (password abcde)) |}]
  ;;
end

let count_valid ~f input =
  let valid = List.count input ~f in
  print_s [%message "Done" (valid : int)]
;;

module Part1 = struct
  let check_entry entry =
    let { Entry.password; first; second; letter } = Entry.of_string entry in
    let count =
      List.count (String.to_list password) ~f:(Char.equal (Char.of_string letter))
    in
    count >= first && count <= second
  ;;

  let f = count_valid ~f:check_entry
end

module Part2 = struct
  let safe_get_char s n = if n > String.length s then None else Some s.[n]

  let%expect_test "safe_get_char" =
    let test s n = Core.printf !"%{sexp: char option}\n" (safe_get_char s n) in
    test "abcde" 3;
    [%expect {| (d) |}];
    test "abcde" 6;
    [%expect {| () |}]
  ;;

  let check_entry entry =
    let { Entry.password; first; second; letter } = Entry.of_string entry in
    let first_matches =
      Option.value_map
        ~default:false
        (safe_get_char password (first - 1))
        ~f:(Char.equal (Char.of_string letter))
    in
    let second_matches =
      Option.value_map
        ~default:false
        (safe_get_char password (second - 1))
        ~f:(Char.equal (Char.of_string letter))
    in
    Bool.( <> ) first_matches second_matches
  ;;

  let%expect_test "check_entry" =
    Core.printf "%b" (check_entry "1-3 a: abcde");
    [%expect {| true |}]
  ;;

  let f = count_valid ~f:check_entry
end

let command = Commands.both ~day:2 ~part1:Part1.f ~part2:Part2.f
