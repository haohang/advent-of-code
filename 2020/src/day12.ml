open! Core
open! Utils
open Graph

module Instruction = struct
  module Directive = struct
    type t =
      | Abs     of Direction.t
      | Turn    of Rotation.t
      | Forward
  end

  let of_string s =
    let dir =
      match String.nget s 0 with
      | 'N' -> Directive.Abs Up
      | 'S' -> Abs Down
      | 'W' -> Abs Left
      | 'E' -> Abs Right
      | 'F' -> Forward
      | 'L' -> Turn Left
      | 'R' -> Turn Right
      | _   -> raise_s [%message "Invalid" (s : string)]
    in
    let num = String.drop_prefix s 1 |> Int.of_string in
    dir, num
  ;;

  let apply (dir, num) ~ship ~waypoint ~cardinal_moves_what =
    match dir with
    | Directive.Abs dir ->
      (match cardinal_moves_what with
      | `Ship     -> waypoint, Coord.move ship ~vec:(Direction.deltas dir) ~n:num
      | `Waypoint -> Coord.move waypoint ~vec:(Direction.deltas dir) ~n:num, ship)
    | Forward           -> waypoint, Coord.move ship ~vec:waypoint ~n:num
    | Turn dir          -> Coord.rotate_n_times waypoint ~dir ~n:(num / 90), ship
  ;;
end

let f' input ~waypoint ~cardinal_moves_what =
  let run instr =
    let _waypoint, ship =
      List.fold instr ~init:(waypoint, Coord.origin) ~f:(fun (waypoint, ship) instr ->
          Instruction.apply instr ~ship ~waypoint ~cardinal_moves_what)
    in
    Coord.manhattan_dist Coord.origin ship
  in
  let instr = List.map input ~f:Instruction.of_string in
  let dist = run instr in
  Core.print_s [%message "Done" (dist : int)]
;;

module Part1 = struct
  let f = f' ~waypoint:Coord.east ~cardinal_moves_what:`Ship
end

module Part2 = struct
  let f = f' ~waypoint:{ Coord.row = -1; col = 10 } ~cardinal_moves_what:`Waypoint
end

let command = Commands.both ~day:12 ~part1:Part1.f ~part2:Part2.f
