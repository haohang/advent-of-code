open! Core
open! Utils

let regex = "^([a-z ]+)\\(contains ([a-z ,]+)\\)$" |> Re2.of_string
let parse_ingredients ingredients = String.strip ingredients |> String.split ~on:' '

let parse_allergens allergens =
  String.strip allergens |> String.split ~on:',' |> List.map ~f:String.strip
;;

let parse line =
  let result = Re2.find_submatches_exn regex line in
  let ingredients = Option.value_exn result.(1) |> parse_ingredients in
  let allergens = Option.value_exn result.(2) |> parse_allergens in
  ingredients, allergens
;;

let ingredients_to_allergens input =
  List.concat_map input ~f:(fun line ->
      let ingredients, allergens = parse line in
      List.map ingredients ~f:(fun i -> i, String.Set.of_list allergens))
  |> String.Map.of_alist_multi
;;

let allergens_to_ingredients input =
  List.concat_map input ~f:(fun line ->
      let ingredients, allergens = parse line in
      List.map allergens ~f:(fun i -> i, String.Set.of_list ingredients))
  |> String.Map.of_alist_multi
;;

let possible_ingredients_for_allergen info allergen =
  String.Map.find_exn info allergen |> List.reduce_exn ~f:String.Set.inter
;;

module Part1 = struct
  let f input =
    let ingredient_map = ingredients_to_allergens input in
    let all_ingredients = ingredient_map |> String.Map.keys |> String.Set.of_list in
    let allergens_to_ingredients = allergens_to_ingredients input in
    let allergens = String.Map.keys allergens_to_ingredients in
    let possible_allergens =
      List.map allergens ~f:(possible_ingredients_for_allergen allergens_to_ingredients)
      |> List.reduce_exn ~f:String.Set.union
    in
    let no_allergens =
      String.Set.diff all_ingredients possible_allergens |> String.Set.to_list
    in
    let result =
      List.map no_allergens ~f:(fun i ->
          String.Map.find_exn ingredient_map i |> List.length)
      |> List.reduce_exn ~f:( + )
    in
    Core.print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let f input =
    let ingredient_map = ingredients_to_allergens input in
    let all_ingredients = ingredient_map |> String.Map.keys |> String.Set.of_list in
    let map = allergens_to_ingredients input in
    let allergens = String.Map.keys map in
    let possible_allergens =
      List.map allergens ~f:(possible_ingredients_for_allergen map)
      |> List.reduce_exn ~f:String.Set.union
    in
    let no_allergens = String.Set.diff all_ingredients possible_allergens in
    let allergens_map =
      String.Map.map map ~f:(fun lst ->
          List.map lst ~f:(fun ingredients -> String.Set.diff ingredients no_allergens))
    in
    let possible_assignment =
      List.map allergens ~f:(fun elt ->
          elt, possible_ingredients_for_allergen allergens_map elt)
    in
    let rec aux assigned rest =
      match rest with
      | []                  -> Some assigned
      | (elt, poss) :: rest ->
        String.Set.find_map poss ~f:(fun ingredient ->
            match String.Map.find assigned ingredient with
            | Some _ -> None
            | None   -> aux (String.Map.set assigned ~key:ingredient ~data:elt) rest)
    in
    let allergens =
      aux String.Map.empty possible_assignment
      |> Option.value_exn
      |> String.Map.to_alist
      |> List.sort ~compare:(Comparable.lift String.compare ~f:snd)
      |> List.map ~f:fst
      |> String.concat ~sep:","
    in
    Core.print_s [%message "Done" (allergens : string)]
  ;;
end

let command = Commands.both ~day:21 ~part1:Part1.f ~part2:Part2.f
