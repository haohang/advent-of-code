open! Core
open! Utils

module Part1 = struct
  let f input =
    let input = List.map input ~f:Int.of_string in
    let check i elt =
      if i < 25
      then true
      else (
        let sublist = List.sub input ~pos:(i - 25) ~len:25 in
        List.exists (List.cartesian_product sublist sublist) ~f:(fun (a, b) ->
            a + b = elt))
    in
    let _, elt =
      List.findi input ~f:(fun i elt -> not (check i elt)) |> Option.value_exn
    in
    Core.print_s [%message "Done" (elt : int)]
  ;;
end

module Part2 = struct
  let f input =
    let target = 29221323 in
    let input = List.map input ~f:Int.of_string in
    let _, partial_sums =
      List.fold input ~init:(0, []) ~f:(fun (sum, accum) elt ->
          sum + elt, (sum + elt) :: accum)
    in
    let partial_sums = List.rev partial_sums in
    let i, j =
      List.find_mapi_exn partial_sums ~f:(fun i elt1 ->
          match
            List.findi
              (List.drop partial_sums (i + 1))
              ~f:(fun _j elt2 -> elt2 - elt1 = target)
          with
          | None        -> None
          | Some (j, _) -> Some (i, j))
    in
    let sublist = List.sub input ~pos:(i + 1) ~len:j |> List.sort ~compare:Int.compare in
    let min, max = List.hd_exn sublist, List.last_exn sublist in
    let sum = min + max in
    Core.print_s [%message "Done" (sum : int)]
  ;;
end

let command = Commands.both ~day:9 ~part1:Part1.f ~part2:Part2.f
