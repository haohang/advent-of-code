open! Core
open! Utils

module Part1 = struct
  let f input =
    let input = List.map input ~f:Int.of_string |> List.sort ~compare:Int.compare in
    let built_in = List.last_exn input + 3 in
    let diffs =
      List.zip_exn (0 :: input) (input @ [ built_in ])
      |> List.map ~f:(fun (lo, hi) -> hi - lo)
    in
    let ones = List.count diffs ~f:(Int.equal 1) in
    let threes = List.count diffs ~f:(Int.equal 3) in
    Core.print_s [%message "Done" ~product:(ones * threes : int)]
  ;;
end

module Part2 = struct
  let rec count_seqs ~prev ~rest ~target ~memo =
    match rest with
    | []      -> if target - prev > 3 then 0 else 1
    | x :: xs ->
      if x - prev > 3
      then 0
      else (
        let including_x =
          match Int.Table.find memo x with
          | None        -> count_seqs ~prev:x ~rest:xs ~target ~memo
          | Some result -> result
        in
        let excluding_x = count_seqs ~prev ~rest:xs ~memo ~target in
        Int.Table.set memo ~key:x ~data:including_x;
        including_x + excluding_x)
  ;;

  let f input =
    let input = List.map input ~f:Int.of_string |> List.sort ~compare:Int.compare in
    let built_in = List.last_exn input + 3 in
    let memo = Int.Table.create () in
    let total = count_seqs ~prev:0 ~rest:input ~target:built_in ~memo in
    Core.print_s [%message "Done" (total : int)]
  ;;
end

let command = Commands.both ~day:10 ~part1:Part1.f ~part2:Part2.f
