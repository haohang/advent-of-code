open! Core
open! Utils

module Expr = struct
  let eval_base s ~combine =
    match String.split ~on:' ' s with
    | []       -> raise_s [%message "Invalid simple expr" (s : string)]
    | hd :: tl ->
      let accum, reg, op =
        List.fold
          tl
          ~init:([], Int.of_string hd, None)
          ~f:(fun (accum, reg, op) next ->
            match op with
            | None    -> accum, reg, Some next
            | Some op ->
              let accum, reg = combine accum reg op next in
              accum, reg, None)
      in
      (match op with
      | Some _ -> raise_s [%message "Invalid simple expr" (s : string)]
      | None   -> accum, reg)
  ;;

  let eval_eager s =
    eval_base s ~combine:(fun accum reg op next ->
        let f =
          match op with
          | "*" -> ( * )
          | "+" -> ( + )
          | _   -> raise_s [%message "Invalid op" (op : string)]
        in
        accum, f (Int.of_string next) reg)
    |> snd
  ;;

  let%expect_test "eval_eager" =
    Core.printf "%d\n%!" (eval_eager "2 * 3 + 4");
    [%expect {| 10 |}]
  ;;

  let eval_adds_first s =
    let accum, reg =
      eval_base s ~combine:(fun accum reg op next ->
          match op with
          | "+" -> accum, Int.of_string next + reg
          | "*" -> accum @ [ reg ], Int.of_string next
          | _   -> raise_s [%message "Invalid op" (op : string)])
    in
    List.fold accum ~init:reg ~f:( * )
  ;;

  let%expect_test "eval_adds_first" =
    Core.printf "%d\n%!" (eval_adds_first "2 * 3 + 4");
    [%expect {| 14 |}]
  ;;

  let find_outer_parens s =
    String.foldi s ~init:([], []) ~f:(fun i (opens, pairs) next ->
        match next with
        | '(' -> i :: opens, pairs
        | ')' ->
          (match opens with
          | [ open_idx ] -> [], (open_idx, i) :: pairs
          | _ :: tl      -> tl, pairs
          | []           -> raise_s [%message "Unmatched closing paren" (i : int)])
        | _   -> opens, pairs)
    |> snd
    |> List.rev
  ;;

  let%expect_test "find_outer_parens" =
    let parens = find_outer_parens "(2 + 4 * 9) * (6 + 9 * 8 + 6) + 6" in
    Core.print_s [%message "" (parens : (int * int) list)];
    [%expect {| (parens ((0 10) (14 28))) |}]
  ;;

  let rec eval s ~eval_base =
    let len = String.length s in
    let parens = find_outer_parens s in
    let reduced, prev_close =
      List.fold parens ~init:([], 0) ~f:(fun (accum, prev_close) (i, j) ->
          let prev_chunk = slice s prev_close i in
          let reduced_chunk = Int.to_string (eval ~eval_base (slice s (i + 1) j)) in
          reduced_chunk :: prev_chunk :: accum, j + 1)
    in
    slice s prev_close len :: reduced |> List.rev |> String.concat ~sep:"" |> eval_base
  ;;

  let%expect_test "eval eager" =
    let eval = eval ~eval_base:eval_eager in
    Core.printf "%d\n%!" (eval "2 * 3 * (1 * 2 + 1)");
    [%expect {| 18 |}];
    Core.printf "%d\n%!" (eval "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))");
    [%expect {| 12240 |}];
    Core.printf "%d\n%!" (eval "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2");
    [%expect{| 13632 |}]
  ;;

  let%expect_test "eval adds first" =
    let eval = eval ~eval_base:eval_adds_first in
    Core.printf "%d\n%!" (eval "1 + (2 * 3) + (4 * (5 + 6))");
    [%expect{| 51 |}];
    Core.printf "%d\n%!" (eval "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))");
    [%expect{| 669060 |}];
    Core.printf "%d\n%!" (eval "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2");
    [%expect{| 23340 |}]
  ;;
end

let f input ~eval_base =
  let result = List.map input ~f:(Expr.eval ~eval_base) |> List.reduce_exn ~f:( + ) in
  Core.print_s [%message "Done" (result : int)]
;;

module Part1 = struct
  let f = f ~eval_base:Expr.eval_eager
end

module Part2 = struct
  let f = f ~eval_base:Expr.eval_adds_first
end

let command = Commands.both ~day:18 ~part1:Part1.f ~part2:Part2.f
