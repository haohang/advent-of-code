open! Core
open! Utils

module Relationship = struct
  type t =
    { bag : string
    ; contains : (int * string) list
    }
  [@@deriving fields, sexp_of]

  let parse_one_contains s =
    let regex = "^([0-9]+) ([a-z ]+) bag[s]?$" |> Re2.of_string in
    let result = Re2.find_submatches_exn regex s in
    Int.of_string (Option.value_exn result.(1)), Option.value_exn result.(2)
  ;;

  let parse_contains s =
    match s with
    | "no other bags" -> []
    | s               ->
      String.split s ~on:','
      |> List.map ~f:String.lstrip
      |> List.map ~f:parse_one_contains
  ;;

  let parse s =
    let regex = "^([a-z ]+) bags contain ([a-z ,0-9]+)\\.$" |> Re2.of_string in
    match Re2.find_submatches regex s with
    | Error _   -> raise_s [%message "Entry failed" (s : string)]
    | Ok result -> Option.value_exn result.(1), Option.value_exn result.(2)
  ;;

  let of_string s =
    let bag, contains = parse s in
    let contains = parse_contains contains in
    { bag; contains }
  ;;

  let%expect_test "of_string" =
    Core.printf
      !"%{sexp: t}\n%!"
      (of_string "light red bags contain 1 bright white bag, 2 muted yellow bags.");
    [%expect {| ((bag "light red") (contains ((1 "bright white") (2 "muted yellow")))) |}]
  ;;
end

module Part1 = struct
  let rec can_contain_shiny_gold relationships bag =
    match String.Map.find relationships bag with
    | None      -> false
    | Some bags ->
      List.exists bags ~f:(fun (_, bag) ->
          String.equal bag "shiny gold" || can_contain_shiny_gold relationships bag)
  ;;

  let f input =
    let relationships =
      List.map input ~f:Relationship.of_string
      |> List.map ~f:(fun { Relationship.contains; bag } -> bag, contains)
      |> String.Map.of_alist_exn
    in
    let to_check = String.Map.keys relationships in
    let num = List.count to_check ~f:(can_contain_shiny_gold relationships) in
    Core.print_s [%message "Done" (num : int)]
  ;;
end

module Part2 = struct
  let rec total_bags relationships bag =
    match String.Map.find relationships bag with
    | None      -> 1
    | Some bags ->
      List.fold bags ~init:1 ~f:(fun accum (num, bag) ->
          accum + (num * total_bags relationships bag))
  ;;

  let f input =
    let relationships =
      List.map input ~f:Relationship.of_string
      |> List.map ~f:(fun { Relationship.contains; bag } -> bag, contains)
      |> String.Map.of_alist_exn
    in
    (* Subtract one because spec doesn't contain initial bag *)
    let num = total_bags relationships "shiny gold" - 1 in
    Core.print_s [%message "Done" (num : int)]
  ;;
end

let command = Commands.both ~day:7 ~part1:Part1.f ~part2:Part2.f
