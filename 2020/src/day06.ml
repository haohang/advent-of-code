open! Core
open! Utils

let reduce_groups input ~f =
  let groups = coagulate input in
  List.map groups ~f:(fun group ->
      List.map group ~f:(fun elt -> String.to_list elt |> Char.Set.of_list)
      |> List.reduce_exn ~f
      |> Char.Set.length)
  |> List.reduce_exn ~f:( + )
;;

module Part1 = struct
  let f input =
    let count = reduce_groups input ~f:Char.Set.union in
    Core.print_s [%message "Done" (count : int)]
  ;;
end

module Part2 = struct
  let f input =
    let count = reduce_groups input ~f:Char.Set.inter in
    Core.print_s [%message "Done" (count : int)]
  ;;
end

let command = Commands.both ~day:6 ~part1:Part1.f ~part2:Part2.f
