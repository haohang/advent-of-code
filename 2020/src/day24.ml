open! Core
open! Utils
open! Graph

(* module Coord = struct
 *   module T = struct
 *     type t = int * int * int [@@deriving hash, compare, sexp]
 *   end
 * 
 *   include T
 *   include Comparable.Make (T)
 * end *)

let dir_of_char = function
  | 'A' -> `se
  | 'B' -> `sw
  | 'C' -> `nw
  | 'D' -> `ne
  | 'w' -> `w
  | 'e' -> `e
  | c   -> raise_s [%message "Invalid" (c : char)]
;;

let move { Coord.row; col } dir =
  let dcol =
    match dir with
    | `w        -> -1
    | `e        -> 1
    | `se | `ne -> row % 2 * 1
    | `sw | `nw -> (row + 1) % 2 * -1
  in
  let drow =
    match dir with
    | `e | `w   -> 0
    | `nw | `ne -> 1
    | `sw | `se -> -1
  in
  { Coord.row = row + drow; col = col + dcol }
;;

let get_coord line =
  let l = String.strip line |> String.to_list |> List.map ~f:dir_of_char in
  List.fold l ~init:Coord.origin ~f:(fun curr dir -> move curr dir)
;;

let get_neighbors coord =
  List.map [ `w; `e; `sw; `se; `nw; `ne ] ~f:(fun dir -> move coord dir)
;;

let parse input =
  List.map input ~f:get_coord
  |> List.mapi ~f:(fun i elt -> elt, i)
  |> Coord.Map.of_alist_multi
;;

module Part1 = struct
  let f input =
    let result =
      parse input
      |> Coord.Map.filter ~f:(fun appearances -> List.length appearances % 2 = 1)
      |> Coord.Map.length
    in
    Core.print_s [%message "Done" (result : int)]
  ;;
end

module Part2 = struct
  let f input =
    let input =
      parse input
      |> Coord.Map.filter ~f:(fun appearances -> List.length appearances % 2 = 1)
      |> Coord.Map.keys
      |> Coord.Set.of_list
    in
    let run black_tiles =
      let black = Coord.Set.to_list black_tiles in
      let to_check = List.concat_map black ~f:get_neighbors in
      let num_black c = get_neighbors c |> List.count ~f:(Coord.Set.mem black_tiles) in
      let newly_black =
        List.filter to_check ~f:(fun c -> num_black c = 2) |> Coord.Set.of_list
      in
      let still_black =
        List.filter black ~f:(fun c ->
            let b = num_black c in
            b = 1 || b = 2)
        |> Coord.Set.of_list
      in
      Coord.Set.union newly_black still_black
    in
    let result = Fn.apply_n_times ~n:100 run input |> Coord.Set.length in
    Core.print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:24 ~part1:Part1.f ~part2:Part2.f
