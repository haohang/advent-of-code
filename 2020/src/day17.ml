open! Core
open! Utils
open Graph

module State = struct
  type t =
    | Active
    | Inactive

  let of_char = function
    | '#' -> Active
    | '.' -> Inactive
    | c   -> raise_s [%message "Invalid" (c : char)]
  ;;
end

let run state ~n =
  Fn.apply_n_times
    ~n
    (fun state ->
      let currently_active = NCoord.Set.to_list state in
      let all_to_consider =
        currently_active @ List.concat_map currently_active ~f:NCoord.adjacent
        |> NCoord.Set.of_list
      in
      NCoord.Set.fold all_to_consider ~init:NCoord.Set.empty ~f:(fun accum elt ->
          let num_active_neighbors =
            NCoord.adjacent elt |> List.count ~f:(NCoord.Set.mem state)
          in
          let is_active = NCoord.Set.mem state elt in
          match is_active, num_active_neighbors with
          | true, (2 | 3) | false, 3 -> NCoord.Set.add accum elt
          | _                        -> accum))
    state
;;

let f input ~dim =
  let grid = Grid.of_char_strings input ~f:State.of_char in
  let active =
    Grid.foldi grid ~init:NCoord.Set.empty ~f:(fun { Coord.row; col } accum elt ->
        match elt with
        | Active   ->
          let coord = pad_right [ row; col ] ~len:dim ~with_:0 in
          NCoord.Set.add accum (NCoord.create_exn coord ~dim)
        | Inactive -> accum)
  in
  let result = run ~n:6 active |> NCoord.Set.length in
  Core.print_s [%message "Done" (result : int)]
;;

module Part1 = struct
  let f = f ~dim:3
end

module Part2 = struct
  let f = f ~dim:4
end

let command = Commands.both ~day:17 ~part1:Part1.f ~part2:Part2.f
