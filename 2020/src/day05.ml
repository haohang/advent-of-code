open! Core
open! Utils

let int_of_binary_string s =
  let digits = String.to_list s |> List.map ~f:Char.get_digit_exn in
  List.foldi ~init:0 (List.rev digits) ~f:(fun i accum d -> (d * Int.pow 2 i) + accum)
;;

let seat_id s =
  let s =
    String.map s ~f:(function
        | 'F' | 'L' -> '0'
        | 'B' | 'R' -> '1'
        | _         -> raise_s [%message "Invalid" (s : string)])
  in
  int_of_binary_string s
;;

module Part1 = struct
  let f input =
    let highest =
      List.map input ~f:seat_id |> List.max_elt ~compare:Int.compare |> Option.value_exn
    in
    Core.print_s [%message "Done" (highest : int)]
  ;;
end

module Part2 = struct
  let f input =
    let all = List.map input ~f:seat_id |> List.sort ~compare:Int.compare in
    let sum = List.reduce_exn all ~f:( + ) in
    let low = List.hd_exn all in
    let high = List.last_exn all in
    let missing = ((high + low) * (high - low + 1) / 2) - sum in
    Core.print_s [%message "Done" (missing : int)]
  ;;
end

let command = Commands.both ~day:5 ~part1:Part1.f ~part2:Part2.f
