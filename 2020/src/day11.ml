open! Core
open! Utils
open Graph

module Chart = struct
  module Loc = struct
    module T = struct
      type t =
        | Seat  of [ `Empty | `Full ]
        | Floor
      [@@deriving compare]

      let assoc = [ '.', Floor; 'L', Seat `Empty; '#', Seat `Full ]
    end

    include T
    include Charable.Make (T)
  end

  let of_input input = Graph.Grid.of_char_strings input ~f:Loc.of_char

  let run_one' t ~check_loc ~num_occupied =
    let open Graph in
    let newly_empty, newly_full =
      Grid.foldi t ~init:([], []) ~f:(fun coord (newly_empty, newly_full) elt ->
          let num_occupied = num_occupied t coord in
          match check_loc elt ~num_occupied with
          | `No_change   -> newly_empty, newly_full
          | `Newly_empty -> coord :: newly_empty, newly_full
          | `Newly_full  -> newly_empty, coord :: newly_full)
    in
    List.iter newly_empty ~f:(fun coord -> Grid.set_exn t coord (Loc.Seat `Empty));
    List.iter newly_full ~f:(fun coord -> Grid.set_exn t coord (Loc.Seat `Full))
  ;;

  let run_until_stable t ~run_one =
    let open Graph in
    let rec aux prev =
      run_one t;
      let grid = Grid.to_lists t in
      if [%compare.equal: Loc.t list list] prev grid then grid else aux grid
    in
    aux (Grid.to_lists t)
  ;;
end

let f' input ~run =
  let chart = Chart.of_input input in
  let end_ = run chart in
  let occupied =
    List.map
      end_
      ~f:
        (List.count ~f:(function
            | Chart.Loc.Seat `Full -> true
            | Seat `Empty | Floor  -> false))
    |> List.reduce_exn ~f:( + )
  in
  Core.print_s [%message "Done" (occupied : int)]
;;

module Part1 = struct
  let run =
    let num_occupied t coord =
      let adjacent = Coord.adjacent coord |> List.filter ~f:(Grid.in_bounds t) in
      List.count adjacent ~f:(fun coord ->
          match Grid.get_exn t coord with
          | Chart.Loc.Floor | Seat `Empty -> false
          | Seat `Full                    -> true)
    in
    let check_loc elt ~num_occupied =
      match elt with
      | Chart.Loc.Seat `Full when num_occupied >= 4 -> `Newly_empty
      | Seat `Empty when num_occupied = 0 -> `Newly_full
      | _ -> `No_change
    in
    let run_one = Chart.run_one' ~check_loc ~num_occupied in
    Chart.run_until_stable ~run_one
  ;;

  let f = f' ~run
end

module Part2 = struct
  let run =
    let num_occupied t coord =
      let adjacent = Coord.adjacent coord |> List.filter ~f:(Grid.in_bounds t) in
      List.count adjacent ~f:(fun coord' ->
          let vec = Coord.sub coord' coord in
          match
            List.find_map (Grid.move_until_edge t coord ~vec) ~f:(fun (_, elt) ->
                match elt with
                | Chart.Loc.Seat seat -> Some seat
                | Floor               -> None)
          with
          | None      -> false
          | Some seat ->
            (match seat with
            | `Full  -> true
            | `Empty -> false))
    in
    let check_loc elt ~num_occupied =
      match elt with
      | Chart.Loc.Seat `Full when num_occupied >= 5 -> `Newly_empty
      | Seat `Empty when num_occupied = 0 -> `Newly_full
      | _ -> `No_change
    in
    let run_one = Chart.run_one' ~check_loc ~num_occupied in
    Chart.run_until_stable ~run_one
  ;;

  let f = f' ~run
end

let command = Commands.both ~day:11 ~part1:Part1.f ~part2:Part2.f
