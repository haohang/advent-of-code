open! Core
open! Utils
open Graph

let num_tile_rows, num_tile_cols = 10, 10
let num_rows, num_cols = 12, 12

let map_grid f =
  List.init num_rows ~f:(fun row ->
      List.init num_cols ~f:(fun col -> f { Coord.row; col }))
;;

let parse_tile input =
  match input with
  | []         -> raise_s [%message "BUG -- empty chunk in input"]
  | hd :: rest ->
    let id =
      let num' = String.chop_prefix_exn hd ~prefix:"Tile " in
      slice num' 0 Int.max_value |> Int.of_string
    in
    let grid = Grid.of_strings rest in
    id, grid
;;

module Dir = struct
  let up, right, down, left = 0, 1, 2, 3
end

module Transformation = struct
  type t =
    { num_rotates_cw : int
    ; vertical_flipped : bool
    }
  [@@deriving compare, sexp]

  let all =
    List.cartesian_product [ true; false ] (List.range 0 4)
    |> List.map ~f:(fun (vertical_flipped, num_rotates_cw) ->
           { num_rotates_cw; vertical_flipped })
  ;;

  let transform { vertical_flipped; num_rotates_cw } tile =
    let rotated = Grid.rotate_cw tile num_rotates_cw in
    if vertical_flipped then Grid.reflect_over_y rotated else rotated
  ;;
end

module Border = struct
  module T = struct
    type t = char list [@@deriving compare, sexp]
  end

  include T
  include Comparable.Make (T)
end

let get_borders transformation grid =
  let transformed = Transformation.transform transformation grid in
  let up = Grid.get_row transformed 0 in
  let down = Grid.get_row transformed (grid.height - 1) in
  let left = Grid.get_col transformed 0 in
  let right = Grid.get_col transformed (grid.width - 1) in
  [ up; right; down; left ]
;;

let find_possible_orientations ~up ~down ~left ~right grid =
  List.filter Transformation.all ~f:(fun o ->
      List.zip_exn [ up; right; down; left ] (get_borders o grid)
      |> List.for_all ~f:(fun (a, b) ->
             Option.value_map a ~default:true ~f:(fun a ->
                 [%compare.equal: char list] a b)))
;;

let%expect_test "find_possible_orientations" =
  let _, tile = {|Tile 1:
0123
4567
8910
2345|} |> String.split ~on:'\n' |> parse_tile in
  let up = Some (String.to_list "2345") in
  let down = None in
  let left = Some (String.to_list "2840") in
  let right = Some (String.to_list "5073") in
  Core.printf
    !"%{sexp: Transformation.t list}\n%!"
    (find_possible_orientations ~up ~down ~left ~right tile);
  [%expect {|
    (((num_rotates_cw 2) (vertical_flipped true))) |}]
;;

let find_possible_transformations grid tile coord =
  let get_border dir coord =
    Option.map (Coord.Map.find grid coord) ~f:(fun ((_, grid), o) ->
        List.nth_exn (get_borders o grid) dir)
  in
  let up = get_border Dir.down Coord.(add coord north) in
  let down = get_border Dir.up Coord.(add coord south) in
  let left = get_border Dir.right Coord.(add coord west) in
  let right = get_border Dir.left Coord.(add coord east) in
  find_possible_orientations ~up ~down ~left ~right (snd tile)
;;

let grid_to_string grid =
  let state =
    map_grid (fun coord ->
        Option.map (Coord.Map.find grid coord) ~f:(fun ((_, tile), o) ->
            Transformation.transform o tile))
  in
  List.concat_map state ~f:(fun row ->
      List.init num_tile_rows ~f:(fun i ->
          List.map row ~f:(fun tile ->
              match tile with
              | None      -> String.make num_tile_cols ' '
              | Some tile -> Grid.get_row tile i |> String.of_char_list)
          |> String.concat ~sep:" ")
      @ [ "\n" ])
;;

let cut_borders tile =
  List.init (num_tile_rows - 2) ~f:(fun row ->
      List.init (num_tile_cols - 2) ~f:(fun col ->
          Grid.get_exn tile { Coord.row = row + 1; col = col + 1 }))
  |> Grid.of_lists
;;

let grid_with_trimmed_borders grid =
  let state =
    map_grid (fun coord ->
        Option.map (Coord.Map.find grid coord) ~f:(fun ((_, tile), o) ->
            cut_borders (Transformation.transform o tile)))
  in
  List.concat_map state ~f:(fun row ->
      List.init (num_tile_rows - 2) ~f:(fun i ->
          List.map row ~f:(fun tile ->
              match tile with
              | None      -> List.init num_tile_cols ~f:(const ' ')
              | Some tile -> Grid.get_row tile i))
      |> List.concat)
  |> Grid.of_lists
;;

let print_grid grid ~which =
  let state = map_grid (fun coord -> Option.map (Coord.Map.find grid coord) ~f:fst) in
  let pp =
    match which with
    | `Id   ->
      List.map state ~f:(fun row ->
          String.concat
            (List.map row ~f:(Option.map ~f:fst)
            |> List.map ~f:(Option.value_map ~f:Int.to_string ~default:"xxxx"))
            ~sep:"\t")
      |> String.concat ~sep:"\n"
    | `Tile -> grid_to_string grid |> String.concat ~sep:"\n"
  in
  Core.printf !"%s\n%!" pp
;;

let backtrack squares =
  let relevant_coords = map_grid Fn.id |> List.concat in
  let rec aux grid squares_placed coords_remaining =
    let remaining = List.length coords_remaining in
    if remaining <= 3
    then (
      Core.printf !"Remaining: %d\n%!" remaining;
      print_grid grid ~which:`Id);
    match coords_remaining with
    | []                        -> Some grid
    | next_coord :: rest_coords ->
      List.find_map squares ~f:(fun ((idx, _) as next) ->
          if Int.Set.mem squares_placed idx
          then None
          else (
            let poss = find_possible_transformations grid next next_coord in
            List.find_map poss ~f:(fun o ->
                aux
                  (Coord.Map.set grid ~key:next_coord ~data:(next, o))
                  (Int.Set.add squares_placed idx)
                  rest_coords)))
  in
  aux Coord.Map.empty Int.Set.empty relevant_coords
;;

module Part1 = struct
  let f input =
    let input = coagulate input in
    let grids = List.map input ~f:parse_tile in
    let result = backtrack grids in
    match result with
    | None      -> Core.print_s [%message "Weird! No solution found"]
    | Some grid ->
      let corners =
        let get coord = Coord.Map.find_exn grid coord |> fst |> fst in
        let top_left = get Coord.origin in
        let top_right = get { row = 0; col = num_cols - 1 } in
        let bottom_left = get { row = num_rows - 1; col = 0 } in
        let bottom_right = get { row = num_rows - 1; col = num_cols - 1 } in
        top_left * top_right * bottom_left * bottom_right
      in
      Core.print_s [%message "Done" (corners : int)]
  ;;
end

module Part2 = struct
  let monster =
    {|
                  # 
#    ##    ##    ###
 #  #  #  #  #  #   
|}
    |> String.split ~on:'\n'
    |> List.filter ~f:(fun row -> not (String.is_empty row))
    |> List.map ~f:String.to_list
    |> Grid.of_lists
  ;;

  let check_for_monster grid coord =
    Grid.foldi
      monster
      ~init:(true, Coord.Set.empty)
      ~f:(fun diff (is_valid_monster, accum) elt ->
        if not is_valid_monster
        then false, Coord.Set.empty
        else (
          let grid_coord = Coord.add diff coord in
          match Grid.get grid grid_coord, elt with
          | Some _, ' '         -> true, accum
          | Some '#', '#'       -> true, Coord.Set.add accum grid_coord
          | Some _, _ | None, _ -> false, Coord.Set.empty))
    |> snd
  ;;

  let f input =
    let input = coagulate input in
    let grids = List.map input ~f:parse_tile in
    let result = backtrack grids in
    match result with
    | None      -> Core.print_s [%message "Weird! No solution found"]
    | Some grid ->
      let grid = grid_with_trimmed_borders grid in
      let monster_coords =
        List.find_map_exn Transformation.all ~f:(fun o ->
            let grid = Transformation.transform o grid in
            let monster_coords =
              Grid.foldi grid ~init:Coord.Set.empty ~f:(fun coord accum _ ->
                  Coord.Set.union accum (check_for_monster grid coord))
            in
            if Coord.Set.is_empty monster_coords then None else Some monster_coords)
      in
      let total_coords =
        Grid.foldi grid ~init:0 ~f:(fun _ accum elt ->
            if Char.equal elt '#' then accum + 1 else accum)
      in
      let result = total_coords - Coord.Set.length monster_coords in
      Core.print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:20 ~part1:Part1.f ~part2:Part2.f
