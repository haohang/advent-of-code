open! Core
open! Utils

module Rule = struct
  type t =
    | And  of int list
    | Or   of t * t
    | Base of string
  [@@deriving sexp]

  let base_regex = "\"([a-z]+)\"" |> Re2.of_string

  let single_of_string s =
    match Re2.find_submatches base_regex s with
    | Error _   -> And [ Int.of_string s ]
    | Ok result -> Base (Option.value_exn result.(1))
  ;;

  let and_of_string s =
    match String.split s ~on:' ' with
    | [ elt ] -> single_of_string elt
    | list    -> And (List.map list ~f:Int.of_string)
  ;;

  let of_string s =
    match String.strip s |> String.split ~on:'|' with
    | [ elt ]  -> and_of_string elt
    | [ l; r ] ->
      let l = and_of_string (String.strip l) in
      let r = and_of_string (String.strip r) in
      Or (l, r)
    | _        -> raise_s [%message "Invalid rule" (s : string)]
  ;;

  let parse s =
    match String.split s ~on:':' with
    | [ idx; rule ] -> Int.of_string idx, of_string rule
    | _             -> raise_s [%message "Invalid line" (s : string)]
  ;;

  let%expect_test "parse" =
    let test s = Core.printf !"%{sexp: (int * t) }" (parse s) in
    test "0: 4 1 5";
    [%expect {| (0 (And (4 1 5))) |}];
    test "1: 2 3 | 3 2";
    [%expect {| (1 (Or (And (2 3)) (And (3 2)))) |}]
  ;;

  let rec reduce i ~rules = reduce' (Int.Map.find_exn rules i) ~rules

  and reduce' t ~rules =
    match t with
    | Base str  -> str
    | And lst   -> List.map lst ~f:(reduce ~rules) |> String.concat ~sep:""
    | Or (l, r) -> sprintf "(%s|%s)" (reduce' l ~rules) (reduce' r ~rules)
  ;;

  let parse_all lst = List.map lst ~f:parse |> Int.Map.of_alist_exn

  let%expect_test "reduce" =
    let rules =
      {|0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: "a"
5: "b"|}
      |> String.split ~on:'\n'
      |> parse_all
    in
    Core.printf "%s" (reduce 0 ~rules);
    [%expect {| a((aa|bb)(ab|ba)|(ab|ba)(aa|bb))b |}]
  ;;
end

let f input ~check =
  let result =
    match coagulate input with
    | [ rules; elts ] ->
      let rules = Rule.parse_all rules in
      List.count elts ~f:(check ~rules)
    | _               -> raise_s [%message "Invalid input"]
  in
  Core.print_s [%message "Done" (result : int)]
;;

module Part1 = struct
  let check ~rules =
    let regex = Rule.reduce 0 ~rules |> sprintf "^%s$" |> Re2.of_string in
    Re2.matches regex
  ;;

  let f = f ~check
end

module Part2 = struct
  let check ~rules s =
    let rule42 = Rule.reduce ~rules 42 in
    let rule31 = Rule.reduce ~rules 31 in
    match
      let r = sprintf "(%s)+$" rule31 |> Re2.of_string in
      Re2.find_first r s
    with
    | Error _ -> false
    | Ok str  ->
      let num = Re2.find_all_exn (Re2.of_string rule31) str |> List.length in
      let resolved_regex =
        sprintf "^(%s)+(%s){%d}(%s){%d}$" rule42 rule42 num rule31 num |> Re2.of_string
      in
      Re2.matches resolved_regex s
  ;;

  let f = f ~check
end

let command = Commands.both ~day:19 ~part1:Part1.f ~part2:Part2.f
