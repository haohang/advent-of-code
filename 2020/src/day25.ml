open! Core
open! Utils

let parse input =
  match input with
  | [ card; door ] -> Int.of_string card, Int.of_string door
  | _              -> raise_s [%message "Invalid input"]
;;

let run_loops subject ~n = Fn.apply_n_times ~n (fun accum -> accum * subject % 20201227) 1

let find_loops subject target =
  let rec aux accum loops =
    if accum = target
    then loops
    else (
      let next = accum * subject % 20201227 in
      aux next (loops + 1))
  in
  aux 1 0
;;

module Part1 = struct
  let f input =
    let card, door = parse input in
    let card_loops = find_loops 7 card in
    let door_loops = find_loops 7 door in
    let result_card = run_loops door ~n:card_loops in
    let result_door = run_loops card ~n:door_loops in
    Core.print_s [%message "Done" (result_card : int) (result_door : int)]
  ;;
end

module Part2 = struct
  let f input = ignore (input : string list)
end

let command = Commands.both ~day:25 ~part1:Part1.f ~part2:Part2.f
