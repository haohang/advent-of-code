open! Core
open! Std_internal
open! Utils
open Graph

module Grid = struct
  let count_trees_in_path t slope =
    let rec count_trees_in_path t slope ~start =
      let next = Coord.add start slope in
      if next.row >= t.Grid.height
      then 0
      else
        (if Grid.get_tiling t next then 1 else 0)
        + count_trees_in_path t slope ~start:next
    in
    count_trees_in_path t slope ~start:Coord.origin
  ;;

  let of_input input = Grid.of_char_strings input ~f:(Char.equal '#')
end

module Part1 = struct
  let f input =
    let grid = Grid.of_input input in
    let trees = Grid.count_trees_in_path grid { Coord.row = 1; col = 3 } in
    Core.print_s [%message "Done" (trees : int)]
  ;;
end

module Part2 = struct
  let f input =
    let grid = Grid.of_input input in
    let (slopes : Coord.t list) =
      [ { row = 1; col = 1 }
      ; { row = 1; col = 3 }
      ; { row = 1; col = 5 }
      ; { row = 1; col = 7 }
      ; { row = 2; col = 1 }
      ]
    in
    let trees =
      List.map slopes ~f:(Grid.count_trees_in_path grid) |> List.fold ~init:1 ~f:( * )
    in
    Core.print_s [%message "Done" (trees : int)]
  ;;
end

let command = Commands.both ~day:3 ~part1:Part1.f ~part2:Part2.f
