open! Core
open! Utils

let parse input =
  exactly_one_exn input |> String.to_list |> List.map ~f:Char.get_digit_exn
;;

let to_list cups start =
  let rec aux next accum =
    if next = start then List.rev accum else aux cups.(next) (next :: accum)
  in
  aux start []
;;

let play ?pad_to_len lst ~n =
  let len = Option.value pad_to_len ~default:n in
  (* Add an extra 0 at the beginning because this game is 1-indexed. *)
  let arr = Array.create ~len:(len + 1) 0 in
  (* Set such that arr.(i) = j means cup j follows cup i  *)
  let first = List.hd_exn lst in
  let last =
    List.fold lst ~init:first ~f:(fun prev elt ->
        arr.(prev) <- elt;
        elt)
  in
  let last =
    match pad_to_len with
    | None     -> last
    | Some len ->
      let max = List.reduce_exn lst ~f:Int.max in
      arr.(last) <- max + 1;
      for i = 1 to len - List.length lst do
        arr.(max + i) <- max + i + 1
      done;
      len
  in
  arr.(last) <- first;
  let curr = ref first in
  for _ = 0 to n do
    let first = arr.(!curr) in
    let second = arr.(first) in
    let third = arr.(second) in
    let fourth = arr.(third) in
    let dest_ok to_check = to_check <> first && to_check <> second && to_check <> third in
    let sub_one curr = if curr = 1 then len else curr - 1 in
    let get_next_ok start =
      (* Since we pick up 3 cups, we have to check at most 4 potential destinations. *)
      let d1 = sub_one start in
      let d2 = sub_one d1 in
      let d3 = sub_one d2 in
      let d4 = sub_one d3 in
      if dest_ok d1 then d1 else if dest_ok d2 then d2 else if dest_ok d3 then d3 else d4
    in
    let dest = get_next_ok (sub_one !curr) in
    let after_dest = arr.(dest) in
    (* Remove the chunk *)
    arr.(!curr) <- fourth;
    (* Insert the chunk after the destination *)
    arr.(dest) <- first;
    arr.(third) <- after_dest;
    curr := arr.(!curr)
  done;
  arr
;;

module Part1 = struct
  let f input =
    let lst = parse input in
    let result = play lst ~n:100 in
    let result = to_list result 1 in
    Core.print_s [%message "Done" (result : int list)]
  ;;
end

module Part2 = struct
  let f input =
    let lst = parse input in
    let result = play lst ~n:10_000_000 ~pad_to_len:1_000_000 in
    let result = result.(1) * result.(result.(1)) in
    Core.print_s [%message "Done" (result : int)]
  ;;
end

let command = Commands.both ~day:23 ~part1:Part1.f ~part2:Part2.f
