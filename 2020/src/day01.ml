open! Core
open! Std_internal

module Part1 = struct
  let find_sum input ~target =
    List.find_exn (List.cartesian_product input input) ~f:(fun (a, b) -> a + b = target)
  ;;

  let f input =
    let input = List.map input ~f:Int.of_string in
    let a, b = find_sum input ~target:2020 in
    print_s [%message "Done" (a : int) (b : int) ~product:(a * b : int)]
  ;;
end

module Part2 = struct
  let find_sum input ~target =
    List.find_exn
      (List.cartesian_product input (List.cartesian_product input input))
      ~f:(fun (a, (b, c)) -> a + b + c = target)
  ;;

  let f input =
    let input = List.map input ~f:Int.of_string in
    let a, (b, c) = find_sum input ~target:2020 in
    print_s [%message "Done" (a : int) (b : int) (c : int) ~product:(a * b * c : int)]
  ;;
end

let command = Commands.both ~day:1 ~part1:Part1.f ~part2:Part2.f
